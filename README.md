# cpp-ast

Extract a simple AST from a C++ project.


# build and run in docker

Build an image from the Dockerfile:

    docker build -t cpp-ast docker/

Compile cpp-ast:

    docker run --rm -v `pwd`:/home/root/cpp-ast cpp-ast /home/root/cpp-ast/build.sh

Generate example AST:

    docker run --rm -v `pwd`:/home/root/cpp-ast cpp-ast /home/root/cpp-ast/generate-example-ast.sh

