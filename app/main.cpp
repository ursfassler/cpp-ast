/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "component/parser/parse.h"
#include "component/ast/logger.h"
#include "component/parser/InputArguments.h"
#include <fstream>
#include <iostream>


int main(int argc, const char* argv[])
{
  StdLogger stdLogger{};
  DoubleMessageFilter logger{stdLogger};

  InputArguments arg{logger};
  arg.parse({argv, argv+argc});

  if (!arg.allOk()) {
    return -1;
  }

  std::ofstream output{"out.ast"};

  const auto ret = parse(arg.sources(), arg.compilationDatabase(), output, logger);

  return ret ? 0 : -1;
}
