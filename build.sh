#!/bin/sh

set -e

scriptdir=`dirname "$0"`
scriptdir=`realpath ${scriptdir}`
cd ${scriptdir}

mkdir -p build/
cd build
cmake ../

make -j `nproc`

