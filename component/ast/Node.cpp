/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Node.h"
#include <cassert>
#include <algorithm>


void Package::foreachPackage(const std::function<void(Package*)>& visitor)
{
  std::for_each(packages.cbegin(), packages.cend(), visitor);
}

void Package::foreachPackage(const std::function<void(const Package*)>& visitor) const
{
  std::for_each(packages.cbegin(), packages.cend(), visitor);
}

std::optional<Package*> Package::findPackage(const std::string& name)
{
  for (auto& child : packages) {
    if (child->getName() == name) {
      return {child};
    }
  }

  return {};
}

void Package::addPackage(Package* value)
{
  packages.push_back(value);
}

std::string Package::toString() const
{
  return getName();
}

void Package::accept(Visitor& visitor) const
{
  visitor.visit(*this);
}

Declaration::Declaration(const std::string& name, const Declaration::Path& path_) :
  Node{name},
  path{path_}
{
  assert(!path.empty());
}

Declaration::Path Declaration::getPath() const
{
  return path;
}

void Declaration::foreachReference(const std::function<void (const Declaration*)>& visitor) const
{
  std::for_each(references.cbegin(), references.cend(), visitor);
}

void Declaration::foreachReference(const std::function<void (Declaration*)>& visitor)
{
  std::for_each(references.cbegin(), references.cend(), visitor);
}

void Declaration::replaceReferences(const std::map<Declaration*, Declaration*>& map)
{
  for (Declaration*& ref : references){
    const auto idx = map.find(ref);
    if (idx != map.end()) {
      ref = idx->second;
    }
  }
}

void Declaration::addReference(Declaration* value)
{
  references.push_back(value);
}

void Declaration::moveReferencesTo(Declaration* destination)
{
  destination->references.insert(destination->references.end(), this->references.begin(), this->references.end());
//  this->references.clear();
}

bool Declaration::hasReferences() const
{
  return !references.empty();
}

bool Declaration::hasEqualReferences(const Declaration* other) const
{
  if (references.size() != other->references.size()) {
    return false;
  }

  const auto count = references.size();
  for (std::size_t i = 0; i < count; i++) {
    if (references[i]->getPath() != other->references[i]->getPath()) {
      return false;
    }
  }

  return true;
}


Node::Node(const std::string& name_) :
  name{name_}
{
}

std::string Node::getName() const
{
  return name;
}

void Node::foreachChild(const std::function<void(const Declaration*)>& visitor) const
{
  std::for_each(children.cbegin(), children.cend(), visitor);
}

const std::vector<Declaration*>& Node::getChildren() const
{
  return children;
}

void Node::addChild(Declaration* child)
{
  children.push_back(child);
}

void Node::clearChildren()
{
  children.clear();
}

void Node::moveChildrenTo(Node* destination)
{
  destination->children.insert(destination->children.end(), this->children.begin(), this->children.end());
  this->children.clear();
}

void Node::check_invariants() const
{
}

std::string Declaration::toString() const
{
  std::string result{};

  result += getName() + ":" + getPath();

  return result;
}

void Declaration::check_invariants() const
{
  Node::check_invariants();
  invariant_allChildrenAreUnique();
}

void Declaration::invariant_allChildrenAreUnique() const
{
  std::set<Path> paths{};
  std::map<Path, Node*> byPath{};
  for (const auto& child : getChildren()) {
    const auto path = child->getPath();

    const auto idx = byPath.find(path);
    if (idx != byPath.end()) {
      const auto defined = idx->second;
      assert(defined == child);
    }

    assert(paths.find(path) == paths.end());
    paths.insert(path);

    assert (idx == byPath.end());
    byPath[path] = child;
  }
}

void Class::accept(Visitor& visitor) const
{
  visitor.visit(*this);
}

void Method::accept(Visitor& visitor) const
{
  visitor.visit(*this);
}

void Field::accept(Visitor& visitor) const
{
  visitor.visit(*this);
}

void Function::accept(Visitor& visitor) const
{
  visitor.visit(*this);
}

void Variable::accept(Visitor& visitor) const
{
  visitor.visit(*this);
}

namespace
{


class TypeVisitor :
    public Visitor
{
  public:
    void visit(const Package&) override
    {
      assert(false);
    }

    void visit(const Class&) override
    {
      type_ = {NodeType::Class};
    }

    void visit(const Method&) override
    {
      type_ = {NodeType::Method};
    }

    void visit(const Field&) override
    {
      type_ = {NodeType::Field};
    }

    void visit(const Function&) override
    {
      type_ = {NodeType::Function};
    }

    void visit(const Variable&) override
    {
      type_ = {NodeType::Variable};
    }

    NodeType type() const
    {
      assert(type_.has_value());
      return type_.value();
    }

  private:
    std::optional<NodeType> type_{};
};


}

NodeType typeOf(const Declaration* node)
{
  TypeVisitor tv{};
  node->accept(tv);
  return tv.type();
}


Root::Root(const std::string& name_) :
  name{name_}
{
}
