/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <map>
#include <vector>
#include <string>
#include <set>
#include <functional>
#include <optional>


enum class NodeType
{
  Class,
  Method,
  Field,
  Function,
  Variable,
};

class Visitor;

class Declaration;

class Node
{
  public:
    Node(const std::string&);
    virtual ~Node() = default;

    std::string getName() const;

    void foreachChild(const std::function<void(const Declaration*)>&) const;
    const std::vector<Declaration*>& getChildren() const;
    void addChild(Declaration*);
    void clearChildren();
    void moveChildrenTo(Node*);

    virtual std::string toString() const = 0;
    virtual void accept(Visitor&) const = 0;
    virtual void check_invariants() const;

  private:
    const std::string name{};
    std::vector<Declaration*> children{};

};

class Declaration :
    public Node
{
  public:
    typedef std::string Path;

    Declaration(const std::string& name, const Path&);

    Path getPath() const;

    void foreachReference(const std::function<void(const Declaration*)>&) const;
    void foreachReference(const std::function<void(Declaration*)>&);
    void replaceReferences(const std::map<Declaration*, Declaration*>&);
    void addReference(Declaration*);
    void moveReferencesTo(Declaration*);
    bool hasReferences() const;
    bool hasEqualReferences(const Declaration*) const;

    std::string toString() const override;

    void check_invariants() const override;

  private:
    std::vector<Declaration*> references{};

    const Path path;

    void invariant_allChildrenAreUnique() const;

};

class Package :
    public Node
{
  public:
    using Node::Node;

    void foreachPackage(const std::function<void(Package*)>&);
    void foreachPackage(const std::function<void(const Package*)>&) const;
    std::optional<Package*> findPackage(const std::string&);
    void addPackage(Package*);

    std::string toString() const override;
    void accept(Visitor&) const override;

  private:
    std::vector<Package*> packages{};
};

class Class :
    public Declaration
{
  public:
    using Declaration::Declaration;

    void accept(Visitor&) const override;
};

class Method :
    public Declaration
{
  public:
    using Declaration::Declaration;

    void accept(Visitor&) const override;
};

class Field :
    public Declaration
{
  public:
    using Declaration::Declaration;

    void accept(Visitor&) const override;
};

class Function :
    public Declaration
{
  public:
    using Declaration::Declaration;

    void accept(Visitor&) const override;
};

class Variable :
    public Declaration
{
  public:
    using Declaration::Declaration;

    void accept(Visitor&) const override;
};

class Root
{
  public:
    Root(const std::string& name);

    const std::string name;
    Package top{""};
};


class Visitor
{
  public:
    virtual ~Visitor() = default;

    virtual void visit(const Package&) = 0;
    virtual void visit(const Class&) = 0;
    virtual void visit(const Method&) = 0;
    virtual void visit(const Field&) = 0;
    virtual void visit(const Function&) = 0;
    virtual void visit(const Variable&) = 0;
};


NodeType typeOf(const Declaration*);
