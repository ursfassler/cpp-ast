/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Nodes.h"
#include "Node.h"
#include <algorithm>
#include <stdexcept>


void Nodes::createRoot(const std::string& name)
{
  assert(stack.empty());

  root_ = new Root(name);
  stack.push_back(&root_->top);

  assert(!stack.empty());
}

Package* Nodes::createPackage(Nodes::Key, const std::string &name)
{
  auto parent = currentAs<Package>();
  auto byName = parent->findPackage(name);
  if (byName.has_value()) {
    return byName.value();
  }

  Package* node = new Package(name);
  parent->addPackage(node);

  return node;
}

Declaration* Nodes::createForDefinition(Key key, NodeType type, const std::string& name, const Declaration::Path& path)
{
  const auto iidx = inTree.find(key);
  if (iidx == inTree.end()) {
    Declaration* node{};

    const auto idx = notYetInTree.find(key);
    if (idx == notYetInTree.end()) {
      node = produce(type, name, path);
    } else {
      node = idx->second;
      assert(typeOf(node) == type);
      assert(node->getName() == name);
      notYetInTree.erase(idx);
    }

    inTree[key] = node;

    auto parent = currentAs<Node>();
    parent->addChild(node);

    return node;
  } else {
    const auto node = iidx->second;
    assert(notYetInTree.find(key) == notYetInTree.end());
    assert(typeOf(node) == type);
    assert(node->getName() == name);
    return node;
  }
}

std::optional<Declaration*> find(Nodes::Key key, NodeType type, const std::string& name, const std::map<Nodes::Key, Declaration*>& map)
{
  const auto idx = map.find(key);
  if (idx == map.end()) {
    return {};
  }

  const auto node = idx->second;

  const auto definedType = typeOf(node);
  const auto definedName = node->getName();
  assert(definedType == type);
  assert(definedName == name);

  return {node};
}

Declaration* Nodes::createForReference(Key key, NodeType type, const std::string& name, const Declaration::Path& path)
{
  const auto treeNode = find(key, type, name, inTree);
  if (treeNode.has_value()) {
    return treeNode.value();
  }

  const auto nonTreeNode = find(key, type, name, notYetInTree);
  if (nonTreeNode.has_value()) {
    return nonTreeNode.value();
  }

  Declaration* node = produce(type, name, path);
  notYetInTree[key] = node;
  return node;
}

void Nodes::push(Node* node)
{
  stack.push_back(node);
}

void Nodes::pop(Node* node)
{
  assert(!stack.empty());
  assert(stack.back() == node);
  stack.pop_back();

  assert(!stack.empty());
}

Declaration* Nodes::current() const
{
  return currentAs<Declaration>();
}

Node* Nodes::currentNode() const
{
  return currentAs<Node>();
}

void Nodes::verifyIsFinished() const
{
  if (!notYetInTree.empty()) {
    std::string msg = "expected all nodes in tree, but those are not:";
    for (const auto& itr : notYetInTree) {
      msg += "\n  " + itr.second->toString();
    }
    throw std::runtime_error(msg);
  }

  if (stack.size() != 1) {
    throw std::runtime_error("expected stack to be 1, but is " + std::to_string(stack.size()));
  }
}

void Nodes::clearKeys()
{
//  assert(isFinished());
  inTree.clear();
  notYetInTree.clear();
}

Root *Nodes::root() const
{
  return root_;
}

Declaration *Nodes::produce(NodeType type, const std::string& name, const Declaration::Path& path) const
{
  switch (type) {
    case NodeType::Class:
      return new Class(name, path);

    case NodeType::Field:
      return new Field(name, path);

    case NodeType::Method:
      return new Method(name, path);

    case NodeType::Function:
      return new Function(name, path);

    case NodeType::Variable:
      return new Variable(name, path);
  }
}
