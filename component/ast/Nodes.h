/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Node.h"
#include <map>
#include <vector>
#include <cassert>


class Nodes
{
public:
  typedef const void* Key;

  void createRoot(const std::string& name);
  Package* createPackage(Key, const std::string& name);
  Declaration* createForDefinition(Key, NodeType, const std::string& name, const Declaration::Path&);
  Declaration* createForReference(Key, NodeType, const std::string& name, const Declaration::Path&);

  void push(Node*);
  void pop(Node*);
  Declaration *current() const;
  Node *currentNode() const;

  void verifyIsFinished() const;

  void clearKeys();

  Root* root() const;

private:
  std::map<Key, Declaration*> inTree{};
  std::map<Key, Declaration*> notYetInTree{};
  std::vector<Node*> stack{};
  Root* root_{};

  template<typename T>
  T* currentAs() const
  {
    assert(!stack.empty());
    T* node = dynamic_cast<T*>(stack.back());
    assert(node != nullptr);
    return node;

  }

  Declaration* produce(NodeType, const std::string& name, const Declaration::Path&) const;

};
