/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "logger.h"
#include <iostream>


void StdLogger::error(const std::string& value) const
{
  std::cerr << value << std::endl;
}

void MemoryLogger::error(const std::string& value) const
{
  messages.push_back(value);
}


DoubleMessageFilter::DoubleMessageFilter(const Logger& logger_) :
  logger{logger_}
{
}

void DoubleMessageFilter::error(const std::string& value) const
{
  if (printed.find(value) == printed.end()) {
    printed.insert(value);
    logger.error(value);
  }
}


LogDistributor::LogDistributor(const Logger& logger1_, const Logger& logger2_) :
  logger1{logger1_},
  logger2{logger2_}
{
}

void LogDistributor::error(const std::string& value) const
{
  logger1.error(value);
  logger2.error(value);
}
