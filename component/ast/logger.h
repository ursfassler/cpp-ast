/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>
#include <set>


class Logger
{
  public:
    virtual ~Logger() = default;

    virtual void error(const std::string&) const = 0;

};

class StdLogger :
    public Logger
{
  public:
    void error(const std::string&) const override;

};

class MemoryLogger :
    public Logger
{
  public:
    void error(const std::string&) const override;

    mutable std::vector<std::string> messages{};

};

class DoubleMessageFilter :
    public Logger
{
  public:
    DoubleMessageFilter(const Logger&);

    void error(const std::string&) const override;

  private:
    const Logger& logger;
    mutable std::set<std::string> printed{};
};

class LogDistributor :
    public Logger
{
  public:
    LogDistributor(const Logger&, const Logger&);

    void error(const std::string&) const override;

  private:
    const Logger& logger1;
    const Logger& logger2;
};
