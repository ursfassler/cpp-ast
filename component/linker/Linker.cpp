/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Linker.h"
#include <cassert>
#include <stdexcept>


std::string Linker::getChildList(const Declaration* root)
{
  std::string result = "";
  for (const auto item : root->getChildren()) {
    result += "  " + item->getName() + "\n";
  }
  return result;
}

void Linker::mapNode(Declaration* original, Declaration* copy, LinkMap& map)
{
  if (original->getPath() != copy->getPath()) {
    throw std::invalid_argument("expected same path, got: " + original->getPath() + " <-> " + copy->getPath());
  }

  const auto on = original->getName();
  const auto cn = copy->getName();
  if (on != cn) {
    throw std::runtime_error("orginal name (" + original->toString() + ") does not match copy name (" + copy->toString() + ")");
  }

  const auto copyIsPrototype = copy->getChildren().empty() && !original->getChildren().empty();
  if (copyIsPrototype) {
    const auto equalReferences = copy->hasEqualReferences(original);
    if (copy->hasReferences() && !equalReferences) {
      throw std::runtime_error("prototype with references: " + copy->toString());
    }
    map[copy] = original;
    return;
  }

  const auto originalIsPrototype = original->getChildren().empty() && !copy->getChildren().empty();
  if (originalIsPrototype) {
    assert(!original->hasReferences());
    map[copy] = original;
    copy->moveReferencesTo(original);
    copy->moveChildrenTo(original);
    return;
  }

  if (original->getChildren().size() == copy->getChildren().size()) {
    const auto count = original->getChildren().size();
    for (std::size_t i = 0; i < count; i++) {
      const auto oc = original->getChildren().at(i);
      const auto cc = copy->getChildren().at(i);
      if (oc->getPath() != cc->getPath()) {
        throw std::runtime_error("child number " + std::to_string(i+1) + " of node " + original->getPath() + " has different paths: " + oc->getPath() + " <-> " + cc->getPath());
      }
    }
  } else {
    const auto origlist = getChildList(original);
    const auto copylist = getChildList(copy);
    throw std::runtime_error("nodes to map (" + original->toString() + ") have different children: " + std::to_string(original->getChildren().size()) + " versus " + std::to_string(copy->getChildren().size()) + "; \n" + origlist + "---- versus\n" + copylist);
  }

  map[copy] = original;

  if (!copy->hasEqualReferences(original)) {
    copy->moveReferencesTo(original);
  }

  for (std::size_t i = 0; i < original->getChildren().size(); i++) {
    auto oc = original->getChildren()[i];
    auto cc = copy->getChildren()[i];
    assert(typeOf(oc) == typeOf(cc));
    mapNode(oc, cc, map);
  }
}

void Linker::mergePackages(Package* original, Package* copy, LinkMap& map)
{
  linkIntoR(original, copy, map);
}

void Linker::relink(Declaration* node, const LinkMap& map)
{
  node->replaceReferences(map);

  for (auto& child : node->getChildren()) {
    relink(child, map);
  }
}

void Linker::relink(Package* node, const LinkMap& map)
{
  node->foreachPackage([&map, this](Package* child) {
    relink(child, map);
  });
  for (auto& child : node->getChildren()) {
    relink(child, map);
  }
}

void Linker::linkIntoR(Package *destination, Package *source, LinkMap& map)
{
  std::map<std::string, Package*> byNamespace;
  destination->foreachPackage([&byNamespace](Package* child) {
    const auto name = child->getName();
    byNamespace[name] = child;
  });

  source->foreachPackage([&destination, &byNamespace, &map, this](Package* child) {
    const auto name = child->getName();
    const auto idx = byNamespace.find(name);
    if (idx != byNamespace.end()) {
      mergePackages(idx->second, child, map);
    } else {
      destination->addPackage(child);
    }
  });

  for (const auto child : source->getChildren()) {
    const auto found = findChild(destination, child);
    if (found.has_value()) {
      mapNode(found.value(), child, map);
    } else {
      destination->addChild(child);
    }
  }

  source->clearChildren();
}

std::optional<Declaration*> Linker::findChild(const Package* parent, const Declaration* toFind)
{
  std::optional<Declaration*> result{};

  for (const auto itr : parent->getChildren()) {
    if (lookTheSame(itr, toFind)) {
      assert(!result);
      result = itr;
    }
  }

  return result;
}

bool Linker::lookTheSame(const Declaration* left, const Declaration* right) const
{
  if (left->getPath() != right->getPath()) {
    return false;
  }

  if (left->getChildren().empty()) {
    // is probably a prototype
    return true;
  }

  if (right->getChildren().empty()) {
    // is probably a prototype
    return true;
  }

  if (left->getChildren().size() != right->getChildren().size()) {
    return false;
  }

  const auto count = left->getChildren().size();
  for (std::size_t i = 0; i < count; i++) {
    const auto lc = left->getChildren().at(i);
    const auto rc = right->getChildren().at(i);
    if (!lookTheSame(lc, rc)) {
      return false;
    }
  }

  return true;
}

void Linker::reduce(LinkMap& map)
{
  for (auto& entry : map) {
    Declaration* dest = entry.second;
    while (true) {
      auto idx = map.find(dest);
      if (idx == map.end()) {
        entry.second = dest;
        break;
      }
      dest = idx->second;
    }
  }
}

void Linker::linkInto(Root *destination, Root *source)
{
  LinkMap map{};
  linkIntoR(&destination->top, &source->top, map);
  reduce(map);
  relink(&destination->top, map);
}
