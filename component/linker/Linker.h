/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/ast/Node.h"
#include <map>


class Linker
{
public:
  void linkInto(Root* destination, Root* source);

private:
  typedef std::map<Declaration*, Declaration*> LinkMap;

  void mapNode(Declaration *original, Declaration *copy, Linker::LinkMap &map);
  void mergePackages(Package *original, Package *copy, LinkMap &map);
  void relink(Declaration *node, const LinkMap &map);
  void relink(Package *node, const LinkMap &map);
  void linkIntoR(Package *destination, Package *source, LinkMap &map);
  void reduce(LinkMap &map);
  std::string getChildList(const Declaration *root);
  std::optional<Declaration*> findChild(const Package*, const Declaration*);
  bool lookTheSame(const Declaration*, const Declaration*) const;
};
