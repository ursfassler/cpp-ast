/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Verifier.h"
#include "component/ast/logger.h"
#include <cassert>
#include <stdexcept>


Verifier::Verifier(Logger& logger_) :
  logger{logger_}
{
}

void Verifier::gatherNodes(const Node* root, std::set<const Node*>& nodes)
{
  const auto idx = nodes.find(root);
  assert(idx == nodes.end());
  nodes.insert(root);

  root->foreachChild([&nodes, this](const Node* child){
    gatherNodes(child, nodes);
  });
}

void Verifier::gatherNodes(const Package* root, std::set<const Node*>& nodes)
{
  root->foreachPackage([&nodes, this](const Package* child) {
    gatherNodes(child, nodes);
  });
  root->foreachChild([&nodes, this](const Node* child){
    gatherNodes(child, nodes);
  });
}

void Verifier::checkReferencesN(const Declaration* root, const std::set<const Node*>& nodes)
{
  root->foreachReference([root, &nodes, this](const Declaration* target){
    const auto idx = nodes.find(target);
    if (idx == nodes.end()) {
      logger.error("reference not in tree; source: " + root->toString() + "; target: " + target->toString());
    }
  });
  root->foreachChild([&nodes, this](const Declaration* child){
    checkReferencesN(child, nodes);
  });
}

void Verifier::checkReferences(const Package* root, const std::set<const Node*>& nodes)
{
  root->foreachPackage([&nodes, this](const Package* child){
    checkReferences(child, nodes);
  });
  root->foreachChild([&nodes, this](const Declaration* child){
    checkReferencesN(child, nodes);
  });
}

void Verifier::gatherPaths(const Node* root, std::map<Declaration::Path, const Node*>& paths)
{
  const auto pn = dynamic_cast<const Package*>(root);
  if (pn) {
    pn->foreachPackage(std::bind(&Verifier::gatherPaths, this, std::placeholders::_1, paths));
  }

  for (const auto& child : root->getChildren()) {
    const auto path = child->getPath();

    const auto idx = paths.find(path);
    if (idx != paths.end()) {
      const auto& node1 = idx->second;
      const auto& node2 = child;
      throw std::runtime_error("path found in 2 different nodes: " + path + "; " + node1->toString() + "; " + node2->toString());
    }
    paths[path] = child;

    gatherPaths(child, paths);
  }
}

void Verifier::verifyUniqueIds(const Root* root)
{
  std::map<Declaration::Path, const Node*> paths{};
  gatherPaths(&root->top, paths);
}

void Verifier::verifyThatAllReferencesPointToNodeInTree(const Root* root)
{
  std::set<const Node*> nodes{};
  gatherNodes(&root->top, nodes);
  checkReferences(&root->top, nodes);
}
