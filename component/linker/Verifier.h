/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/ast/Node.h"
#include <set>
#include <map>

class Logger;


class Verifier
{
public:
  Verifier(Logger&);

  void verifyThatAllReferencesPointToNodeInTree(const Root*);
  void verifyUniqueIds(const Root *root);

private:
  Logger& logger;
  void checkReferences(const Package *root, const std::set<const Node *> &nodes);
  void checkReferencesN(const Declaration *root, const std::set<const Node *> &nodes);
  void gatherPaths(const Node *root, std::map<Declaration::Path, const Node*>&);
  void gatherNodes(const Node *root, std::set<const Node *> &nodes);
  void gatherNodes(const Package *root, std::set<const Node *> &nodes);
};


