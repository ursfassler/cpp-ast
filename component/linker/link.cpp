/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "link.h"
#include "Linker.h"
#include "Verifier.h"


void linkInto(Root *destination, Root *source, Logger& logger)
{
  Verifier verifier{logger};
  Linker linker{};

  verifier.verifyUniqueIds(source);
  verifier.verifyThatAllReferencesPointToNodeInTree(source);

  linker.linkInto(destination, source);

  verifier.verifyThatAllReferencesPointToNodeInTree(destination);
//  verifier.verifyUniqueIds(destination);
}
