/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DeclInfo.h"
#include "ListString.h"
#include "exceptions.h"
#include "type.h"
#include <clang/AST/DeclCXX.h>
#include <clang/AST/DeclTemplate.h>
#include <llvm/ADT/StringExtras.h>


DeclInfo::DeclInfo(
    const clang::LangOptions& languageOptions_,
    clang::SourceManager& sourceManager_
    ) :
  languageOptions{languageOptions_},
  sourceManager{sourceManager_}
{
}

std::string DeclInfo::getName(const clang::NamedDecl* decl) const
{
  const auto name = getBaseName(decl->getDeclName());

  const clang::TemplateDecl* templ = dynamic_cast<const clang::TemplateDecl*>(decl);
  const std::string midfix = templ ? getTemplateSignature(templ) : "";

  const clang::ClassTemplateSpecializationDecl* spec = dynamic_cast<const clang::ClassTemplateSpecializationDecl*>(decl);
  const std::string specfix = spec ? getSpecializationSignature(spec) : "";

  const clang::FunctionDecl* func = asFunction(decl);
  const std::string suffix = func ? getFunctionSignature(func) : "";

  return name + midfix + specfix + suffix;
}

std::string DeclInfo::getNameForId(const clang::NamedDecl* decl) const
{
  const auto name = getName(decl);
  if (name == "") {
    return "!" + getAnyNameFor(decl);
  } else {
    return name;
  }
}

std::string DeclInfo::getBaseName(const clang::DeclarationName& declName) const
{
  const auto nameKind = declName.getNameKind();
  if (nameKind == clang::DeclarationName::NameKind::CXXConversionFunctionName) {
    const auto type = declName.getCXXNameType();
    const auto typeName = getTypeAsString(type, languageOptions);
    return "operator " + typeName;
  } else {
    return declName.getAsString();
  }
}

std::string DeclInfo::getAnyNameFor(const clang::NamedDecl* decl) const
{
  const auto tagDecl = dynamic_cast<const clang::TagDecl*>(decl);
  if (tagDecl) {
    const auto typedefName = tagDecl->getTypedefNameForAnonDecl();
    if (typedefName) {
      const auto name = typedefName->getNameAsString();
      return name;
    }
  }

  const auto typeDecl = dynamic_cast<const clang::TypeDecl*>(decl);
  if (typeDecl) {
    const auto type = typeDecl->getTypeForDecl();
    assert(type);
    const auto canon = type->getCanonicalTypeInternal();
    const auto name = getTypeAsString(canon, languageOptions);
    return name;
  }

  return {};
}

std::string DeclInfo::getFunctionSignature(const clang::FunctionDecl* func) const
{
  const auto canonical = func->getCanonicalDecl();

  ListString arguments{"(", ", ", ")"};

  const auto parameters = canonical->parameters();
  for (const clang::ParmVarDecl* param : parameters) {
    const auto type = getTypeAsString(param->getType(), languageOptions);
    arguments.add(type);
  }

  const auto method = dynamic_cast<const clang::CXXMethodDecl*>(canonical);
  const std::string suffix = (method && method->isConst()) ? " const" : "";

  const auto result = arguments.str() + suffix;
  return result;
}

std::string DeclInfo::getTemplateSignature(const clang::TemplateDecl* templ) const
{
  ListString arguments{"<", ", ", ">"};

  const auto parameters = templ->getTemplateParameters();
  for (const clang::NamedDecl* param : *parameters) {
    const auto parameterName = getTemplateParameterName(param);
    arguments.add(parameterName);
  }

  const auto result = arguments.str();
  return result;
}

std::string DeclInfo::getTemplateParameterName(const clang::NamedDecl* param) const
{
  const auto kind = param->getKind();
  switch (kind) {
    case clang::Decl::Kind::TemplateTypeParm:
      {
        return "typename";
      }

    case clang::Decl::Kind::TemplateTemplateParm:
      {
        const auto typeParam = dynamic_cast<const clang::TemplateTemplateParmDecl*>(param);
        ListString arguments{"<", ", ", ">"};
        const auto list = typeParam->getTemplateParameters();
        for (const auto itr : list->asArray()) {
          const auto name = getTemplateParameterName(itr);
          arguments.add(name);
        }
        return "template" + arguments.str() + " typename";
      }

    case clang::Decl::Kind::NonTypeTemplateParm:
      {
        const auto typeParam = dynamic_cast<const clang::NonTypeTemplateParmDecl*>(param);
        const auto type = getTypeAsString(typeParam->getType(), languageOptions);
        return type;
      }

    default:
      {
        const auto loc = param->getLocation().printToString(sourceManager);
        const auto name = std::string{param->getDeclKindName()};
        throw NotImplementedException("template parameter kind \"" + name + "\" not impelmented @" + loc);
      }
  }
}

std::string DeclInfo::getSpecializationSignature(const clang::ClassTemplateSpecializationDecl* spec) const
{
  ListString arguments{"<", ", ", ">"};

  const auto& args = spec->getTemplateArgs();
  for (const clang::TemplateArgument& arg : args.asArray()) {
    const auto name = getSpecializationArgumentName(arg, spec->getLocation());
    const auto cname = name.c_str();
    arguments.add(name);
  }

  const auto result = arguments.str();
  return result;
}

std::string DeclInfo::getSpecializationArgumentName(const clang::TemplateArgument& arg, const clang::SourceLocation& loc) const
{
  const auto kind = arg.getKind();
  switch (kind) {
    case clang::TemplateArgument::ArgKind::Type:
      {
        const auto type = arg.getAsType();
        const auto name = getTypeAsString(type, languageOptions);
        return name;
      }

    case clang::TemplateArgument::ArgKind::Integral:
      {
        const auto integral = arg.getAsIntegral();
        const auto name = llvm::toString(integral, 10);
        return name;
      }

    default:
      {
        const auto sloc = loc.printToString(sourceManager);
        throw NotImplementedException("specialization parameter kind " + std::to_string(kind) + " not impelmented @" + sloc);
      }
  }
}


const clang::FunctionDecl* DeclInfo::asFunction(const clang::NamedDecl* decl) const
{
  const clang::FunctionTemplateDecl* funcTempl = dynamic_cast<const clang::FunctionTemplateDecl*>(decl);
  if (funcTempl) {
    return funcTempl->getTemplatedDecl();
  }

  const clang::FunctionDecl* func = dynamic_cast<const clang::FunctionDecl*>(decl);
  if (func) {
    const auto templatedKind = func->getTemplatedKind();
    switch (templatedKind) {
      case clang::FunctionDecl::TK_NonTemplate:
        return func;

      case clang::FunctionDecl::TK_FunctionTemplateSpecialization:
        {
          const auto functionSpecializationInfo = func->getTemplateSpecializationInfo();
          eassert(functionSpecializationInfo);
          const auto function = functionSpecializationInfo->getTemplate();
          eassert(function);
          return function->getTemplatedDecl();
        }

      case clang::FunctionDecl::TK_FunctionTemplate:
        {
          const auto describedFunctionTemplate = func->getDescribedFunctionTemplate();
          eassert(describedFunctionTemplate);
          const auto function = describedFunctionTemplate->getTemplatedDecl();
          eassert(function);
          return function;
        }

      case clang::FunctionDecl::TK_MemberSpecialization:
        {
          const auto memberSpecializationInfo = func->getMemberSpecializationInfo();
          eassert(memberSpecializationInfo);
          const auto definition = memberSpecializationInfo->getInstantiatedFrom();
          eassert(definition);
          const auto function = definition->getAsFunction();
          eassert(function);
          return function;
        }

      case clang::FunctionDecl::TK_DependentFunctionTemplateSpecialization:
        throw NotImplementedException("asFunction TK_DependentFunctionTemplateSpecialization");
    }
  }

  return {};
}

bool DeclInfo::isInSystemHeader(const clang::Decl *value) const
{
  auto location = value->getLocation();
  return sourceManager.isInSystemHeader(location);
}
