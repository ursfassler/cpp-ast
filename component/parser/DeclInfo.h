/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <clang/AST/Decl.h>
#include <clang/AST/DeclTemplate.h>
#include <clang/Basic/LangOptions.h>
#include <clang/Basic/SourceManager.h>


class DeclInfo
{
public:
  DeclInfo(
      const clang::LangOptions&,
      clang::SourceManager&
      );

  std::string getName(const clang::NamedDecl*) const;
  std::string getNameForId(const clang::NamedDecl*) const;

  bool isInSystemHeader(const clang::Decl*) const;

private:
  const clang::LangOptions& languageOptions;
  clang::SourceManager& sourceManager;

  std::string getAnyNameFor(const clang::NamedDecl*) const;
  std::string getFunctionSignature(const clang::FunctionDecl*) const;
  std::string getTemplateSignature(const clang::TemplateDecl*) const;
  std::string getTemplateParameterName(const clang::NamedDecl*) const;
  std::string getSpecializationSignature(const clang::ClassTemplateSpecializationDecl*) const;
  std::string getSpecializationArgumentName(const clang::TemplateArgument&, const clang::SourceLocation&) const;
  const clang::FunctionDecl *asFunction(const clang::NamedDecl*) const;
  std::string getBaseName(const clang::DeclarationName&) const;
};
