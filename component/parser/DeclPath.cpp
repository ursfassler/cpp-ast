/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DeclPath.h"
#include "DeclInfo.h"
#include "exceptions.h"
#include "type.h"


DeclPath::DeclPath(
    const std::string& tuId_,
    const DeclInfo& info_,
    const clang::SourceManager& sourceManager_
    ) :
  tuId{tuId_},
  info{info_},
  sourceManager{sourceManager_}
{
}

std::string DeclPath::getStringPath(const DeclPath::Path& path) const
{
  std::string result{};

  bool needOffsetToIdentify = false;
  for (const auto part : path) {
    result += "::";

    const auto name = info.getNameForId(part);
    result += name;

    if (needOffsetToIdentify) {
      const auto offset = sourceManager.getFileOffset(part->getSourceRange().getBegin());
      result += "@" + std::to_string(offset);
    }

    const auto func = dynamic_cast<const clang::FunctionDecl*>(part);
    needOffsetToIdentify  = needOffsetToIdentify || func;
  }

  return result;
}

DeclPath::Path DeclPath::getPathList(const clang::NamedDecl* value) const
{
  auto path = getPath(value->getDeclContext());
  path.push_back(value);
  return path;
}

DeclPath::Path DeclPath::getPath(const clang::DeclContext* value) const
{
  if (value->isTranslationUnit()) {
    return {};
  }

  auto result = getPath(value->getParent());

  const auto kind = value->getDeclKind();
  switch (kind) {
    case clang::Decl::Kind::Function:
    case clang::Decl::Kind::CXXMethod:
    case clang::Decl::Kind::CXXConstructor:
    case clang::Decl::Kind::CXXConversion:
    case clang::Decl::Kind::CXXDestructor:
    case clang::Decl::Kind::CXXDeductionGuide:
      result.push_back(static_cast<const clang::FunctionDecl*>(value));
      break;

    case clang::Decl::Kind::Record:
    case clang::Decl::Kind::CXXRecord:
    case clang::Decl::Kind::ClassTemplateSpecialization:
    case clang::Decl::Kind::ClassTemplatePartialSpecialization:
      result.push_back(static_cast<const clang::RecordDecl*>(value));
      break;

    case clang::Decl::Kind::Enum:
      result.push_back(static_cast<const clang::EnumDecl*>(value));
      break;

    case clang::Decl::Kind::Namespace:
      result.push_back(static_cast<const clang::NamespaceDecl*>(value));
      break;

    case clang::Decl::Kind::LinkageSpec:
      break;

    default:
      throw NotImplementedException(std::string{"getPath for decl kind "} + value->getDeclKindName());
    }

  return result;
}

Declaration::Path DeclPath::getPath(const clang::NamedDecl* value) const
{
  const auto kind = getDeclKindName(value);
  const auto baseName = getFullPath(value);
  const auto nameWithType = kind + "+" + baseName;

  const auto tuPrivate = isTuPrivate(value);
  const auto nameWithVisibility = tuPrivate ? (tuId + "+" + nameWithType) : nameWithType;

  return nameWithVisibility;
}

std::string DeclPath::getDeclKindName(const clang::NamedDecl* value) const
{
  const auto record = dynamic_cast<const clang::CXXRecordDecl*>(value);
  const auto isCStruct = record && record->isStruct();

  if (isCStruct) {
    return "Record";
  }

  return value->getDeclKindName();
}

bool DeclPath::isTuPrivate(const clang::NamedDecl* value) const
{
  const auto hasLinkage = value->hasLinkage();
  if (hasLinkage) {
    const auto isExternallyVisible = value->isExternallyVisible();
    return !isExternallyVisible;
  } else {
    const auto isInAnonymousNamespace = value->isInAnonymousNamespace();
    return isInAnonymousNamespace;
  }
}

std::string DeclPath::getFullPath(const clang::NamedDecl* value) const
{
  const auto vpath = getPathList(value);
  const auto spath = getStringPath(vpath);
  return spath;
}
