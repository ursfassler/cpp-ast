/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/ast/Node.h"
#include <clang/AST/Decl.h>


class DeclInfo;


class DeclPath
{
public:
  DeclPath(
      const std::string& tuId,
      const DeclInfo&,
      const clang::SourceManager&
      );

  Declaration::Path getPath(const clang::NamedDecl*) const;

private:
  const std::string tuId;
  const DeclInfo& info;
  const clang::SourceManager& sourceManager;

  typedef std::vector<const clang::NamedDecl*> Path;

  std::string getDeclKindName(const clang::NamedDecl*) const;
  bool isTuPrivate(const clang::NamedDecl*) const;
  std::string getFullPath(const clang::NamedDecl*) const;
  std::string getStringPath(const Path&) const;
  DeclPath::Path getPathList(const clang::NamedDecl*) const;
  DeclPath::Path getPath(const clang::DeclContext*) const;
};
