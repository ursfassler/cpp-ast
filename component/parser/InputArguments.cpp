/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "InputArguments.h"
#include "commands.h"
#include "component/ast/logger.h"
#include <algorithm>
#include <fstream>


InputArguments::InputArguments(const Logger& log_) :
  log{log_}
{
}

void InputArguments::parse(const std::vector<std::string>& arg)
{
  ok = false;

  if (arg.size() < 2) {
    log.error("expected compilation database as argument");
    return;
  }

  const std::string name = "compile_commands.json";

  const std::string cdbName = arg[1];
  std::ifstream cdb{cdbName};
  sources_ = commands(cdb);

  if (arg.size() > 2) {
    std::vector<std::string> specsource{};
    for (int i = 2; i < arg.size(); i++) {
      const auto& filename = arg[i];
      const auto idx = std::find(sources_.begin(), sources_.end(), filename);
      if (idx == sources_.end()) {
        log.error("specified file not in compilation database: " + filename);
        return;
      }
      specsource.push_back(filename);
    }
    sources_ = specsource;
  }

  const auto path = cdbName.substr(0, cdbName.size()-name.size());

  std::string error{};
  _compilationDatabase = clang::tooling::CompilationDatabase::loadFromDirectory(path, error);

  if (!_compilationDatabase) {
    log.error("could not open compilation database: " + error);
    return;
  }

  ok = true;
}

bool InputArguments::allOk() const
{
  return ok;
}

std::vector<std::string> InputArguments::sources() const
{
  return sources_;
}

const clang::tooling::CompilationDatabase& InputArguments::compilationDatabase() const
{
  return *_compilationDatabase.get();
}
