/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <clang/Tooling/CompilationDatabase.h>
#include <vector>
#include <string>
#include <istream>

class Logger;

class InputArguments
{
  public:
    InputArguments(const Logger&);

    void parse(const std::vector<std::string>& arg);
    bool allOk() const;

    std::vector<std::string> sources() const;
    const clang::tooling::CompilationDatabase& compilationDatabase() const;

  private:
    const Logger& log;
    bool ok{false};
    std::vector<std::string> sources_{};
    std::unique_ptr<clang::tooling::CompilationDatabase> _compilationDatabase{};
};

