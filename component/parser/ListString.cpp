/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ListString.h"


ListString::ListString(
    const std::string& opening_,
    const std::string& separator_,
    const std::string& closing_) :
  opening{opening_},
  separator{separator_},
  closing{closing_}
{
}

void ListString::add(const std::string& value)
{
  if (first) {
    first = false;
  } else {
    list += separator;
  }

  list += value;
}

std::string ListString::str() const
{
  return opening + list + closing;
}
