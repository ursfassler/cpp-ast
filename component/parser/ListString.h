/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>


class ListString
{
public:
  ListString(
      const std::string& opening,
      const std::string& separator,
      const std::string& closing
      );

  void add(const std::string&);
  std::string str() const;

private:
  std::string list{};

  bool first = true;

  const std::string opening;
  const std::string separator;
  const std::string closing;
};
