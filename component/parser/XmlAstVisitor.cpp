/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "XmlAstVisitor.h"
#include "DeclInfo.h"
#include "DeclPath.h"
#include "component/ast/Node.h"
#include "component/ast/Nodes.h"
#include "component/ast/logger.h"
#include "exceptions.h"
#include "type.h"
#include <iostream>

XmlAstVisitor::XmlAstVisitor(
    const DeclPath& path_,
    const DeclInfo& info_,
    clang::ASTContext& context_,
    clang::SourceManager& sourceManager_,
    Nodes& nodes_,
    Logger& logger_
    ) :
  path{path_},
  info{info_},
  context{context_},
  sourceManager{sourceManager_},
  nodes{nodes_},
  logger{logger_}
{
}

bool XmlAstVisitor::shouldVisitTemplateInstantiations() const
{
  return false;
}

bool XmlAstVisitor::shouldVisitImplicitCode() const
{
  return false;
}

const Nodes& XmlAstVisitor::getNodes() const
{
  return nodes;
}

bool XmlAstVisitor::TraverseDecl(clang::Decl* value)
{
  if (!value) {
    return true;
  } else if (info.isInSystemHeader(value)) {
    return true;
  } else {
    const auto idx = visitedDecl.find(value);
    const auto hasVisited = idx != visitedDecl.end();
    if (hasVisited) {
      return true;
    }

    visitedDecl.insert(value);

    try{
      return RecursiveASTVisitor<XmlAstVisitor>::TraverseDecl(value);
    } catch (BasicErrorException& e) {
      const auto loc = value->getLocation().printToString(sourceManager);
      throw ErrorAtException(e.message(), loc);
    }
  }
}

bool XmlAstVisitor::TraverseClassTemplateDecl(clang::ClassTemplateDecl* value)
{
  const auto name = getName(value);
  const auto path = getPath(value);
  const auto key = value->getFirstDecl();

  Declaration* node = nodes.createForDefinition(key, NodeType::Class, name, path);
  const auto decl = value->getTemplatedDecl();
  if (decl->isThisDeclarationADefinition()) {
    for (const auto& base : decl->bases()) {
      addTypeReferencesToNode(base.getType(), node, value->getLocation());
    }
  }

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseClassTemplateDecl(value);
  nodes.pop(node);

  return result;
}

bool XmlAstVisitor::VisitNonTypeTemplateParmDecl(const clang::NonTypeTemplateParmDecl* value)
{
  const auto type = value->getType();
  const auto parent = nodes.current();
  addTypeReferencesToNode(type, parent, value->getLocation());

  return true;
}

bool XmlAstVisitor::TraverseCXXRecordDecl(clang::CXXRecordDecl* value)
{
  if (isEmptyDeclaration(value, !value->field_empty())) {
    return true;
  }

  const auto name = getName(value);
  const auto path = getPath(value);

  const auto isOutOfLine = value->isOutOfLine();
  if (isOutOfLine) {
    logError("ignoring out of line declaration of " + name, value);
    return true;
  }

  const auto describedClassTemplate = value->getDescribedClassTemplate();
  const auto hasTemplate = describedClassTemplate;
  const auto isTemplated = value->isTemplated();
  const auto isTemplateDefinition = isTemplated && hasTemplate;
  if (isTemplateDefinition) {
    const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseCXXRecordDecl(value);
    return result;
  }

  Declaration* node = nodes.createForDefinition(value->getFirstDecl(), NodeType::Class, name, path);
  if (value->isCompleteDefinition()) {
    for (const auto& base : value->bases()) {
      addTypeReferencesToNode(base.getType(), node, value->getLocation());
    }
  }

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseCXXRecordDecl(value);
  nodes.pop(node);
  return result;
}

bool XmlAstVisitor::TraverseRecordDecl(clang::RecordDecl* value)
{
  if (isEmptyDeclaration(value, !value->field_empty())) {
    return true;
  }

  const auto name = getName(value);
  const auto path = getPath(value);

  Declaration* node = nodes.createForDefinition(value->getFirstDecl(), NodeType::Class, name, path);

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseRecordDecl(value);
  nodes.pop(node);
  return result;
}

bool XmlAstVisitor::isEmptyDeclaration(const clang::TagDecl* value, bool hasElements) const
{
  const auto name = getName(value);
  if (name == "") {
    const auto kindName = value->getKindName().str();
    logError("found " + kindName + " without name", value);

    if (!hasElements) {
      return true;
    }
  }

  return false;
}

bool XmlAstVisitor::TraverseClassTemplatePartialSpecializationDecl(clang::ClassTemplatePartialSpecializationDecl* value)
{
  const auto name = getName(value);
  const auto path = getPath(value);

  Declaration* node = nodes.createForDefinition(value->getFirstDecl(), NodeType::Class, name, path);

  const auto type = value->getInjectedSpecializationType();
  addTypeReferencesToNode(type, node, value->getLocation());

  if (value->isCompleteDefinition()) {
    for (const auto& base : value->bases()) {
      addTypeReferencesToNode(base.getType(), node, value->getLocation());
    }
  }

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseClassTemplatePartialSpecializationDecl(value);
  nodes.pop(node);
  return result;
}

bool XmlAstVisitor::TraverseClassTemplateSpecializationDecl(clang::ClassTemplateSpecializationDecl* value)
{
  const auto name = getName(value);
  const auto path = getPath(value);

  Declaration* node = nodes.createForDefinition(value->getFirstDecl(), NodeType::Class, name, path);

  const auto templ = value->getSpecializedTemplate();
  const auto templName = getName(templ);
  const auto templId = getPath(value);
  const auto root = nodes.createForReference(templ, NodeType::Class, templName, templId);
  node->addReference(root);

  if (value->isCompleteDefinition()) {
    for (const auto& base : value->bases()) {
      addTypeReferencesToNode(base.getType(), node, value->getLocation());
    }
  }

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseClassTemplateSpecializationDecl(value);
  nodes.pop(node);
  return result;
}

bool XmlAstVisitor::TraverseCXXMethodDecl(clang::CXXMethodDecl* value)
{
  if (value->isTemplateInstantiation()) {
    // This is an implicitly generated method.
    // We are only interested in relations that are visible in the code.
    return true;
  }

  const auto templatedKind = value->getTemplatedKind();
  if (templatedKind == clang::FunctionDecl::TemplatedKind::TK_FunctionTemplate) {
    // we already added this method via FunctionTemplateDecl
    return RecursiveASTVisitor<XmlAstVisitor>::TraverseCXXMethodDecl(value);
  }

  Declaration* node;

  if (templatedKind == clang::FunctionDecl::TemplatedKind::TK_MemberSpecialization) {
    const auto name = getName(value);
    const auto path = getPath(value);

    auto memberSpecializationInfo = value->getMemberSpecializationInfo();
    eassert(memberSpecializationInfo);
    auto definition = memberSpecializationInfo->getInstantiatedFrom();
    eassert(definition);
    auto function = dynamic_cast<clang::CXXMethodDecl*>(definition);
    eassert(function);

    node = nodes.createForReference(function, NodeType::Method, name, path);
  } else {
    node = handleMethod(value);
  }

  const auto isImplementation = value->isThisDeclarationADefinition();
  const auto isPureVirtual = value->isPure();
  if (!isImplementation && !isPureVirtual) {
    return true;
  }

  addTypeReferencesToNode(value->getReturnType(), node, value->getLocation());

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseCXXMethodDecl(value);
  nodes.pop(node);

  return result;
}

bool XmlAstVisitor::TraverseCXXConstructorDecl(clang::CXXConstructorDecl* value)
{
  if (value->isTemplateInstantiation()) {
    // This is an implicitly generated method.
    // We are only interested in relations that are visible in the code.
    return true;
  }

  const auto templatedKind = value->getTemplatedKind();
  if (templatedKind == clang::FunctionDecl::TemplatedKind::TK_FunctionTemplate) {
    // we already added this method via FunctionTemplateDecl
    return RecursiveASTVisitor<XmlAstVisitor>::TraverseCXXConstructorDecl(value);
  }

  auto node = handleMethod(value);

  if (value->isThisDeclarationADefinition()) {
    for (const auto& init : value->inits()) {
      const auto base = init->isBaseInitializer();
      const auto anyMember = init->isAnyMemberInitializer();
      const auto delegatingInitializer = init->isDelegatingInitializer();
      eassert((base ^ anyMember) | delegatingInitializer);
      if (anyMember) {
        const auto field = init->getAnyMember();
        eassert(field);
        const auto name = getName(field);
        addReferenceIfAllowed(node, field, NodeType::Field, name);
      }
    }
  } else {
    return true;
  }

  addTypeReferencesToNode(value->getReturnType(), node, value->getLocation());

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseCXXConstructorDecl(value);
  nodes.pop(node);

  return result;
}

bool XmlAstVisitor::TraverseCXXDestructorDecl(clang::CXXDestructorDecl* value)
{
  if (value->isTemplateInstantiation()) {
    // This is an implicitly generated method.
    // We are only interested in relations that are visible in the code.
    return true;
  }

  const auto templatedKind = value->getTemplatedKind();
  if (templatedKind == clang::FunctionDecl::TemplatedKind::TK_FunctionTemplate) {
    // we already added this method via FunctionTemplateDecl
    return RecursiveASTVisitor<XmlAstVisitor>::TraverseCXXDestructorDecl(value);
  }

  auto node = handleMethod(value);

  if (!value->isThisDeclarationADefinition()) {
    return true;
  }

  addTypeReferencesToNode(value->getReturnType(), node, value->getLocation());

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseCXXDestructorDecl(value);
  nodes.pop(node);

  return result;
}

bool XmlAstVisitor::TraverseCXXConversionDecl(clang::CXXConversionDecl* value)
{
  auto node = handleMethod(value);

  if (!value->isThisDeclarationADefinition()) {
    return true;
  }

  addTypeReferencesToNode(value->getReturnType(), node, value->getLocation());

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseCXXConversionDecl(value);
  nodes.pop(node);

  return result;
}

bool XmlAstVisitor::TraverseFriendDecl(clang::FriendDecl* value)
{
  logError("can not handle friend declaration", value);
  return true;
}

Declaration* XmlAstVisitor::handleMethod(const clang::CXXMethodDecl* value)
{
  const auto name = getName(value);
  const auto path = getPath(value);

  Declaration* node = nodes.createForDefinition(value->getFirstDecl(), NodeType::Method, name, path);

  const auto templateKind = value->getTemplatedKind();
  switch (templateKind) {
    case clang::CXXMethodDecl::TK_NonTemplate: {
        break;
      }

    case clang::CXXMethodDecl::TK_FunctionTemplate: {
        throw NotImplementedException("clang::CXXMethodDecl::TK_FunctionTemplate");
        break;
      }

    case clang::CXXMethodDecl::TK_MemberSpecialization: {
        const auto memberSpecializationInfo = value->getMemberSpecializationInfo();
        eassert(memberSpecializationInfo);
        const auto definition = memberSpecializationInfo->getInstantiatedFrom();
        eassert(definition);
        const auto function = definition->getAsFunction();
        eassert(function);
        const auto templ = function->getFirstDecl();

        const auto templName = getName(templ);
        addReferenceIfAllowed(node, templ->getFirstDecl(), NodeType::Method, templName);

        break;
      }

    case clang::CXXMethodDecl::TK_FunctionTemplateSpecialization: {
        const auto functionSpecializationInfo = value->getTemplateSpecializationInfo();
        eassert(functionSpecializationInfo);
        const auto function = functionSpecializationInfo->getTemplate();
        eassert(function);
        const auto templ = function->getFirstDecl();

        const auto templName = getName(templ);
        addReferenceIfAllowed(node, templ, NodeType::Method, templName);

        break;
      }

    case clang::CXXMethodDecl::TK_DependentFunctionTemplateSpecialization: {
        throw NotImplementedException("clang::CXXMethodDecl::TK_DependentFunctionTemplateSpecialization");
        break;
      }
  }

  return node;
}

bool XmlAstVisitor::TraverseFunctionTemplateDecl(clang::FunctionTemplateDecl* value)
{
  const auto name = getName(value);
  const auto path = getPath(value);
  const auto key = value->getFirstDecl();

  const auto context = value->getDeclContext();
  const auto contextIsClass = context->isRecord();
  const auto type = contextIsClass ? NodeType::Method : NodeType::Function;

  if (!value->isThisDeclarationADefinition()) {
    return true;
  }

  Declaration* node = nodes.createForDefinition(key, type, name, path);
  addTypeReferencesToNode(value->getTemplatedDecl()->getReturnType(), node, value->getLocation());

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseFunctionTemplateDecl(value);
  nodes.pop(node);
  return result;
}

bool XmlAstVisitor::TraverseFunctionDecl(clang::FunctionDecl* value)
{
  const auto isTemplateSpecialization = value->isFunctionTemplateSpecialization();
  if (isTemplateSpecialization) {
    return true;
  }

  const auto isTemplated = value->isTemplated();
  if (isTemplated) {
    return RecursiveASTVisitor<XmlAstVisitor>::TraverseFunctionDecl(value);
  }

  const auto name = getName(value);
  const auto path = getPath(value);

  const auto parent = nodes.currentNode();
  const auto parentIsFunction = dynamic_cast<const Function*>(parent);
  const auto parentIsMethod = dynamic_cast<const Method*>(parent);
  const auto inFunction = parentIsFunction || parentIsMethod;
  if (inFunction) {
    const std::string type = parentIsFunction ? "function" : "method";
    logError("can't handle function prototype in " + type, value);
    return true;
  }

  Declaration* node = nodes.createForDefinition(value->getFirstDecl(), NodeType::Function, name, path);

  if (!value->isThisDeclarationADefinition()) {
    return true;
  }

  addTypeReferencesToNode(value->getReturnType(), node, value->getLocation());

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseFunctionDecl(value);
  nodes.pop(node);
  return result;
}

bool XmlAstVisitor::TraverseRedeclarableTemplateDecl(clang::RedeclarableTemplateDecl*)
{
  throw NotImplementedException("TraverseRedeclarableTemplateDecl");
}

bool XmlAstVisitor::TraverseVarTemplateDecl(clang::VarTemplateDecl*)
{
  throw NotImplementedException("TraverseVarTemplateDecl");
}

bool XmlAstVisitor::TraverseTypeAliasTemplateDecl(clang::TypeAliasTemplateDecl* value)
{
  const auto name = getName(value);
  const auto path = getPath(value);

  Declaration* node = nodes.createForDefinition(value->getFirstDecl(), NodeType::Class, name, path);

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseTypeAliasTemplateDecl(value);
  nodes.pop(node);
  return result;
}

bool XmlAstVisitor::TraverseTypeAliasDecl(clang::TypeAliasDecl* value)
{
  const auto name = getName(value);
  const auto isTemplated = value->isTemplated();

  if (isTemplated) {
    auto node = nodes.current();

    const auto type = getTypedefNameDeclType(value);
    addTypeReferencesToNode(type, node, value->getLocation());

    const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseTypeAliasDecl(value);

    return result;
  } else {
    const auto path = getPath(value);
    Declaration* node = nodes.createForDefinition(value->getFirstDecl(), NodeType::Class, name, path);

    const auto type = getTypedefNameDeclType(value);
    addTypeReferencesToNode(type, node, value->getLocation());

    nodes.push(node);
    const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseTypeAliasDecl(value);
    nodes.pop(node);

    return result;
  }
}

bool XmlAstVisitor::TraverseTypedefDecl(clang::TypedefDecl* value)
{
  const auto name = getName(value);
  const auto path = getPath(value);

  Declaration* node = nodes.createForDefinition(value->getFirstDecl(), NodeType::Class, name, path);
  const auto type = getTypedefNameDeclType(value);
  addTypeReferencesToNode(type, node, value->getLocation());

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseTypedefDecl(value);
  nodes.pop(node);
  return result;
}

clang::QualType XmlAstVisitor::getTypedefNameDeclType(const clang::TypedefNameDecl* value) const
{
  const auto type = context.getTypeDeclType(value);
  const auto typedefType = type->getAs<clang::TypedefType>();
  const auto qualType = typedefType->desugar();
  return qualType;
}

bool XmlAstVisitor::TraverseFieldDecl(clang::FieldDecl* value)
{
  const auto name = getName(value);
  const auto path = getPath(value);

  Declaration* node = nodes.createForDefinition(value->getFirstDecl(), NodeType::Field, name, path);

  const auto type = value->getType();
  addTypeReferencesToNode(type, node, value->getLocation());

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseFieldDecl(value);
  nodes.pop(node);

  return result;
}

NodeType XmlAstVisitor::variableType(const clang::VarDecl* value) const
{
  const auto isClassMember = value->isCXXClassMember();
  const auto type = isClassMember ? NodeType::Field : NodeType::Variable;
  return type;
}

bool XmlAstVisitor::TraverseVarDecl(clang::VarDecl* value)
{
  const auto isVariableDeclaration = !value->isLocalVarDecl();
  if (isVariableDeclaration) {
    const auto TemplateSpecializationKind = value->getTemplateSpecializationKind();
    switch (TemplateSpecializationKind) {
      case clang::TemplateSpecializationKind::TSK_Undeclared:
      {
        const auto name = getName(value);
        const auto path = getPath(value);
        const auto type = variableType(value);

        const auto node = nodes.createForDefinition(value->getFirstDecl(), type, name, path);

        const auto hasExternalStorage = value->hasExternalStorage();
        if (!hasExternalStorage) {
          // only add type reference once for original definition
          const auto type = value->getType();
          const auto loc = value->getLocation();
          addTypeReferencesToNode(type, node, loc);
        }

        nodes.push(node);
        const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseVarDecl(value);
        nodes.pop(node);

        return result;
      }

      case clang::TemplateSpecializationKind::TSK_ImplicitInstantiation:
      {
        const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseVarDecl(value);
        return result;
      }

      case clang::TemplateSpecializationKind::TSK_ExplicitSpecialization:
        throw NotImplementedException("clang::TemplateSpecializationKind::TSK_ExplicitSpecialization");

      case clang::TemplateSpecializationKind::TSK_ExplicitInstantiationDefinition:
        throw NotImplementedException("clang::TemplateSpecializationKind::TSK_ExplicitInstantiationDefinition");

      case clang::TemplateSpecializationKind::TSK_ExplicitInstantiationDeclaration:
        throw NotImplementedException("clang::TemplateSpecializationKind::TSK_ExplicitInstantiationDeclaration");
    }

  } else {
    const auto name = getName(value);
    const auto type = value->getType();
    addTypeReferencesToNode(type, nodes.current(), value->getLocation());
    const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseVarDecl(value);
    return result;
  }
}

bool XmlAstVisitor::TraverseParmVarDecl(clang::ParmVarDecl* value)
{
  const auto type = value->getType();
  addTypeReferencesToNode(type, nodes.current(), value->getLocation());

  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseParmVarDecl(value);
  return result;
}

bool XmlAstVisitor::TraverseNamespaceDecl(clang::NamespaceDecl* value)
{
  if (value->isAnonymousNamespace()) {
    return RecursiveASTVisitor<XmlAstVisitor>::TraverseNamespaceDecl(value);
  }

  const auto name = getName(value);

  Package* node = nodes.createPackage(value->getFirstDecl(), name);

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseNamespaceDecl(value);
  nodes.pop(node);

  return result;
}

bool XmlAstVisitor::TraverseEnumDecl(clang::EnumDecl* value)
{
  const auto hasElements = value->enumerator_begin() != value->enumerator_end();
  if (isEmptyDeclaration(value, hasElements)) {
    return true;
  }

  const auto name = getName(value);
  const auto path = getPath(value);

  Declaration* node = nodes.createForDefinition(value->getFirstDecl(), NodeType::Class, name, path);

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseEnumDecl(value);
  nodes.pop(node);

  return result;
}

bool XmlAstVisitor::TraverseEnumConstantDecl(clang::EnumConstantDecl* value)
{
  const auto name = getName(value);
  const auto path = getPath(value);

  Declaration* node = nodes.createForDefinition(value->getFirstDecl(), NodeType::Field, name, path);

  nodes.push(node);
  const auto result = RecursiveASTVisitor<XmlAstVisitor>::TraverseEnumConstantDecl(value);
  nodes.pop(node);

  return result;
}

bool XmlAstVisitor::TraverseStaticAssertDecl(clang::StaticAssertDecl*)
{
  return true;
}

const clang::NamedDecl* XmlAstVisitor::getTemplatedMethod(const clang::FunctionDecl* value) const
{
  const auto kind = value->getTemplatedKind();
  switch (kind) {
    case clang::FunctionDecl::TemplatedKind::TK_NonTemplate:
      return value;

    case clang::FunctionDecl::TemplatedKind::TK_MemberSpecialization:
      {
        const auto memberSpecializationInfo = value->getMemberSpecializationInfo();
        eassert(memberSpecializationInfo);
        const auto definition = memberSpecializationInfo->getInstantiatedFrom();
        eassert(definition);
        const auto function = definition->getAsFunction();
        eassert(function);
        return function->getFirstDecl();
      }

    case clang::FunctionDecl::TemplatedKind::TK_FunctionTemplateSpecialization:
      {
        const auto templateInstantiationPattern = value->getTemplateInstantiationPattern();
        eassert(templateInstantiationPattern);
        const clang::FunctionDecl* definition = templateInstantiationPattern->getDefinition();
        eassert(definition);
        const auto describedFunctionTemplate = definition->getDescribedFunctionTemplate();
        eassert(describedFunctionTemplate);
        return describedFunctionTemplate->getFirstDecl();
      }

    case clang::FunctionDecl::TemplatedKind::TK_FunctionTemplate:
      throw NotImplementedException("clang::FunctionDecl::TemplatedKind::TK_FunctionTemplate");
      break;

    case clang::FunctionDecl::TemplatedKind::TK_DependentFunctionTemplateSpecialization:
      throw NotImplementedException("clang::FunctionDecl::TemplatedKind::TK_DependentFunctionTemplateSpecialization");
      break;
  }

  throw ReachedUnreachableException("kind: " + std::to_string(kind));
  return {};
}

const clang::FieldDecl* XmlAstVisitor::getTemplatedField(const clang::FieldDecl* value) const
{
  const auto parent = value->getParent();
  eassert(parent);

  const auto kind = parent->getKind();
  if (kind == clang::Decl::Kind::Record) {
    return value;
  }

  const auto fieldParent = dynamic_cast<const clang::CXXRecordDecl*>(parent);
  eassert(fieldParent);

  const auto pat = fieldParent->getTemplateInstantiationPattern();
  if (pat) {
    //TODO find better way to find original field
    for (const auto& tfield : pat->fields()) {
      if (tfield->getFieldIndex() == value->getFieldIndex()) {
        return tfield;
      }
    }
    throw ReachedUnreachableException("field: " + value->getName().str());
  } else {
    return value;
  }
}

Declaration::Path XmlAstVisitor::getPath(const clang::NamedDecl* value) const
{
  return path.getPath(value);
}

std::string XmlAstVisitor::getName(const clang::NamedDecl* value) const
{
  return info.getName(value);
}

std::pair<const clang::NamedDecl*, NodeType> XmlAstVisitor::memberDecl(const clang::ValueDecl* decl) const
{
  const auto kind = decl->getKind();

  switch (kind) {
    case clang::Decl::Kind::CXXMethod:
    case clang::Decl::Kind::CXXDestructor:
    case clang::Decl::CXXConversion:
    {
      const auto method = dynamic_cast<const clang::CXXMethodDecl*>(decl)->getFirstDecl();
      const auto templated = getTemplatedMethod(method);
      const auto name = getName(templated);
      return {templated, NodeType::Method};
    }
    case clang::Decl::Kind::Field:
    {
      const auto field = dynamic_cast<const clang::FieldDecl*>(decl)->getFirstDecl();
      const auto templated = getTemplatedField(field)->getFirstDecl();
      const auto name = getName(templated);
      return {templated, NodeType::Field};
    }

    case clang::Decl::ParmVar:
      return {decl, NodeType::Variable};

    case clang::Decl::Var:
    {
      const auto varDecl = dynamic_cast<const clang::VarDecl*>(decl);
      eassert(varDecl);
      if (varDecl->isStaticDataMember()) {
        const clang::VarDecl* decl = getOriginalDeclaration(varDecl);
        return {decl->getFirstDecl(), NodeType::Field};
      } else {
        return {varDecl, NodeType::Variable};
      }
    }

    case clang::Decl::EnumConstant:
      return {decl, NodeType::Field};

    case clang::Decl::Function:
      return {decl, NodeType::Function};
  }

  logError("not implemented memberDecl", decl);
  throw NotImplementedException("memberDecl " + std::to_string(kind));

  return {{}, {}};
}

bool XmlAstVisitor::VisitMemberExpr(const clang::MemberExpr* value)
{
  Declaration* parent = nodes.current();

  const clang::ValueDecl* decl = value->getMemberDecl();
  const auto me = memberDecl(decl);
  const auto name = getName(me.first);
  addReferenceIfAllowed(parent, me.first, me.second, name);

  return true;
}

void XmlAstVisitor::addReferenceIfAllowed(Declaration* node, const clang::NamedDecl* decl, NodeType type, const std::string& name) const
{
  const auto inSystem = info.isInSystemHeader(decl);
  const auto isImplicit = decl->isImplicit();
  if (!isImplicit && !inSystem) {
    const auto path = getPath(decl);
    const auto target = nodes.createForReference(decl, type, name, path);
    node->addReference(target);
  }
}

bool XmlAstVisitor::VisitDeclRefExpr(const clang::DeclRefExpr* value)
{
  if (visited.find(value) != visited.end()) { //FIXME find root cause of visiting those 2 times and fix it
    return true;
  }
  visited.insert(value);

  Declaration* parent = nodes.current();

  const clang::ValueDecl* decl = value->getDecl();
  eassert(decl);
  const auto name = getName(decl);

  const auto kind = decl->getKind();
  switch (kind) {
    case clang::Decl::Kind::ParmVar:
    case clang::Decl::Kind::NonTypeTemplateParm:
      break;

    case clang::Decl::Kind::Var: {
      const auto var = dynamic_cast<const clang::VarDecl*>(decl);
      const auto isLocal = var->isLocalVarDecl();
      if (!isLocal) {
        const auto origDecl = getOriginalDeclaration(var);
        const auto first = origDecl->getFirstDecl();
        eassert(first);
        const auto type = variableType(var);
        addReferenceIfAllowed(parent, first, type, name);
      }
      break;
    }

    case clang::Decl::Kind::CXXMethod: {
      const auto method = dynamic_cast<const clang::CXXMethodDecl*>(decl);
      const auto first = dynamic_cast<const clang::CXXMethodDecl*>(method->getFirstDecl());
      const auto isLambda = first->getParent()->isLambda();
      if (!isLambda) {
        visitFunctionRefExpr(first, NodeType::Method);
      }
      break;
    }

    case clang::Decl::Kind::Function: {
      const auto function = dynamic_cast<const clang::FunctionDecl*>(decl);
      visitFunctionRefExpr(function, NodeType::Function);
      break;
    }

    case clang::Decl::Kind::EnumConstant: {
        const auto enumConst = dynamic_cast<const clang::EnumConstantDecl*>(decl);
        eassert(enumConst);
        const auto type = enumConst->getType();
        const auto types = getTypes(type);

        bool isTemplated = false;
        eassert(types.size() <= 1);
        if (types.size() == 1) {
          const clang::EnumDecl* enumDecl = dynamic_cast<const clang::EnumDecl*>(types.at(0));
          eassert(enumDecl);
          isTemplated = enumDecl->isTemplated();
        }

        if (isTemplated) {
          logError("can not handle reference to enum value defined within a template", value->getLocation());
        } else {
          addReferenceIfAllowed(parent, decl, NodeType::Field, name);
        }
      break;
    }

    default: {
      const NodeType type = typeFromKind(kind);
      addReferenceIfAllowed(parent, decl, type, name);
      break;
    }
  }

  return true;
}

const clang::VarDecl* XmlAstVisitor::getOriginalDeclaration(const clang::VarDecl* value) const
{
  const auto kind = value->getTemplateSpecializationKind();

  switch (kind) {
    case clang::TemplateSpecializationKind::TSK_Undeclared:
    case clang::TemplateSpecializationKind::TSK_ExplicitInstantiationDeclaration:
      return value;

    case clang::TemplateSpecializationKind::TSK_ImplicitInstantiation:
    {
      const clang::VarDecl* decl = value->getTemplateInstantiationPattern();
      eassert(decl);
      return decl;
    }

    case clang::TemplateSpecializationKind::TSK_ExplicitSpecialization:
    {
      const auto loc = value->getLocation().printToString(sourceManager);
      throw NotImplementedException("clang::TemplateSpecializationKind::TSK_ExplicitSpecialization @" + loc);
    }

    case clang::TemplateSpecializationKind::TSK_ExplicitInstantiationDefinition:
    {
      const auto loc = value->getLocation().printToString(sourceManager);
      throw NotImplementedException("clang::TemplateSpecializationKind::TSK_ExplicitInstantiationDefinition @" + loc);
    }
  }
}

void XmlAstVisitor::visitFunctionRefExpr(const clang::FunctionDecl* value, NodeType type)
{
  const auto function = value->getFirstDecl();

  Declaration* parent = nodes.current();
  const auto name = getName(function);

  //TODO merge with getTemplatedMethod
  const auto kind = function->getTemplatedKind();
  switch (kind) {
    case clang::FunctionDecl::TK_NonTemplate:
    {
      addReferenceIfAllowed(parent, function, type, name);
      break;
    }
    case clang::FunctionDecl::TK_FunctionTemplate:
    {
      throw NotImplementedException("clang::FunctionDecl::TK_FunctionTemplate");
      break;
    }
    case clang::FunctionDecl::TK_MemberSpecialization:
    {
      const auto memberSpecializationInfo = value->getMemberSpecializationInfo();
      eassert(memberSpecializationInfo);
      const auto definition = memberSpecializationInfo->getInstantiatedFrom();
      eassert(definition);
      const auto function = definition->getAsFunction();
      eassert(function);
      addReferenceIfAllowed(parent, function, type, name);
      break;
    }
    case clang::FunctionDecl::TK_FunctionTemplateSpecialization:
    {
      const clang::FunctionTemplateSpecializationInfo* functionSpecializationInfo = value->getTemplateSpecializationInfo();
      eassert(functionSpecializationInfo);
      const clang::FunctionTemplateDecl* function = functionSpecializationInfo->getTemplate();
      eassert(function);
      const auto memberTemplate = function->getInstantiatedFromMemberTemplate();
      const clang::FunctionTemplateDecl* templ = memberTemplate ? memberTemplate : function;
      const auto first = templ->getFirstDecl();
      eassert(first);

      const auto templName = getName(first);
      addReferenceIfAllowed(parent, first, type, templName);

      const auto args = functionSpecializationInfo->TemplateArguments;
      eassert(args);
      for (const clang::TemplateArgument& itr : args->asArray()) {
        addTemplateArgumentToNode(itr, parent, value->getLocation());
      }
      break;
    }
    case clang::FunctionDecl::TK_DependentFunctionTemplateSpecialization:
    {
      throw NotImplementedException("clang::FunctionDecl::TK_DependentFunctionTemplateSpecialization");
      break;
    }
  }
}

void XmlAstVisitor::addReference(Declaration* node, const clang::ValueDecl* target)
{
  const auto name = getName(target);
  const auto kind = target->getKind();
  const NodeType type = typeFromKind(kind);

  addReferenceIfAllowed(node, target, type, name);
}

bool XmlAstVisitor::VisitExplicitCastExpr(const clang::ExplicitCastExpr* value)
{
  if (visited.find(value) != visited.end()) { //FIXME find root cause of visiting those 2 times and fix it
    return true;
  }
  visited.insert(value);

  addTypeReferencesToExpr(value);
  return true;
}

bool XmlAstVisitor::VisitCXXNewExpr(const clang::CXXNewExpr* value)
{
  if (visited.find(value) != visited.end()) { //FIXME find root cause of visiting those 2 times and fix it
    return true;
  }
  visited.insert(value);

  addTypeReferencesToExpr(value);

  return true;
}

bool XmlAstVisitor::VisitCXXConstructExpr(const clang::CXXConstructExpr* value)
{
  const auto constructor = getTemplatedMethod(value->getConstructor()->getFirstDecl());
  const auto name = getName(constructor);
  const auto parent = nodes.current();
  addReferenceIfAllowed(parent, constructor, NodeType::Method, name);

  return true;
}

bool XmlAstVisitor::VisitCXXDeleteExpr(const clang::CXXDeleteExpr* value)
{
  addTypeReferencesToExpr(value);

  const auto destroyedType = value->getDestroyedType();
  if (destroyedType.isNull()) {
    return true;
  }

  if (!destroyedType->isRecordType()) {
    return true;
  }

  const auto record = destroyedType->getAsCXXRecordDecl();
  eassert(record);
  auto destructor = record->getDestructor();
  if (destructor) {
    const auto decl = getTemplatedMethod(destructor->getFirstDecl());
    const auto name = getName(decl);
    const auto parent = nodes.current();
    addReferenceIfAllowed(parent, decl, NodeType::Method, name);
  }

  return true;
}

bool XmlAstVisitor::VisitUnresolvedLookupExpr(const clang::UnresolvedLookupExpr* value)
{
  const auto parent = nodes.current();

  for (const auto& decl : value->decls()) {
    const auto name = getName(decl);
    const auto type = decl->isCXXClassMember() ? NodeType::Method : NodeType::Function;
    addReferenceIfAllowed(parent, decl, type, name);
  }

  for (const auto& argLoc : value->template_arguments()) {
    const clang::TemplateArgument& arg = argLoc.getArgument();
    addTemplateArgumentToNode(arg, parent, value->getBeginLoc());
  }

  return true;
}

void XmlAstVisitor::addTypeReferencesToExpr(const clang::Expr* value)
{
  const auto parent = nodes.current();
  const auto type = value->getType();
  addTypeReferencesToNode(type, parent, value->getExprLoc());
}

void XmlAstVisitor::addTypeReferencesToNode(const clang::QualType& qualType, Declaration* node, const clang::SourceLocation& loc) const
{
  const auto types = getTypes(qualType);
  for (const auto& type : types) {
    if (type) {
      const auto name = getName(type);
      addReferenceIfAllowed(node, type, NodeType::Class, name);
    } else {
      const auto name = qualType.getAsString();
      logError("can not resolve type: " + name, loc);
    }
  }
}

void XmlAstVisitor::addTemplateArgumentToNode(const clang::TemplateArgument& arg, Declaration* node, const clang::SourceLocation& loc) const
{
  if (arg.getKind() == clang::TemplateArgument::ArgKind::Type) {
    const auto type = arg.getAsType();
    addTypeReferencesToNode(type, node, loc);
  }
}

void XmlAstVisitor::logError(const std::string& message, const clang::Decl* decl) const
{
  const auto location = decl->getLocation();
  logError(message, location);
}

void XmlAstVisitor::logError(const std::string& message, const clang::SourceLocation& location) const
{
  const auto strLoc = location.printToString(sourceManager);
  logger.error(strLoc + ": " + message);
}
