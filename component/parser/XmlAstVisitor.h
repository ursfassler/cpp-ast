/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/ast/Node.h"
#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/AST/Mangle.h>
#include <clang/AST/ASTContext.h>
#include <clang/Basic/SourceManager.h>
#include <map>

class Nodes;
class Logger;
class DeclInfo;
class DeclPath;

class XmlAstVisitor :
    public clang::RecursiveASTVisitor<XmlAstVisitor>
{
public:
  XmlAstVisitor(
      const DeclPath&,
      const DeclInfo&,
      clang::ASTContext&,
      clang::SourceManager&,
      Nodes&,
      Logger&
      );

  bool shouldVisitTemplateInstantiations() const;
  bool shouldVisitImplicitCode() const;

  const Nodes& getNodes() const;

  bool TraverseDecl(clang::Decl*);
  bool TraverseClassTemplateDecl(clang::ClassTemplateDecl*);
  bool VisitNonTypeTemplateParmDecl(const clang::NonTypeTemplateParmDecl*);
  bool TraverseCXXRecordDecl(clang::CXXRecordDecl*);
  bool TraverseRecordDecl(clang::RecordDecl*);
  bool TraverseCXXMethodDecl(clang::CXXMethodDecl*);
  bool TraverseClassTemplatePartialSpecializationDecl(clang::ClassTemplatePartialSpecializationDecl*);
  bool TraverseClassTemplateSpecializationDecl(clang::ClassTemplateSpecializationDecl*);
  bool TraverseCXXConstructorDecl(clang::CXXConstructorDecl*);
  bool TraverseCXXDestructorDecl(clang::CXXDestructorDecl*);
  bool TraverseCXXConversionDecl(clang::CXXConversionDecl*);
  bool TraverseFriendDecl(clang::FriendDecl*);
  bool TraverseFunctionTemplateDecl(clang::FunctionTemplateDecl*);
  bool TraverseFunctionDecl(clang::FunctionDecl*);
  bool TraverseRedeclarableTemplateDecl(clang::RedeclarableTemplateDecl*);
  bool TraverseVarTemplateDecl(clang::VarTemplateDecl*);
  bool TraverseTypeAliasTemplateDecl(clang::TypeAliasTemplateDecl*);
  bool TraverseTypeAliasDecl(clang::TypeAliasDecl*);
  bool TraverseTypedefDecl(clang::TypedefDecl*);
  bool TraverseFieldDecl(clang::FieldDecl*);
  bool TraverseVarDecl(clang::VarDecl*);
  bool TraverseParmVarDecl(clang::ParmVarDecl*);
  bool TraverseNamespaceDecl(clang::NamespaceDecl*);
  bool TraverseEnumDecl(clang::EnumDecl*);
  bool TraverseEnumConstantDecl(clang::EnumConstantDecl*);
  bool TraverseStaticAssertDecl(clang::StaticAssertDecl*);
  bool VisitMemberExpr(const clang::MemberExpr*);
  bool VisitDeclRefExpr(const clang::DeclRefExpr*);
  bool VisitExplicitCastExpr(const clang::ExplicitCastExpr*);
  bool VisitCXXNewExpr(const clang::CXXNewExpr*);
  bool VisitCXXConstructExpr(const clang::CXXConstructExpr*);
  bool VisitCXXDeleteExpr(const clang::CXXDeleteExpr*);
  bool VisitUnresolvedLookupExpr(const clang::UnresolvedLookupExpr*);

private:
  const DeclPath& path;
  const DeclInfo& info;
  clang::ASTContext& context;
  clang::SourceManager& sourceManager;
  Nodes& nodes;
  std::set<const clang::Expr*> visited{};  // workaround since the traverser visits those elements 2 times
  std::set<const clang::Decl*> visitedDecl{};
  Logger& logger;

  void addTypeReferencesToNode(const clang::QualType &qualType, Declaration *node, const clang::SourceLocation&) const;
  void addTemplateArgumentToNode(const clang::TemplateArgument&, Declaration*, const clang::SourceLocation&) const;
  Declaration *handleMethod(const clang::CXXMethodDecl*);
  void addTypeReferencesToExpr(const clang::Expr*);
  void addReference(Declaration *node, const clang::ValueDecl* target);
  void addReferenceIfAllowed(Declaration*, const clang::NamedDecl*, NodeType, const std::string&) const;
  void visitFunctionRefExpr(const clang::FunctionDecl*, NodeType);
  std::pair<const clang::NamedDecl*, NodeType> memberDecl(const clang::ValueDecl*) const;
  std::string getName(const clang::NamedDecl*) const;
  clang::QualType getTypedefNameDeclType(const clang::TypedefNameDecl*) const;
  NodeType variableType(const clang::VarDecl*) const;
  const clang::NamedDecl *getTemplatedMethod(const clang::FunctionDecl*) const;
  const clang::FieldDecl *getTemplatedField(const clang::FieldDecl*) const;
  Declaration::Path getPath(const clang::NamedDecl*) const;
  void logError(const std::string&, const clang::Decl*) const;
  void logError(const std::string&, const clang::SourceLocation&) const;
  const clang::VarDecl* getOriginalDeclaration(const clang::VarDecl*) const;
  bool isEmptyDeclaration(const clang::TagDecl*, bool hasElements) const;
};
