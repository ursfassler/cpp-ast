/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "commands.h"

#include <nlohmann/json.hpp>
#include <unordered_set>

std::vector<std::string> commands(std::istream& stream)
{
  nlohmann::json json;
  stream >> json;

  std::vector<std::string> result{};

  std::unordered_set<std::string> inserted{};
  for (const auto& entry : json) {
    const auto file = entry["file"];
    if (inserted.find(file) == inserted.end()) {
      inserted.emplace(file);
      result.push_back(file);
    }
  }

  return result;
}
