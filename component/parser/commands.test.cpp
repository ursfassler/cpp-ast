/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "commands.h"
#include <gtest/gtest.h>
#include <sstream>


namespace
{

using namespace testing;


class Commands_Test :
    public Test
{
  public:
};

TEST_F(Commands_Test, return_list_of_source_files)
{
  std::stringstream json {R"json(
  [
  {
    "directory": "/metric/build-codeflight-Qt5-Default",
    "command": "/usr/bin/g++-8   -I/metric/codeflight/codeflight -I/metric/codeflight/. -I/metric/codeflight/feature-tests -I/metric/codeflight/unit-tests  -g   -Wall -Wextra -pedantic -std=gnu++17 -o CMakeFiles/unit-tests.dir/artefact/Path.cpp.o -c /metric/codeflight/artefact/Path.cpp",
    "file": "/metric/codeflight/artefact/Path.cpp"
  },

  {
    "directory": "/metric/build-codeflight-Qt5-Default",
    "command": "/usr/bin/g++-8   -I/metric/codeflight/codeflight -I/metric/codeflight/. -I/metric/codeflight/feature-tests -I/metric/codeflight/unit-tests  -g   -Wall -Wextra -pedantic -std=gnu++17 -o CMakeFiles/unit-tests.dir/artefact/Path.test.cpp.o -c /metric/codeflight/artefact/Path.test.cpp",
    "file": "/metric/codeflight/artefact/Path.test.cpp"
  },

  {
    "directory": "/metric/build-codeflight-Qt5-Default",
    "command": "/usr/bin/g++-8   -I/metric/codeflight/codeflight -I/metric/codeflight/. -I/metric/codeflight/feature-tests -I/metric/codeflight/unit-tests  -g   -Wall -Wextra -pedantic -std=gnu++17 -o CMakeFiles/unit-tests.dir/artefact/unit-test/SimpleArtefact.cpp.o -c /metric/codeflight/artefact/unit-test/SimpleArtefact.cpp",
    "file": "/metric/codeflight/artefact/unit-test/SimpleArtefact.cpp"
  }
  ]
  )json"};
  const std::vector<std::string> expected{
    "/metric/codeflight/artefact/Path.cpp",
    "/metric/codeflight/artefact/Path.test.cpp",
    "/metric/codeflight/artefact/unit-test/SimpleArtefact.cpp",
  };

  const auto actual = commands(json);

  ASSERT_EQ(expected, actual);
}

TEST_F(Commands_Test, removes_duplicates)
{
  std::stringstream json {R"json(
  [
  {
    "directory": "/metric/build-codeflight-Qt5-Default",
    "command": "/usr/bin/g++-8   -I/metric/codeflight/codeflight -I/metric/codeflight/. -I/metric/codeflight/feature-tests -I/metric/codeflight/unit-tests  -g   -Wall -Wextra -pedantic -std=gnu++17 -o CMakeFiles/unit-tests.dir/artefact/Path.cpp.o -c /metric/codeflight/artefact/Path.cpp",
    "file": "/metric/codeflight/artefact/Path.cpp"
  },

  {
    "directory": "/metric/build-codeflight-Qt5-Default",
    "command": "/usr/bin/g++-8   -I/metric/codeflight/codeflight -I/metric/codeflight/. -I/metric/codeflight/feature-tests -I/metric/codeflight/unit-tests  -g   -Wall -Wextra -pedantic -std=gnu++17 -o CMakeFiles/unit-tests.dir/artefact/Path.test.cpp.o -c /metric/codeflight/artefact/Path.test.cpp",
    "file": "/metric/codeflight/artefact/Path.test.cpp"
  },

  {
    "directory": "/metric/build-codeflight-Qt5-Default",
    "command": "/usr/bin/g++-8   -I/metric/codeflight/codeflight -I/metric/codeflight/. -I/metric/codeflight/feature-tests -I/metric/codeflight/unit-tests  -g   -Wall -Wextra -pedantic -std=gnu++17 -o CMakeFiles/unit-tests.dir/artefact/unit-test/SimpleArtefact.cpp.o -c /metric/codeflight/artefact/unit-test/SimpleArtefact.cpp",
    "file": "/metric/codeflight/artefact/Path.cpp"
  }
  ]
  )json"};
  const std::vector<std::string> expected{
    "/metric/codeflight/artefact/Path.cpp",
    "/metric/codeflight/artefact/Path.test.cpp",
  };

  const auto actual = commands(json);

  ASSERT_EQ(expected, actual);
}


}
