/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "exceptions.h"


NotImplementedException::NotImplementedException(const std::string &functionality_) :
  functionality{functionality_}
{
}

const char *NotImplementedException::what() const noexcept
{
  return "not implemented";
}

std::string NotImplementedException::message() const
{
  return "not implemented: " + functionality;
}


ReachedUnreachableException::ReachedUnreachableException(const std::string& reason_) :
  reason{reason_}
{
}

const char *ReachedUnreachableException::what() const noexcept
{
  return "reached unreachable code";
}

std::string ReachedUnreachableException::message() const
{
  return "unreachable code: " + reason;
}


AssertionFailedException::AssertionFailedException(const std::string &assertion_) :
  assertion{assertion_}
{
}

const char *AssertionFailedException::what() const noexcept
{
  return "assertion failed";
}

std::string AssertionFailedException::message() const
{
  return "assertion failed: " + assertion;
}

void iassert(bool predicate, const std::string& assertion, const std::string& file, int line)
{
  if (!predicate) {
    throw AssertionFailedException(assertion + "@" + file + ":" + std::to_string(line));
  }
}


ErrorAtException::ErrorAtException(const std::string &message_, const std::string &location_) :
  message{message_},
  location{location_}
{
}

const char *ErrorAtException::what() const noexcept
{
  return "error at";
}
