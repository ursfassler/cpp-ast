/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <exception>
#include <string>

class BasicErrorException :
    public std::exception
{
  public:
    virtual std::string message() const = 0;
};

class NotImplementedException :
    public BasicErrorException
{
  public:
    NotImplementedException(const std::string& functionality);

    const char* what() const noexcept override;
    std::string message() const override;

  private:
    const std::string functionality;

};


class ReachedUnreachableException :
    public BasicErrorException
{
  public:
    ReachedUnreachableException(const std::string& reason);

    const char* what() const noexcept override;
    std::string message() const override;

  private:
    const std::string reason;

};


class AssertionFailedException :
    public BasicErrorException
{
  public:
    AssertionFailedException(const std::string& assertion);

    const char* what() const noexcept override;
    std::string message() const override;

  private:
    const std::string assertion;

};

void iassert(bool, const std::string&, const std::string& file, int line);

#define eassert(p) \
  do { \
    iassert(p, #p, __FILE__, __LINE__); \
  } while(false) \



class ErrorAtException :
    public std::exception
{
  public:
    ErrorAtException(const std::string& message, const std::string& location);

    const char* what() const noexcept override;

    const std::string message;
    const std::string location;

};

