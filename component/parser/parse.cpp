/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "parse.h"
#include "DeclInfo.h"
#include "DeclPath.h"
#include "XmlAstVisitor.h"
#include "component/ast/Node.h"
#include "component/ast/Nodes.h"
#include "component/ast/logger.h"
#include "component/linker/link.h"
#include "component/writer/NodePrinter.h"
#include "component/writer/NumberedId.h"
#include "component/writer/Writer.h"
#include "exceptions.h"
#include <clang/Tooling/Tooling.h>
#include <iostream>


bool parse(const std::vector<std::string>& files, const clang::tooling::CompilationDatabase& db, std::ostream& stream, Logger& logger)
{
  std::vector<Nodes*> nodes{};

  try {
    std::size_t number = 0;
    for (const auto& file : files) {
      number++;
      std::cout << "parse " << number << "/" << files.size() << ": " << file << std::endl;

      clang::tooling::ClangTool Tool{db, {file}};

      std::vector<std::unique_ptr<clang::ASTUnit>> asts{};
      Tool.buildASTs(asts);

      for (const auto& ast : asts) {
        const auto filename = ast->getOriginalSourceFileName().str();
        if (filename.empty()) {
          throw std::runtime_error("got file without a filename");
        }

        DeclInfo info{ast->getLangOpts(), ast->getSourceManager()};
        DeclPath declPath{filename, info, ast->getSourceManager()};

        Nodes* node = new Nodes();
        nodes.push_back(node);
        node->createRoot(filename);

        XmlAstVisitor visitor{declPath, info, ast->getASTContext(), ast->getSourceManager(), *node, logger};
        visitor.TraverseDecl(ast->getASTContext().getTranslationUnitDecl());
      }
    }
  } catch (ErrorAtException eae) {
    logger.error(eae.location + ": " + eae.message);
    return false;
  }


  std::cout << "start linking" << std::endl;

  Root out{{}};
  for (auto& node : nodes) {
    std::cout << "  link " << node->root()->name << std::endl;
    linkInto(&out, node->root(), logger);
  }

  std::cout << "start printing" << std::endl;

  NumberedId ids{out};
  NodePrinter printer{&out, ids.idGetter(), NodePrinter::NoPath, logger};

  xml::Writer writer{stream};
  printer.print(writer);

  std::cout << "done" << std::endl;

  return true;
}
