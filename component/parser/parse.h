/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <clang/Tooling/CompilationDatabase.h>
#include <ostream>

class Logger;

bool parse(const std::vector<std::string>&, const clang::tooling::CompilationDatabase&, std::ostream&, Logger&);
