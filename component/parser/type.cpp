/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "type.h"
#include "exceptions.h"
#include <clang/AST/DeclCXX.h>
#include <clang/AST/DeclTemplate.h>
#include <clang/AST/PrettyPrinter.h>


std::vector<const clang::NamedDecl *> getTypes(const clang::ArrayRef<clang::TemplateArgument>& args)
{
  std::vector<const clang::NamedDecl *> result{};

  for (const clang::TemplateArgument& itr : args) {
    //TODO merge with function template
    const auto kind = itr.getKind();
    switch (kind) {
      case clang::TemplateArgument::ArgKind::Type:
      {
        const auto type = itr.getAsType();
        const auto types = getTypes(type);
        result.insert(result.end(), types.cbegin(), types.cend());
        break;
      }

      case clang::TemplateArgument::ArgKind::Template:
      {
        const clang::TemplateName templ = itr.getAsTemplate();
        const clang::TemplateDecl* decl = templ.getAsTemplateDecl();
        const clang::NamedDecl* named = decl->getUnderlyingDecl();
        result.push_back(named);
        break;
      }

      case clang::TemplateArgument::ArgKind::Expression:
      case clang::TemplateArgument::ArgKind::Integral:
      case clang::TemplateArgument::ArgKind::Pack:
      {
        break;
      }

      case clang::TemplateArgument::ArgKind::Null:
      case clang::TemplateArgument::ArgKind::Declaration:
      case clang::TemplateArgument::ArgKind::NullPtr:
      case clang::TemplateArgument::ArgKind::TemplateExpansion:
      {
        throw NotImplementedException("clang::TemplateArgument::ArgKind::" + std::to_string(kind)); break;
      }
    }
  }

  return result;
}

std::vector<const clang::NamedDecl *> getTypes(const clang::ArrayRef<clang::QualType>& args)
{
  std::vector<const clang::NamedDecl *> result{};

  for (const clang::QualType& itr : args) {
    const auto tt = getTypes(itr);
    result.insert(result.end(), tt.begin(), tt.end());
  }

  return result;
}

std::vector<const clang::NamedDecl *> getTypes(const clang::QualType& qualType)
{
  eassert(!qualType.isNull());

  const auto typeClass = qualType->getTypeClass();
  switch (typeClass) {
    case clang::Type::TypeClass::Builtin:
    {
      const auto spec = qualType->getAs<clang::BuiltinType>();
      eassert(spec);

      const auto kind = spec->getKind();
      if (kind == clang::BuiltinType::Dependent) {
        return {{}};
      }

      return {};
    }

    case clang::Type::TypeClass::LValueReference:
    case clang::Type::TypeClass::RValueReference:
    {
      const auto spec = qualType->getAs<clang::ReferenceType>();
      eassert(spec);

      return getTypes(spec->getPointeeType());
    }

    case clang::Type::TypeClass::Pointer:
    {
      const auto spec = qualType->getAs<clang::PointerType>();
      eassert(spec);

      return getTypes(spec->getPointeeType());
    }

    case clang::Type::TypeClass::ConstantArray:
    case clang::Type::TypeClass::DependentSizedArray:
    case clang::Type::TypeClass::IncompleteArray:
    case clang::Type::TypeClass::VariableArray:
    {
      const auto spec = qualType->getAsArrayTypeUnsafe();
      eassert(spec);

      return getTypes(spec->getElementType());
    }

    case clang::Type::TypeClass::SubstTemplateTypeParm:
    {
      const auto substs = qualType->getAs<clang::SubstTemplateTypeParmType>();
      eassert(substs);

      return getTypes(substs->getReplacementType());
    }

    case clang::Type::TypeClass::Auto:
    {
      const auto deducedType = qualType->getAs<clang::AutoType>();
      eassert(deducedType);

      const auto deduced = deducedType->getDeducedType();
      if (deduced.isNull()) {
        //TODO probably a const auto within a template
        return {};
      }
      eassert(!deduced.isNull());
      return getTypes(deduced);
    }

    case clang::Type::TypeClass::DeducedTemplateSpecialization:
    {
      const clang::DeducedTemplateSpecializationType* deducedType = qualType->getAs<clang::DeducedTemplateSpecializationType>();
      eassert(deducedType);

      const auto deduced = deducedType->getDeducedType();
      if (deduced.isNull()) {
        const auto templateName = deducedType->getTemplateName();
        const clang::TemplateDecl* templateDecl = templateName.getAsTemplateDecl();
        eassert(templateDecl);
        return {templateDecl};
      }
      eassert(!deduced.isNull());
      return getTypes(deduced);
    }

    case clang::Type::TypeClass::Elaborated:
    {
      const auto elaboratedType = qualType->getAs<clang::ElaboratedType>();
      eassert(elaboratedType);

      const auto qualifier = elaboratedType->getQualifier();
      if (qualifier) {
        const clang::NestedNameSpecifier::SpecifierKind kind = qualifier->getKind();

        switch (kind) {
          case clang::NestedNameSpecifier::SpecifierKind::TypeSpec:
            {
              const auto qType = qualifier->getAsType();
              eassert(qType);
              const auto qTypePtrClass = qType->getTypeClass();
              switch (qTypePtrClass) {
                case clang::Type::TemplateSpecialization:
                  {
                    const auto canon = qualType.getCanonicalType();
                    const auto canonClass = canon->getTypeClass();
                    if (canonClass == clang::Type::TypeClass::Record) {
                      const clang::RecordType* recordType = canon->getAs<clang::RecordType>();
                      eassert(recordType);

                      const clang::RecordDecl* decl = recordType->getAsRecordDecl();
                      eassert(decl);

                      const auto kind = decl->getKind();
                      if (kind == clang::Decl::Kind::ClassTemplateSpecialization) {
                        return {{}};
                      }
                    }
                    return getTypes(canon);
                  }

                case clang::Type::Typedef:
                  // This is a workaround. The type pointed to a implicit template instantiation
                  return getTypes(qualType.getCanonicalType());

                default:
                  break;
              }
              break;
            }
          case clang::NestedNameSpecifier::SpecifierKind::Namespace:
          case clang::NestedNameSpecifier::SpecifierKind::Global:
          case clang::NestedNameSpecifier::SpecifierKind::NamespaceAlias:
            break;
          case clang::NestedNameSpecifier::SpecifierKind::Super:
            throw NotImplementedException("clang::NestedNameSpecifier::SpecifierKind::Super"); break;
          case clang::NestedNameSpecifier::SpecifierKind::Identifier:
            throw NotImplementedException("clang::NestedNameSpecifier::SpecifierKind::Identifier"); break;
          case clang::NestedNameSpecifier::SpecifierKind::TypeSpecWithTemplate:
            throw NotImplementedException("clang::NestedNameSpecifier::SpecifierKind::TypeSpecWithTemplate"); break;
        }
      }

      const auto namedType = elaboratedType->getNamedType();
      return getTypes(namedType);
    }

    case clang::Type::TypeClass::TemplateSpecialization:
    {
      const auto templateSpecializationType = qualType->getAs<clang::TemplateSpecializationType>();
      eassert(templateSpecializationType);

      const auto templateName = templateSpecializationType->getTemplateName();
      const auto templateDecl = templateName.getAsTemplateDecl();

      const auto redecl = dynamic_cast<const clang::RedeclarableTemplateDecl*>(templateDecl);
      const auto decl = redecl ? redecl->getFirstDecl() : templateDecl;

      std::vector<const clang::NamedDecl *> result{};
      result.push_back(decl);

      const auto& args = templateSpecializationType->template_arguments();
      const auto specTypes = getTypes(args);
      result.insert(result.end(), specTypes.cbegin(), specTypes.cend());

      return result;
    }

    case clang::Type::TypeClass::Record:
    {
      const clang::RecordType* recordType = qualType->getAs<clang::RecordType>();
      eassert(recordType);

      const clang::RecordDecl* decl = recordType->getAsRecordDecl();
      eassert(decl);

      const auto kind = decl->getKind();

      if (kind == clang::Decl::Kind::CXXRecord) {
        const auto cxxdecl = dynamic_cast<const clang::CXXRecordDecl*>(decl);
        eassert(cxxdecl);

        const clang::MemberSpecializationInfo* memspecinfo = cxxdecl->getMemberSpecializationInfo();
        if (memspecinfo) {
          const auto src = memspecinfo->getInstantiatedFrom();
          return {src};
        }
      }

      if (kind == clang::Decl::Kind::ClassTemplateSpecialization) {
        const auto cts = dynamic_cast<const clang::ClassTemplateSpecializationDecl*>(decl);

        const auto templ = cts->getSpecializedTemplate();
        eassert(templ);
        eassert(templ->getKind() != clang::Decl::Kind::ClassTemplateSpecialization);
        std::vector<const clang::NamedDecl *> result{templ};

        const auto& args = cts->getTemplateInstantiationArgs().asArray();
        const auto specTypes = getTypes(args);
        result.insert(result.end(), specTypes.cbegin(), specTypes.cend());

        return result;
      }

      return {decl->getFirstDecl()};
    }

    case clang::Type::TypeClass::Enum:
    {
      const auto enumType = qualType->getAs<clang::EnumType>();
      eassert(enumType);

      const auto decl = enumType->getDecl();
      eassert(decl);

      const auto TemplateSpecializationKind = decl->getTemplateSpecializationKind();

      if (TemplateSpecializationKind == clang::TemplateSpecializationKind::TSK_ImplicitInstantiation) {
        const auto templDecl = decl->getTemplateInstantiationPattern();
        eassert(templDecl);
        return {templDecl};
      } else {
        return {decl};
      }
    }

    case clang::Type::TypeClass::MemberPointer:
    {
      const auto memberPointerType = qualType->getAs<clang::MemberPointerType>();
      eassert(memberPointerType);

      const auto klass = memberPointerType->getClass();
      eassert(klass);
      const auto canType = klass->getCanonicalTypeInternal();
      const auto funcTypes = getTypes(canType);

      const auto pointee = memberPointerType->getPointeeType();
      eassert(!pointee.isNull());
      const auto argTypes = getTypes(pointee);

      std::vector<const clang::NamedDecl *> result{};
      result.insert(result.end(), funcTypes.begin(), funcTypes.end());
      result.insert(result.end(), argTypes.begin(), argTypes.end());

      return result;
    }

    case clang::Type::TypeClass::FunctionProto:
    {
      const auto functionProtoType = qualType->getAs<clang::FunctionProtoType>();
      eassert(functionProtoType);

      return getTypes(functionProtoType->getParamTypes());
    }

    case clang::Type::TypeClass::TemplateTypeParm:
    {
      const auto templateTypeParmType = qualType->getAs<clang::TemplateTypeParmType>();
      eassert(templateTypeParmType);

      const auto isCanonicalUnqualified = templateTypeParmType->isCanonicalUnqualified();
      if (isCanonicalUnqualified) {
        return {};
      } else {
        const clang::TemplateTypeParmDecl* decl = templateTypeParmType->getDecl();
        eassert(decl);
        return {decl};
      }
    }

    case clang::Type::TypeClass::InjectedClassName:
    {
      const auto injectedClassNameType = qualType->getAs<clang::InjectedClassNameType>();
      eassert(injectedClassNameType);

      const auto templName = injectedClassNameType->getTemplateName();
      const auto decl = templName.getAsTemplateDecl();
      eassert(decl);
      return {decl};
    }

    case clang::Type::TypeClass::Paren:
    {
      const auto parenType = qualType->getAs<clang::ParenType>();
      eassert(parenType);

      return getTypes(parenType->getInnerType());
    }

    case clang::Type::TypeClass::Typedef:
    {
      const auto typedefType = qualType->getAs<clang::TypedefType>();
      eassert(typedefType);
      const clang::TypedefNameDecl* decl = typedefType->getDecl()->getFirstDecl();
      eassert(decl);
      return {decl};
    }

    case clang::Type::TypeClass::DependentName:
    {
      const auto spec = qualType->getAs<clang::DependentNameType>();
      eassert(spec);

      const auto qualifier = spec->getQualifier();
      const auto kind = qualifier->getKind();
      eassert(kind == clang::NestedNameSpecifier::TypeSpec);
      const auto type = qualifier->getAsType();
      const auto qt = type->getCanonicalTypeInternal();

      return getTypes(qt);
    }

    case clang::Type::TypeClass::PackExpansion:
    {
      const auto spec = qualType->getAs<clang::PackExpansionType>();
      eassert(spec);

      return getTypes(spec->getPattern());
    }

    case clang::Type::TypeClass::Decayed:
    {
      const auto spec = qualType->getAs<clang::DecayedType>();
      eassert(spec);

      return getTypes(spec->getDecayedType());
    }

    case clang::Type::TypeClass::Decltype:
    {
      const auto spec = qualType->getAs<clang::DecltypeType>();
      eassert(spec);

      return getTypes(spec->getUnderlyingType());
    }

    case clang::Type::TypeClass::Using:
    {
      const auto spec = qualType->getAs<clang::UsingType>();
      eassert(spec);

      return getTypes(spec->getUnderlyingType());
    }

    case clang::Type::TypeClass::BitInt:
      throw NotImplementedException("clang::Type::TypeClass::BitInt"); break;

    case clang::Type::TypeClass::DependentBitInt:
      throw NotImplementedException("clang::Type::TypeClass::DependentBitInt"); break;

    case clang::Type::TypeClass::MacroQualified:
      throw NotImplementedException("clang::Type::TypeClass::MacroQualified"); break;

    case clang::Type::TypeClass::ConstantMatrix:
      throw NotImplementedException("clang::Type::TypeClass::ConstantMatrix"); break;

    case clang::Type::TypeClass::DependentSizedMatrix:
      throw NotImplementedException("clang::Type::TypeClass::DependentSizedMatrix"); break;

    case clang::Type::TypeClass::Atomic:
      throw NotImplementedException("clang::Type::TypeClass::Atomic"); break;

    case clang::Type::TypeClass::Pipe:
      throw NotImplementedException("clang::Type::TypeClass::Pipe"); break;

    case clang::Type::TypeClass::ObjCObjectPointer:
      throw NotImplementedException("clang::Type::TypeClass::ObjCObjectPointer"); break;

    case clang::Type::TypeClass::ObjCTypeParam:
      throw NotImplementedException("clang::Type::TypeClass::ObjCTypeParam"); break;

    case clang::Type::TypeClass::ObjCInterface:
      throw NotImplementedException("clang::Type::TypeClass::ObjCInterface"); break;

    case clang::Type::TypeClass::ObjCObject:
      throw NotImplementedException("clang::Type::TypeClass::ObjCObject"); break;

    case clang::Type::TypeClass::DependentTemplateSpecialization:
      throw NotImplementedException("clang::Type::TypeClass::DependentTemplateSpecialization"); break;

    case clang::Type::TypeClass::SubstTemplateTypeParmPack:
      throw NotImplementedException("clang::Type::TypeClass::SubstTemplateTypeParmPack"); break;

    case clang::Type::TypeClass::Attributed:
      throw NotImplementedException("clang::Type::TypeClass::Attributed"); break;

    case clang::Type::TypeClass::UnaryTransform:
      throw NotImplementedException("clang::Type::TypeClass::UnaryTransform"); break;

    case clang::Type::TypeClass::TypeOfExpr:
      throw NotImplementedException("clang::Type::TypeClass::TypeOfExpr"); break;

    case clang::Type::TypeClass::TypeOf:
      throw NotImplementedException("clang::Type::TypeClass::TypeOf"); break;

    case clang::Type::TypeClass::Vector:
      throw NotImplementedException("clang::Type::TypeClass::Vector"); break;

    case clang::Type::TypeClass::ExtVector:
      throw NotImplementedException("clang::Type::TypeClass::ExtVector"); break;

    case clang::Type::TypeClass::FunctionNoProto:
      throw NotImplementedException("clang::Type::TypeClass::FunctionNoProto"); break;

    case clang::Type::TypeClass::UnresolvedUsing:
      throw NotImplementedException("clang::Type::TypeClass::UnresolvedUsing"); break;

    case clang::Type::TypeClass::Adjusted:
      throw NotImplementedException("clang::Type::TypeClass::Adjusted"); break;

    case clang::Type::TypeClass::DependentSizedExtVector:
      throw NotImplementedException("clang::Type::TypeClass::DependentSizedExtVector"); break;

    case clang::Type::TypeClass::DependentVector:
      throw NotImplementedException("clang::Type::TypeClass::DependentVector"); break;

    case clang::Type::TypeClass::DependentAddressSpace:
      throw NotImplementedException("clang::Type::TypeClass::DependentAddressSpace"); break;

    case clang::Type::TypeClass::Complex:
      throw NotImplementedException("clang::Type::TypeClass::Complex"); break;

    case clang::Type::TypeClass::BlockPointer:
      throw NotImplementedException("clang::Type::TypeClass::BlockPointer"); break;
  }

  throw ReachedUnreachableException(std::string{"type class "} + qualType->getTypeClassName());
  return {};
}

NodeType typeFromKind(clang::Decl::Kind value)
{
  switch (value) {
    case clang::Decl::Field:
      return NodeType::Field;

    case clang::Decl::ParmVar:
    case clang::Decl::Var:
      return NodeType::Variable;

    case clang::Decl::CXXMethod:
    case clang::Decl::CXXConversion:
      return NodeType::Method;

    case clang::Decl::EnumConstant:
      return NodeType::Field;

    case clang::Decl::Function:
      return NodeType::Function;

    default:
      throw NotImplementedException("typeFromKind: " + std::to_string(value));
  }

  throw NotImplementedException("typeFromKind: " + std::to_string(value));
}




std::string removeSpacesInFrontOf(const std::string& string, const std::string& symbol)
{
  std::string result = string;

  const std::string toFind = " " + symbol;
  while (true) {
    const auto idx = result.find(toFind);
    if (idx == result.npos) {
      break;
    }
    result.replace(idx, toFind.size(), symbol);
  }

  return result;
}

std::string removeSpaces(const std::string& value)
{
  std::string result = value;

  result = removeSpacesInFrontOf(result, "*");
  result = removeSpacesInFrontOf(result, "&");

  return result;
}

std::string getTypeAsString(const clang::QualType& value, const clang::LangOptions& lo)
{
  clang::PrintingPolicy pp{lo};
  pp.FullyQualifiedName = false;
  pp.SuppressUnwrittenScope = true;
  pp.SuppressScope = true;

  const auto typeStr = value.getAsString(pp);
  const auto cleaned = removeSpaces(typeStr);

  return cleaned;
}
