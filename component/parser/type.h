/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/ast/Node.h"
#include <clang/AST/DeclBase.h>
#include <clang/AST/Type.h>
#include <vector>


std::vector<const clang::NamedDecl*> getTypes(const clang::QualType&);
NodeType typeFromKind(clang::Decl::Kind);
std::string getTypeAsString(const clang::QualType&, const clang::LangOptions&);
