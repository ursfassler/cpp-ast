/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "NodePrinter.h"
#include "component/ast/Node.h"
#include "component/ast/logger.h"
#include "Writer.h"
#include <cassert>


namespace
{

const std::map<NodeType, std::string> NodeTypeNames
{
  {NodeType::Class, "class"},
  {NodeType::Method, "method"},
  {NodeType::Field, "field"},
  {NodeType::Function, "function"},
  {NodeType::Variable, "variable"},
};

std::string nameByType(const Declaration* value)
{
  const auto idx = NodeTypeNames.find(typeOf(value));
  assert(idx != NodeTypeNames.end());
  return idx->second;
}

}


const NodePrinter::GetPath NodePrinter::NoPath{[](const Declaration*){
    return std::optional<std::string>{};
  }};
const NodePrinter::GetPath NodePrinter::PathFromNode{[](const Declaration* node){
    return std::optional<std::string>{node->getPath()};
  }};


NodePrinter::NodePrinter(const Root *root_, const GetId& getId_, const GetPath& getPath_, const Logger& logger_) :
  logger{logger_},
  root{root_},
  getId{getId_},
  getPath{getPath_}
{
}

void NodePrinter::print(xml::Writer &writer) const
{
  writer.startNode("project");

  print(&root->top, writer);

  writer.endNode();
}

void NodePrinter::print(const Package *node, xml::Writer &writer) const
{
  writer.startNode("package");
  writer.attribute("name", node->getName());

  node->foreachPackage([&writer, this](const Package* package){
    print(package, writer);
  });
  node->foreachChild([&writer, this](const Declaration* child){
    print(child, writer);
  });

  writer.endNode();
}

void NodePrinter::print(const Declaration *node, xml::Writer &writer) const
{
  const auto name = nameByType(node);
  writer.startNode(name);

  writer.attribute("name", node->getName());

  const auto path = getPath(node);
  if (path.has_value()) {
    writer.attribute("path", path.value());
  }

  const auto id = getId(node);
  if (id.has_value()) {
    writer.attribute("id", id.value());
  }

  node->foreachReference([this, &writer](const Declaration* reference){
    const auto id = getId(reference);
    if (id.has_value()) {
      writer.startNode("reference");
      writer.attribute("target", id.value());
      writer.endNode();
    } else {
      logger.error("reference to node without id: " + reference->toString());
    }
  });

//  node->foreachLocation([&writer](const Location& location){
//    writer.startNode("location");
//    writer.attribute("file", location.file);
//    writer.attribute("offset", std::to_string(location.offset));
//    writer.endNode();
//  });

  node->foreachChild([&writer, this](const Declaration* child){
    print(child, writer);
  });

  writer.endNode();
}
