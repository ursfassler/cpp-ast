/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <functional>
#include <optional>
#include <string>

namespace xml
{

class Writer;

}

class Root;
class Node;
class Declaration;
class Package;
class Logger;


class NodePrinter
{
public:
  typedef std::function<std::optional<std::string>(const Declaration*)> GetId;
  typedef std::function<std::optional<std::string>(const Declaration*)> GetPath;

  static const GetPath NoPath;
  static const GetPath PathFromNode;

  NodePrinter(const Root*, const GetId&, const GetPath&, const Logger&);

  void print(xml::Writer& writer) const;

private:
  const Logger& logger;
  const Root* root;

  GetId getId;
  GetPath getPath;

  void print(const Declaration* node, xml::Writer& writer) const;
  void print(const Package* node, xml::Writer& writer) const;


};
