/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "NumberedId.h"
#include "component/ast/Node.h"


NumberedId::NumberedId(const Root& root)
{
  gatherReferencees(&root.top);
  addId(&root.top, 1);
}

void NumberedId::gatherReferencees(const Node *node)
{
  auto withPackages = dynamic_cast<const Package*>(node);
  if (withPackages) {
    withPackages->foreachPackage([this](const Package* child){
      gatherReferencees(child);
    });
  }

  auto withReferences = dynamic_cast<const Declaration*>(node);
  if (withReferences) {
    withReferences->foreachReference([this](const Declaration* child){
      nodeWithReferencees.insert(child);
    });
  }

  node->foreachChild(std::bind(&NumberedId::gatherReferencees, this, std::placeholders::_1));
}

bool NumberedId::hasReferencees(const Declaration *node) const
{
  return nodeWithReferencees.find(node) != nodeWithReferencees.end();
}

uint32_t NumberedId::addId(const Node *node, uint32_t id)
{
  auto withPackages = dynamic_cast<const Package*>(node);
  if (withPackages) {
    withPackages->foreachPackage([&id, this](const Package* child){
      id = addId(child, id);
    });
  }

  auto withReferences = dynamic_cast<const Declaration*>(node);
  if (withReferences) {
    if (hasReferencees(withReferences)) {
      nodeIds[withReferences] = id;
      id++;
    }
  }

  node->foreachChild([&id, this](const Node* child){
    id = addId(child, id);
  });

  return id;
}

std::optional<std::string> NumberedId::getId(const Declaration *node) const
{
  const auto idx = nodeIds.find(node);
  if (idx == nodeIds.end()) {
    return {};
  } else {
    return {std::to_string(idx->second)};
  }
}

std::function<std::optional<std::string>(const Declaration*)> NumberedId::idGetter() const
{
  return std::bind(&NumberedId::getId, this, std::placeholders::_1);
}
