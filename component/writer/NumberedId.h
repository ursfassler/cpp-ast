/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <optional>
#include <set>
#include <map>
#include <functional>
#include <string>

class Root;
class Node;
class Declaration;


class NumberedId
{
public:
  NumberedId(const Root&);

  std::optional<std::string> getId(const Declaration* node) const;
  std::function<std::optional<std::string>(const Declaration*)> idGetter() const;

private:
  void gatherReferencees(const Node* node);
  bool hasReferencees(const Declaration *node) const;

  std::set<const Declaration*> nodeWithReferencees{};
  std::map<const Declaration*, uint32_t> nodeIds{};

  uint32_t addId(const Node* node, uint32_t id);
};
