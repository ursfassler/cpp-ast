#include <iostream>
#include <vector>

namespace types
{

using Number = int;

}

namespace business
{

class Entity
{
public:
    Entity(types::Number stuff_) :
        stuff{stuff_}
    {
    }

    types::Number getStuff() const
    {
        return stuff;
    }

private:
    types::Number stuff{};
};

using Repository = std::vector<Entity>;

}

namespace services
{

types::Number getAllStuff(const business::Repository& repo)
{
    types::Number result{};
    for (const auto& entity : repo) {
        result += entity.getStuff();
    }
    return result;
}

void addStuffThing(types::Number stuff, business::Repository& repo)
{
    business::Entity thing{stuff};
    repo.push_back(thing);
}

}

namespace ui
{

class StuffPrinter
{
public:
    StuffPrinter(const business::Repository& repo_, std::ostream& stream_) :
        repo{repo_},
        stream{stream_}
    {
    }

    void print()
    {
        const auto stuff = services::getAllStuff(repo);
        print(stuff);
    }

private:
    const business::Repository& repo;
    std::ostream& stream;

    void print(types::Number number)
    {
        stream << "All stuff: " << number << std::endl;
    }
};

}

namespace application
{

void run()
{
    business::Repository repo{};
    ui::StuffPrinter printer{repo, std::cout};

    services::addStuffThing(4, repo);
    services::addStuffThing(3, repo);
    services::addStuffThing(8, repo);
    services::addStuffThing(5, repo);

    printer.print();
}

}

int main()
{
    application::run();
    return 0;
}
