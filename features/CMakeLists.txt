cmake_minimum_required (VERSION 3.1)

set(features
    "declaration via macro.feature"
    "multiple files project.feature"
    "namespaces.feature"
    "parse built-in functions.feature"
    "parse class with references.feature"
    "parse class with type references.feature"
    "parse enum.feature"
    "parse function.feature"
    "parse lambda.feature"
    "parse simple class.feature"
    "parse variable.feature"
    "template class.feature"
    "template function.feature"
    "parse friend.feature"
    "parse C code.feature"
    "linker.feature"
)

add_executable(feature-tests
    step_definition/steps.cpp
    step_definition/Builder.h
    step_definition/GraphParser.cpp
    step_definition/GraphParser.h
    step_definition/AstBuilder.cpp
    step_definition/AstBuilder.h
    ${features}
)

include_directories(feature-tests
    ${CMAKE_SOURCE_DIR}
    ${CLANG_INCDIR}
)

target_link_libraries(feature-tests
    parser
    tinyxml
    _options
)

target_link_libraries(feature-tests
    cucumber-cpp
    boost_regex boost_system boost_program_options boost_filesystem
    pthread
    gtest
)

target_compile_options(feature-tests PRIVATE -Wall)
