# language: en

Feature: handle declarations via macros
  As a developer
  I want to be able to parse files with declarations via macros
  In order to get at least some information from the files


Scenario: parse class defined with a macro-magic body
  Given I have the source file:
    """
    #define TheClass(name)\
    class x##name \
    {\
     public:\
      x##name(){} \
    };

    TheClass(Foo)
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="xFoo">
                <method name="xFoo()"/>
            </class>
        </package>
    </project>

    """


Scenario: parse class defined as macro-magic arg
  Given I have the source file:
    """
    #define TheClass(name)\
    class name \
    {\
     public:\
      name(){} \
    };

    TheClass(Foo)
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="Foo">
                <method name="Foo()"/>
            </class>
        </package>
    </project>

    """


Scenario: parse reference to macro magic defined class
  Given I have the source file:
    """
    #define TheClass(name)\
    class name \
    {\
     public:\
      name(){} \
    };

    TheClass(Foo)

    Foo bar;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="Foo" id="1">
                <method name="Foo()" id="2"/>
            </class>
            <variable name="bar">
                <reference target="1"/>
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse reference to 2 macro magic defined classes
  Given I have the source file:
    """
    #define TheClasses(name)\
    class x##name \
    {\
    }; \
    class y##name \
    {\
    };

    TheClasses(Foo)

    xFoo x;
    yFoo y;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="xFoo" id="1"/>
            <class name="yFoo" id="2"/>
            <variable name="x">
                <reference target="1"/>
            </variable>
            <variable name="y">
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """
