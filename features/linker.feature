# language: en

Feature: linker
  As a developer
  I want to be able to link ASTs together
  In order to get one AST that contains the whole project


Scenario: link unrelated classes
  Given I have the AST file "a.ast":
    """
    <project>
        <package name="">
            <class name="1" path="1"/>
        </package>
    </project>

    """
  And I have the AST file "b.ast":
    """
    <project>
        <package name="">
            <class name="2" path="2"/>
        </package>
    </project>

    """

  When I link the files "a.ast" and "b.ast"

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="1" path="1"/>
            <class name="2" path="2"/>
        </package>
    </project>

    """


Scenario: use class defined in different translation unit
  Given I have the AST file "a.ast":
    """
    <project>
        <package name="">
            <class name="1" path="1"/>
        </package>
    </project>

    """
  And I have the AST file "b.ast":
    """
    <project>
        <package name="">
            <class name="2" path="2">
                <method name="3" path="3">
                    <reference target="1"/>
                </method>
            </class>
            <class name="1" path="1" id="1"/>
        </package>
    </project>

    """

  When I link the files "a.ast" and "b.ast"

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="1" path="1" id="1"/>
            <class name="2" path="2">
                <method name="3" path="3">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: use method defined in different translation unit
  Given I have the AST file "a.ast":
    """
    <project>
        <package name="">
            <class name="1" path="1">
                <method name="2" path="2"/>
            </class>
        </package>
    </project>

    """
  And I have the AST file "b.ast":
    """
    <project>
        <package name="">
            <class name="3" path="3">
                <method name="4" path="4">
                    <reference target="1"/>
                    <reference target="2"/>
                </method>
            </class>
            <class name="1" path="1" id="1">
                <method name="2" path="2" id="2"/>
            </class>
        </package>
    </project>

    """

  When I link the files "a.ast" and "b.ast"

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="1" path="1" id="1">
                <method name="2" path="2" id="2"/>
            </class>
            <class name="3" path="3">
                <method name="4" path="4">
                    <reference target="1"/>
                    <reference target="2"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: merge two existing nodes when they become one after another tu is linked in
  Given I have the AST file "a.ast":
    """
    <project>
        <package name="">
            <class name="1" path="1"/>
        </package>
    </project>

    """
  And I have the AST file "b.ast":
    """
    <project>
        <package name="">
            <class name="1" path="1"/>
        </package>
    </project>

    """

  When I link the files "a.ast" and "b.ast"

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="1" path="1"/>
        </package>
    </project>

    """


Scenario: does not duplicate references when equal node is merged
  Given I have the AST file "a.ast":
    """
    <project>
        <package name="">
            <class name="X" path="X" id="X"/>
            <class name="1" path="1">
                <reference target="X"/>
            </class>
        </package>
    </project>

    """
  And I have the AST file "b.ast":
    """
    <project>
        <package name="">
            <class name="X" path="X" id="X"/>
            <class name="1" path="1">
                <reference target="X"/>
            </class>
        </package>
    </project>

    """

  When I link the files "a.ast" and "b.ast"

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="X" path="X" id="1"/>
            <class name="1" path="1">
                <reference target="1"/>
            </class>
        </package>
    </project>

    """


Scenario: does not merge nodes with same name when children differ
  Given I have the AST file "a.ast":
    """
    <project>
        <package name="">
            <class name="X" path="X" id="X">
                <field name="a" path="X::a"/>
            </class>
            <variable name="a" path="a">
                <reference target="X"/>
            </variable>
        </package>
    </project>
    """
  And I have the AST file "b.ast":
    """
    <project>
        <package name="">
            <class name="X" path="X" id="X">
                <field name="b" path="X::b"/>
            </class>
            <variable name="b" path="b">
                <reference target="X"/>
            </variable>
        </package>
    </project>
    """

  When I link the files "a.ast" and "b.ast"

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="X" path="X" id="1">
                <field name="a" path="X::a"/>
            </class>
            <variable name="a" path="a">
                <reference target="1"/>
            </variable>
            <class name="X" path="X" id="2">
                <field name="b" path="X::b"/>
            </class>
            <variable name="b" path="b">
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """
