# language: en

Feature: multiple files
  As a developer
  I want to get the AST of the whole project
  In order to see only the relevant information


Scenario: parse classes in 2 different files
  Given I have the source file "A.cpp":
    """
    class A
    {
    };
    """
  And I have the source file "B.cpp":
    """
    class B
    {
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A"/>
            <class name="B"/>
        </package>
    </project>

    """


Scenario: parse class with separate declaration and implementation
  Given I have the header file "A.h":
    """
    #pragma once

    class A
    {
      public:
        void foo();
        void bar();
    };
    """
  And I have the source file "A.cpp":
    """
    #include "A.h"

    void A::foo()
    {
      bar();
    }

    void A::bar()
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="foo()">
                    <reference target="1"/>
                </method>
                <method name="bar()" id="1"/>
            </class>
        </package>
    </project>

    """


Scenario: link to class defined in another translation unit
  Given I have the header file "A.h":
    """
    #pragma once

    class A
    {
    };
    """
  And I have the source file "A.cpp":
    """
    #include "A.h"
    """
  And I have the header file "B.h":
    """
    #pragma once

    class B
    {
      public:
        void bar();
    };
    """
  And I have the source file "B.cpp":
    """
    #include "B.h"
    #include "A.h"

    void B::bar()
    {
      A a{};
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B">
                <method name="bar()">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: call method of class defined in another file
  Given I have the header file "A.h":
    """
    #pragma once

    class A
    {
      public:
        void foo();
    };
    """
  And I have the source file "A.cpp":
    """
    #include "A.h"

    void A::foo()
    {
    }
    """
  And I have the header file "B.h":
    """
    #pragma once

    class A;

    class B
    {
      public:
        void bar(A&);
    };
    """
  And I have the source file "B.cpp":
    """
    #include "B.h"
    #include "A.h"

    void B::bar(A& a)
    {
      a.foo();
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <method name="foo()" id="2"/>
            </class>
            <class name="B">
                <method name="bar(A&amp;)">
                    <reference target="1"/>
                    <reference target="2"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: merge namespaces defined in different files
  Given I have the source file "A.cpp":
    """
    namespace x
    {
    }
    """
  And I have the source file "B.cpp":
    """
    namespace x
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="x"/>
        </package>
    </project>

    """


Scenario: merge namespaces with classes and links
  Given I have the header file "A.h":
    """
    #pragma once

    namespace x
    {
      class A
      {
        public:
          void foo();
      };
    }
    """
  And I have the source file "A.cpp":
    """
    #include "A.h"

    namespace x
    {
      void A::foo()
      {
      }
    }
    """
  And I have the header file "B.h":
    """
    #pragma once

    #include "A.h"

    namespace x
    {
      class A;

      class B
      {
        public:
          void foo();
          void bar(A&);
      };
    }
    """
  And I have the source file "B.cpp":
    """
    #include "B.h"
    #include "A.h"

    namespace x
    {
      void B::foo()
      {
      }

      void B::bar(A& a)
      {
        a.foo();
      }
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="x">
                <class name="A" id="1">
                    <method name="foo()" id="2"/>
                </class>
                <class name="B">
                    <method name="foo()"/>
                    <method name="bar(A&amp;)">
                        <reference target="1"/>
                        <reference target="2"/>
                    </method>
                </class>
            </package>
        </package>
    </project>

    """


Scenario: merge prototype declaration defined in different files
  Given I have the header file "A.h":
    """
    #pragma once

    class X;

    class A
    {
    public:
      void foo(X*);
    };
    """
  And I have the source file "A.cpp":
    """
    #include "A.h"
    #include "X.h"

    void A::foo(X* x)
    {
      x->bar();
    }
    """
  And I have the header file "X.h":
    """
    #pragma once

    class X
    {
    public:
      void bar();
    };
    """
  And I have the source file "X.cpp":
    """
    #include "X.h"

    void X::bar()
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="X" id="1">
                <method name="bar()" id="2"/>
            </class>
            <class name="A">
                <method name="foo(X*)">
                    <reference target="1"/>
                    <reference target="2"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: link references to types that are not declared in some translation units
  Given I have the header file "A.h":
    """
    #pragma once

    class B;

    class A
    {
    public:
      A(B& b_);
    private:
      B& b;
    };
    """
  And I have the source file "A.cpp":
    """
    #include "A.h"
    #include "B.h"

    A::A(B &b_) :
      b{b_}
    {
    }
    """
  And I have the source file "C.cpp":
    """
    #include "A.h"

    void c(A& writer){}
    """
  And I have the header file "B.h":
    """
    #pragma once

    class B
    {
      int x;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="B" id="1">
                <field name="x"/>
            </class>
            <class name="A" id="2">
                <method name="A(B&amp;)">
                    <reference target="3"/>
                    <reference target="1"/>
                </method>
                <field name="b" id="3">
                    <reference target="1"/>
                </field>
            </class>
            <function name="c(A&amp;)">
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: link references to types that are not declared in some translation units (in different order)
  And I have the source file "C.cpp":
    """
    #include "A.h"

    void c(A& writer){}
    """
  Given I have the header file "A.h":
    """
    #pragma once

    class B;

    class A
    {
    public:
      A(B& b_);
    private:
      B& b;
    };
    """
  And I have the source file "A.cpp":
    """
    #include "A.h"
    #include "B.h"

    A::A(B &b_) :
      b{b_}
    {
    }
    """
  And I have the header file "B.h":
    """
    #pragma once

    class B
    {
      int x;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="B" id="1">
                <field name="x"/>
            </class>
            <class name="A" id="2">
                <method name="A(B&amp;)">
                    <reference target="3"/>
                    <reference target="1"/>
                </method>
                <field name="b" id="3">
                    <reference target="1"/>
                </field>
            </class>
            <function name="c(A&amp;)">
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: merge references when 3 files are involved
  Given I have the header file "a.h":
    """
    #pragma once

    class B;
    """
  And I have the header file "b.h":
    """
    #pragma once

    class B
    {
    };
    """
  And I have the header file "c.h":
    """
    #pragma once

    #include "b.h"

    class C :
        public B
    {
    };
    """
  And I have the source file "GraphParser.cpp":
    """
    #include "c.h"
    """
  And I have the source file "Queue.cpp":
    """
    #include "a.h"
    """
  And I have the source file "Queue.test.cpp":
    """
    #include "a.h"
    #include "c.h"
    """

  When I parse the source

  Then I expect no error output
  And I expect the output:
    """
    <project>
        <package name="">
            <class name="B" id="1"/>
            <class name="C">
                <reference target="1"/>
            </class>
        </package>
    </project>

    """


Scenario: parse call to constructor in implementation file
  Given I have the header file "A.h":
    """
    #pragma once

    class A
    {
    public:
      A();

    };

    class B :
        public A
    {
    public:
      B();
    };

    """
  And I have the source file "A.cpp":
    """
    #include "A.h"

    A::A()
    {
    }

    B::B() :
      A{}
    {
    }

    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <method name="A()" id="2"/>
            </class>
            <class name="B">
                <reference target="1"/>
                <method name="B()">
                    <reference target="2"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: link prototype with reference
  Given I have the header file "A.h":
    """
    #pragma once

    template<typename T>
    class A
    {
    public:
      T a;

    };
    """
  And I have the source file "B.cpp":
    """
    #include "A.h"

    A<int> b;
    """
  And I have the source file "C.cpp":
    """
    #include "A.h"

    typedef A<int> C;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <field name="a"/>
            </class>
            <variable name="b">
                <reference target="1"/>
            </variable>
            <class name="C">
                <reference target="1"/>
            </class>
        </package>
    </project>

    """



Scenario: define object with same name but different types in two different files
  Given I have the source file "A.cpp":
    """
    const char* X[] = {
      "Hello",
    };

    const auto a = X;
    """
  And I have the source file "B.cpp":
    """
    enum X {
      Hello,
    };

    const X b{};
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <variable name="X" id="1"/>
            <variable name="a">
                <reference target="1"/>
            </variable>
            <class name="X" id="2">
                <field name="Hello"/>
            </class>
            <variable name="b">
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: define object with same name and same types in two different files
  Given I have the source file "A.cpp":
    """
    static const char* X[] = {
      "Hello",
    };

    const auto a = X;
    """
  And I have the source file "B.cpp":
    """
    static const char* X[] = {
      "Hello",
    };

    const auto b = X;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <variable name="X" id="1"/>
            <variable name="a">
                <reference target="1"/>
            </variable>
            <variable name="X" id="2"/>
            <variable name="b">
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: don't merge translation unit private class declarations
  Given I have the source file "A.cpp":
    """
    namespace
    {
      class X{};
    }

    class Y{};

    X ax;
    Y ay;
    """
  And I have the source file "B.cpp":
    """
    namespace
    {
      class X{};
    }

    class Y{};

    X bx;
    Y by;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="X" id="1"/>
            <class name="Y" id="2"/>
            <variable name="ax">
                <reference target="1"/>
            </variable>
            <variable name="ay">
                <reference target="2"/>
            </variable>
            <class name="X" id="3"/>
            <variable name="bx">
                <reference target="3"/>
            </variable>
            <variable name="by">
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: merge typedef in class
  Given I have the header file "X.h":
    """
    #pragma once

    #include <vector>

    class X
    {
    public:
      typedef std::vector<int> Y;
    };
    """
  And I have the source file "A.cpp":
    """
    #include "X.h"

    X::Y a;
    """
  And I have the source file "B.cpp":
    """
    #include "X.h"

    X::Y b;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="X">
                <class name="Y" id="1"/>
            </class>
            <variable name="a">
                <reference target="1"/>
            </variable>
            <variable name="b">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: don't merge typedefs from anonymous namespaces
  Given I have the source file "A.cpp":
    """
    #include <vector>
    namespace
    {
      typedef std::vector<int> Y;
    }
    Y a;
    """
  And I have the source file "B.cpp":
    """
    #include <vector>
    namespace
    {
      typedef std::vector<int> Y;
    }
    Y b;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="Y" id="1"/>
            <variable name="a">
                <reference target="1"/>
            </variable>
            <class name="Y" id="2"/>
            <variable name="b">
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


@todo
Scenario: ignore out of line declaration
  Given I have the header file "X.h":
    """
    #pragma once

    class X
    {
    public:
      class A* foo()    // this is what we test
      {
        return {};
      }
    };
    """
  Given I have the source file "A.cpp":
    """
    class A
    {
    };

    #include "X.h"
    """
  And I have the source file "B.cpp":
    """
    #include "X.h"
    """

  When I parse the source

  Then I expect this part in the error output: "X.h:6:9: ignoring out of line declaration of A"
  And I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="X">
                <method name="foo()">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


@todo
Scenario: handle out of line declarations in multiple files
  Given I have the header file "A.h":
    """
    #pragma once

    class A
    {
    public:
      class C* a;
    };
    """
  And I have the source file "A.cpp":
    """
    #include "A.h"
    #include "C.h"
    """
  And I have the header file "B.h":
    """
    #pragma once

    class B
    {
    public:
      class C* b;
    };
    """
  And I have the source file "B.cpp":
    """
    #include "B.h"
    #include "C.h"
    """
  And I have the header file "C.h":
    """
    #pragma once

    class C
    {
    public:
      int x;
    };
    """

  When I parse the source

  Then I expect this part in the error output: "A.h:6:9: ignoring out of line declaration of C"
  And I expect this part in the error output: "B.h:6:9: ignoring out of line declaration of C"
  And I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <field name="a">
                    <reference target="1"/>
                </field>
            </class>
            <class name="C" id="1">
                <field name="x"/>
            </class>
            <class name="B">
                <field name="b">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: handle out of line declaration in a C typedef
  Given I have the header file "B.h":
    """
    #pragma once

    extern "C"
    {

    typedef struct A* B;

    }
    """
  And I have the header file "A.h":
    """
    #pragma once

    struct A
    {
    };
    """
  And I have the source file "A.c":
    """
    #include "A.h"
    """
  And I have the source file "C.cpp":
    """
    #include "B.h"
    #include "A.h"

    class C
    {
      B b;
    };
    """

  When I parse the source

  Then I expect this part in the error output: "B.h:6:16: ignoring out of line declaration of A"
  And I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B" id="2">
                <reference target="1"/>
            </class>
            <class name="C">
                <field name="b">
                    <reference target="2"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: handle out of line declaration of a C++ struct in a struct
  Given I have the source file "A.cpp":
    """
    struct A
    {
      struct X*	y;
    };

    #include "X.h"
    """
  And I have the source file "B.cpp":
    """
    #include "X.h"
    """
  And I have the header file "X.h":
    """
    #pragma once

    struct X
    {
      int x;
    };
    """

  When I parse the source

  Then I expect this part in the error output: "A.cpp:3:10: ignoring out of line declaration of X"
  And I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <field name="y">
                    <reference target="1"/>
                </field>
            </class>
            <class name="X" id="1">
                <field name="x"/>
            </class>
        </package>
    </project>

    """


Scenario: handle struct with name declared in different translation units
  Given I have the source file "A.cpp":
    """
    struct X
    {
      int a;
    };

    X a;
    """
  And I have the source file "B.cpp":
    """
    struct X
    {
      int b;
    };

    X b;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="X" id="1">
                <field name="a"/>
            </class>
            <variable name="a">
                <reference target="1"/>
            </variable>
            <class name="X" id="2">
                <field name="b"/>
            </class>
            <variable name="b">
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: handle same struct with prototype in different translation units
  Given I have the source file "A.cpp":
    """
    struct X;

    X* a;
    """
  And I have the source file "B.cpp":
    """
    struct X;

    X* b;
    """
  And I have the source file "X.cpp":
    """
    struct X
    {
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="X" id="1"/>
            <variable name="a">
                <reference target="1"/>
            </variable>
            <variable name="b">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """
