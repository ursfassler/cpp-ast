# language: en

Feature: detect namespaces
  As a developer
  I want to get the AST of a simple class
  In order to see only the relevant information


Scenario: parse top level namespace
  Given I have the source file:
    """
    namespace a
    {
    }

    namespace b
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="a"/>
            <package name="b"/>
        </package>
    </project>

    """


Scenario: parse cascaded namespaces
  Given I have the source file:
    """
    namespace a
    {
        namespace b
        {
            namespace c
            {
            }
        }
        namespace d
        {
        }
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="a">
                <package name="b">
                    <package name="c"/>
                </package>
                <package name="d"/>
            </package>
        </package>
    </project>

    """


Scenario: parse nested namespaces
  Given I have the source file:
    """
    namespace a::b::c
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="a">
                <package name="b">
                    <package name="c"/>
                </package>
            </package>
        </package>
    </project>

    """


Scenario: parse classes in namespaces
  Given I have the source file:
    """
    namespace a
    {
        class A{};
        namespace b
        {
            namespace c
            {
                class C{};
            }
            class B{};
        }
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="a">
                <package name="b">
                    <package name="c">
                        <class name="C"/>
                    </package>
                    <class name="B"/>
                </package>
                <class name="A"/>
            </package>
        </package>
    </project>

    """


Scenario: ignore anonymous namespace declarations
  Given I have the source file:
    """
    namespace a
    {
        class A{};
        namespace
        {
            class B{};
        }
        class C{};
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="A"/>
                <class name="B"/>
                <class name="C"/>
            </package>
        </package>
    </project>

    """


Scenario: merge namespaces declared in header file and used in multiple implementation files
  Given I have the header file "ns.h":
    """
    #pragma once
    namespace x{}
    """
  And I have the source file "A.cpp":
    """
    #include "ns.h"
    """
  And I have the source file "B.cpp":
    """
    #include "ns.h"
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="x"/>
        </package>
    </project>

    """


Scenario: parse reference to class into different namespace
  Given I have the header file "A.h":
    """
    #pragma once

    namespace ns
    {
      class A{};
    }
    """
  And I have the source file "A.cpp":
    """
    #include "A.h"
    """
  And I have the header file "b.h":
    """
    #pragma once

    namespace ns
    {
      class A;
    }

    void bar(ns::A&);
    """
  And I have the source file "b.cpp":
    """
    #include "b.h"
    #include "A.h"

    void bar(ns::A&)
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="ns">
                <class name="A" id="1"/>
            </package>
            <function name="bar(ns::A&amp;)">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse typedef with same name in different namespaces
  Given I have the source file:
    """
    namespace a
    {
      typedef int TheType;
    }
    namespace b
    {
      typedef float TheType;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="TheType"/>
            </package>
            <package name="b">
                <class name="TheType"/>
            </package>
        </package>
    </project>

    """


Scenario: use fully qualified name in function signature
  Given I have the source file:
    """
    namespace ns
    {

    typedef int A;

    void foo(A)
    {
    }

    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="ns">
                <class name="A" id="1"/>
                <function name="foo(A)">
                    <reference target="1"/>
                </function>
            </package>
        </package>
    </project>

    """
