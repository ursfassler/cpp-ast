# language: en

Feature: parse C code
  As a tool user
  I want to parse C code
  Because my C++ code uses C headers


Scenario: parse a C struct
  Given I have the C source file:
    """
    typedef struct
    {
      int a;
      float b;
    } A;
    """

  When I parse the source

  Then I expect this part in the error output: "input.c:1:9: found struct without name"
  And I expect the output:
    """
    <project>
        <package name="">
            <class name="" id="1">
                <field name="a"/>
                <field name="b"/>
            </class>
            <class name="A">
                <reference target="1"/>
            </class>
        </package>
    </project>

    """


Scenario: parse enum with values dependent on same enum
  Given I have the C source file:
    """
    typedef enum A
    {
      x = 1,
      y = 2,
      z = x | y,
    } B;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <field name="x" id="2"/>
                <field name="y" id="3"/>
                <field name="z">
                    <reference target="2"/>
                    <reference target="3"/>
                </field>
            </class>
            <class name="B">
                <reference target="1"/>
            </class>
        </package>
    </project>

    """


Scenario: parse reference to struct
  Given I have the C source file:
    """
    typedef struct A
    {
      int x;
    } B;

    int foo(B* b)
    {
      return b->x;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <field name="x" id="2"/>
            </class>
            <class name="B" id="3">
                <reference target="1"/>
            </class>
            <function name="foo(B*)">
                <reference target="3"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: parse class with 2 unnamed unions
  Given I have the C source file:
    """
    union
    {
      int a;
    } A;

    union
    {
      int b;
    } B;
    """

  When I parse the source

  Then I expect this part in the error output: "input.c:1:1: found union without name"
  And I expect this part in the error output: "input.c:6:1: found union without name"
  And I expect the output:
    """
    <project>
        <package name="">
            <class name="" id="1">
                <field name="a"/>
            </class>
            <variable name="A">
                <reference target="1"/>
            </variable>
            <class name="" id="2">
                <field name="b"/>
            </class>
            <variable name="B">
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse 2 empty unions without a name
  Given I have the C source file:
    """
    union
    {
    };

    union
    {
    };
    """

  When I parse the source

  Then I expect this part in the error output: "input.c:1:1: found union without name"
  And I expect this part in the error output: "input.c:5:1: found union without name"
  Then I expect the output:
    """
    <project>
        <package name=""/>
    </project>

    """


Scenario: parse unnamed union with 2 unnamed structs in it
  Given I have the C source file:
    """
    union
    {
      struct
      {
        int a;
      } A;

      struct
      {
        int b;
      } B;
    } C;
    """

  When I parse the source

  Then I expect this part in the error output: "input.c:1:1: found union without name"
  And I expect this part in the error output: "input.c:3:3: found struct without name"
  And I expect this part in the error output: "input.c:8:3: found struct without name"
  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="" id="1">
                <class name="" id="2">
                    <field name="a"/>
                </class>
                <field name="A">
                    <reference target="2"/>
                </field>
                <class name="" id="3">
                    <field name="b"/>
                </class>
                <field name="B">
                    <reference target="3"/>
                </field>
            </class>
            <variable name="C">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: access C function from C++
  Given I have the source file "foo.c":
    """
    int foo()
    {
    }
    """
  And I have the header file "foo.h":
    """
    #pragma once

    extern "C"
    {

    int foo();

    }
    """
  And I have the source file "bar.cpp":
    """
    #include "foo.h"

    int bar()
    {
      return foo();
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="foo()" id="1"/>
            <function name="bar()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """
