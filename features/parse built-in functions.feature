# language: en

Feature: parse built-in functions
  As a developer
  I want to get the AST of a file with built-in functions
  In order to be able to parse all of my files


Scenario: parse a function that calls __builtin_trap
  Given I have the source file:
    """
    void func()
    {
      __builtin_trap();
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="func()"/>
        </package>
    </project>

    """


Scenario: no references from static_assert
  Given I have the source file:
    """
    constexpr int x = 5;

    static_assert(x == 5);
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <variable name="x"/>
        </package>
    </project>

    """

