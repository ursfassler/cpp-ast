# language: en

Feature: parse a class with references
  As a developer
  I want to get information about relations
  In order to see what is connected


Scenario: parse a class with a reference from a method to a field
  Given I have the source file:
    """
    class A
    {
    public:
      void foo()
      {
        bar = 42;
      }

    private:
      int bar{};

    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="foo()">
                    <reference target="1"/>
                </method>
                <field name="bar" id="1"/>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a reference from a method to another method
  Given I have the source file:
    """
    class A
    {
    public:
      void bar()
      {
      }

      void foo()
      {
        bar();
      }

    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="bar()" id="1"/>
                <method name="foo()">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse a reference to a field
  Given I have the source file:
    """
    class A
    {
    public:
      int b;
    };

    void test()
    {
      A a;
      a.b = 11;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <field name="b" id="2"/>
            </class>
            <function name="test()">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: parse a class with a reference to another class that is defined later
  Given I have the source file:
    """
    class A;

    class B
    {
      A* a;
    };

    class A
    {
    };

    class A;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B">
                <field name="a">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: add a type reference when inherited
  Given I have the source file:
    """
    class A
    {
    };

    class B :
        private A
    {
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B">
                <reference target="1"/>
            </class>
        </package>
    </project>

    """


Scenario: add a type reference only to direct class parents
  Given I have the source file:
    """
    class A
    {
    };

    class B :
        public A
    {
    };

    class C :
        public B
    {
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B" id="2">
                <reference target="1"/>
            </class>
            <class name="C">
                <reference target="2"/>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a reference in a method parameter
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
      void foo(A)
      {
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B">
                <method name="foo(A)">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with references in method parameters
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
    };

    class C
    {
      void foo(B&, A*)
      {
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B" id="2"/>
            <class name="C">
                <method name="foo(B&amp;, A*)">
                    <reference target="2"/>
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a reference in a method return
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
      A foo()
      {
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B">
                <method name="foo()">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a field reference in a delete operator
  Given I have the source file:
    """
    class X
    {
      int* a;

      void foo()
      {
        delete a;
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="X">
                <field name="a" id="1"/>
                <method name="foo()">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse a reference to a custom constructor with new
  Given I have the source file:
    """
    class A
    {
    public:
      A(int)
      {
      }
    };

    A* x = new A(23);
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <method name="A(int)" id="2"/>
            </class>
            <variable name="x">
                <reference target="1"/>
                <reference target="1"/>
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse a reference to a implicit constructor with new
  Given I have the source file:
    """
    class A
    {
    };

    A* x = new A();
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <variable name="x">
                <reference target="1"/>
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse a reference to a custom destructor with delete
  Given I have the source file:
    """
    class A
    {
    public:
      ~A()
      {
      }
    };

    void func(A* a)
    {
      delete a;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <method name="~A()" id="2"/>
            </class>
            <function name="func(A*)">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: parse a reference to a implicit destructor with delete
  Given I have the source file:
    """
    class A
    {
    };

    void func(A* a)
    {
      delete a;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <function name="func(A*)">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse delete expression to template
  Given I have the source file:
    """
    template<typename T>
    void deleter(T& c) {
      for (auto v : c) {
        delete v;
      }
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="deleter&lt;typename&gt;(T&amp;)"/>
        </package>
    </project>

    """


Scenario: handle delete of primitive data type
  Given I have the source file:
    """
    void func(int* a)
    {
      delete a;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="func(int*)"/>
        </package>
    </project>

    """


Scenario: parse a reference to a implicit constructor with initializer
  Given I have the source file:
    """
    class A
    {
    };

    A x{};
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <variable name="x">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse a reference to a custom constructor with initializer
  Given I have the source file:
    """
    class A
    {
    public:
      A(int)
      {
      }
    };

    A x{41};
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <method name="A(int)" id="2"/>
            </class>
            <variable name="x">
                <reference target="1"/>
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse a reference to a implicit destructor
  Given I have the source file:
    """
    class A
    {
    };

    void func()
    {
        A x{};
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <function name="func()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


@todo
Scenario: does not parse a reference to a custom destructor
  We would expect a reference from func to ~A
  Given I have the source file:
    """
    class A
    {
    public:
      ~A()
      {
      }
    };

    void func()
    {
        A x{};
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <method name="~A()"/>
            </class>
            <function name="func()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse call to static class field
  Given I have the header file "A.h":
    """
    #pragma once

    class A
    {
    public:
      static A* get();

    private:
      static A* instance;
    };
    """
  And I have the source file "A.cpp":
    """
    #include "A.h"

    A* A::instance = 0;

    A* A::get()
    {
      return instance;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <method name="get()">
                    <reference target="1"/>
                    <reference target="2"/>
                </method>
                <field name="instance" id="2">
                    <reference target="1"/>
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse class with implicit added assign operator
  Given I have the header file "A.h":
    """
    #pragma once

    class B{};

    class A
    {
    public:
      B& foo()
      {
        return x;
      }

      B x;
    };

    """
  And I have the source file "A.cpp":
    """
    #include "A.h"

    A a;
    B b;

    void bar()
    {
      b = a.foo();
    }


    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="B" id="1"/>
            <class name="A" id="2">
                <method name="foo()" id="3">
                    <reference target="1"/>
                    <reference target="4"/>
                </method>
                <field name="x" id="4">
                    <reference target="1"/>
                </field>
            </class>
            <variable name="a" id="5">
                <reference target="2"/>
            </variable>
            <variable name="b" id="6">
                <reference target="1"/>
            </variable>
            <function name="bar()">
                <reference target="6"/>
                <reference target="3"/>
                <reference target="5"/>
            </function>
        </package>
    </project>

    """
