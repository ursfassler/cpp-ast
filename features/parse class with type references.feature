# language: en

Feature: parse a class with references
  As a developer
  I want to get information about relations
  In order to see what is connected


Scenario: parse a class with a reference to another class
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
      A a;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B">
                <field name="a">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a pointer to another class
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
      A* a;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B">
                <field name="a">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a multi pointer to another class
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
      A******* a;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B">
                <field name="a">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with an array to another class
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
      A a[];
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B">
                <field name="a">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with an multidimensional array to another class
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
      A a[2][3][4];
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B">
                <field name="a">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with an array and pointer another class
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
      A* a[];
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B">
                <field name="a">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a reference to another class
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
      A& a;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B">
                <field name="a">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a reference to another class
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
      A*& a;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B">
                <field name="a">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a reference to itself
  Given I have the source file:
    """
    class A
    {
      A* a;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <field name="a">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with references to multiple targets
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
    };

    class C
    {
    };

    class D
    {
      A a;
      B b;
      C c;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B" id="2"/>
            <class name="C" id="3"/>
            <class name="D">
                <field name="a">
                    <reference target="1"/>
                </field>
                <field name="b">
                    <reference target="2"/>
                </field>
                <field name="c">
                    <reference target="3"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a type reference method variable
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
      void foo()
      {
        if (true) {
          A a{};
        }
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B">
                <method name="foo()">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a type reference in cast
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
    };

    class X
    {
      void foo(void* x)
      {
        (A*)x;
        static_cast<B*>(x);
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B" id="2"/>
            <class name="X">
                <method name="foo(void*)">
                    <reference target="1"/>
                    <reference target="2"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: ignore type references to system header files
  Given I have the source file:
    """
    #include <vector>

    class A
    {
      std::vector<int> vec{};
    };

    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <field name="vec"/>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a type reference in a new operator
  Given I have the source file:
    """
    class A
    {
    };

    class X
    {
      void foo()
      {
        new A();
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="X">
                <method name="foo()">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a type reference in a constructor initializer
  Given I have the source file:
    """
    class A
    {
    };

    class B :
      public A
    {
    };

    class X
    {
    public:
      X() :
        ref{new B()}
      {
      }

    private:
      A* ref;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B" id="2">
                <reference target="1"/>
            </class>
            <class name="X">
                <method name="X()">
                    <reference target="3"/>
                    <reference target="2"/>
                </method>
                <field name="ref" id="3">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a type reference in a constructor initializer with separate declaration and implementation
  Given I have the header file "A.h":
    """
    #pragma once

    class A
    {
    };
    """
  And I have the header file "B.h":
    """
    #pragma once

    #include "A.h"

    class B :
      public A
    {
    };
    """
  And I have the header file "X.h":
    """
    #pragma once

    class A;

    class X
    {
    public:
      X();

    private:
      A* ref;
    };
    """
  And I have the source file "X.cpp":
    """
    #include "X.h"
    #include "B.h"

    X::X() :
      ref{new B()}
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="X">
                <method name="X()">
                    <reference target="2"/>
                    <reference target="3"/>
                </method>
                <field name="ref" id="2">
                    <reference target="1"/>
                </field>
            </class>
            <class name="B" id="3">
                <reference target="1"/>
            </class>
        </package>
    </project>

    """


Scenario: parse constructor initializer in wrong order
  A correct order would be nice, but we don't see at the moment how this can be done.
  THe correct referenc order would be: 4, 1, 5, 2, 6, 3

  Given I have the source file:
    """
    class A{};
    class B{};
    class C{};

    class X
    {
    public:
      X() :
        a{new A()},
        b{new B()}
      {
        c = new C();
      }

    private:
      A* a;
      B* b;
      C* c;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B" id="2"/>
            <class name="C" id="3"/>
            <class name="X">
                <method name="X()">
                    <reference target="4"/>
                    <reference target="5"/>
                    <reference target="1"/>
                    <reference target="2"/>
                    <reference target="6"/>
                    <reference target="3"/>
                </method>
                <field name="a" id="4">
                    <reference target="1"/>
                </field>
                <field name="b" id="5">
                    <reference target="2"/>
                </field>
                <field name="c" id="6">
                    <reference target="3"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a type reference in the constructor
  Given I have the source file:
    """
    class A
    {
    };

    class B :
      public A
    {
    };

    class X
    {
    public:
      X()
      {
        ref = new B();
      }

    private:
      A* ref;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B" id="2">
                <reference target="1"/>
            </class>
            <class name="X">
                <method name="X()">
                    <reference target="3"/>
                    <reference target="2"/>
                </method>
                <field name="ref" id="3">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a type reference in a field initializer
  Given I have the source file:
    """
    class A
    {
    };

    class B :
      public A
    {
    };

    class X
    {
    private:
      A* ref{new B()};
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B" id="2">
                <reference target="1"/>
            </class>
            <class name="X">
                <field name="ref">
                    <reference target="1"/>
                    <reference target="2"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse a reference to a using-type
  Given I have the source file:
    """
    namespace a
    {
      class A
      {
      };
    }

    using a::A;

    A foo{};
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="A" id="1"/>
            </package>
            <variable name="foo">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse translation unit where not all types are declared
  Given I have the source file:
    """
    class B;

    class A
    {
      B* b;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="B" id="1"/>
            <class name="A">
                <field name="b">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: link different prototype declarations of same class correctly
  Given I have the header file "A.h":
    """
    #pragma once
    class X;
    """
  And I have the source file "A.cpp":
    """
    #include "A.h"
    """
  And I have the header file "X.h":
    """
    #pragma once
    class X
    {
      int x;
    };
    """
  And I have the source file "X.cpp":
    """
    #include "X.h"
    """
  And I have the source file "Y.cpp":
    """
    #include "X.h"
    #include "A.h"
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="X">
                <field name="x"/>
            </class>
        </package>
    </project>

    """
