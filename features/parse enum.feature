# language: en

Feature: parse enums
  As a developer
  I want to get the AST of enum
  In order to see only the relevant information


Scenario: parse an enum declaration
  Given I have the source file:
    """
    enum A
    {
      a,
      b,
      c,
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <field name="a"/>
                <field name="b"/>
                <field name="c"/>
            </class>
        </package>
    </project>

    """


Scenario: parse an enum instantiation
  Given I have the source file:
    """
    enum class A
    {
      a,
      b,
    };

    A var;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <field name="a"/>
                <field name="b"/>
            </class>
            <variable name="var">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse reference to enum values
  Given I have the source file:
    """
    enum class A
    {
      a,
      b,
    };

    A func()
    {
      return A::b;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <field name="a"/>
                <field name="b" id="2"/>
            </class>
            <function name="func()">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: parse reference to enum values without type
  Given I have the source file:
    """
    enum A
    {
      a,
      b,
    };

    A func(A value)
    {
      return (value == b) ? a : b;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <field name="a" id="2"/>
                <field name="b" id="3"/>
            </class>
            <function name="func(A)">
                <reference target="1"/>
                <reference target="1"/>
                <reference target="3"/>
                <reference target="2"/>
                <reference target="3"/>
            </function>
        </package>
    </project>

    """


Scenario: parse reference in enum declaration
  Given I have the source file:
    """
    enum A
    {
      aa,
      ab,
      ac,
    };

    enum B
    {
      ba = ac,
      bb = ab,
      bc = aa,
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <field name="aa" id="1"/>
                <field name="ab" id="2"/>
                <field name="ac" id="3"/>
            </class>
            <class name="B">
                <field name="ba">
                    <reference target="3"/>
                </field>
                <field name="bb">
                    <reference target="2"/>
                </field>
                <field name="bc">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse enum declaration included in two translation units
  Given I have the header file "A.h":
    """
    #pragma once

    enum A
    {
        a,
        b,
        c,
    };
    """
  And I have the source file "B.cpp":
    """
    #include "A.h"

    A x = A::b;
    """
  And I have the source file "C.cpp":
    """
    #include "A.h"

    A y = A::c;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <field name="a"/>
                <field name="b" id="2"/>
                <field name="c" id="3"/>
            </class>
            <variable name="x">
                <reference target="1"/>
                <reference target="2"/>
            </variable>
            <variable name="y">
                <reference target="1"/>
                <reference target="3"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse typedef enum
  Given I have the source file:
    """
    typedef enum
    {
      a,
      b,
      c
    } A;
    """

  When I parse the source

  Then I expect this part in the error output: "input.cpp:1:9: found enum without name"
  And I expect the output:
    """
    <project>
        <package name="">
            <class name="" id="1">
                <field name="a"/>
                <field name="b"/>
                <field name="c"/>
            </class>
            <class name="A">
                <reference target="1"/>
            </class>
        </package>
    </project>

    """


Scenario: parse two typedef enum
  Given I have the source file:
    """
    typedef enum
    {
      a,
    } A;

    typedef enum
    {
      b,
    } B;
    """

  When I parse the source

  Then I expect this part in the error output: "input.cpp:1:9: found enum without name"
  And I expect this part in the error output: "input.cpp:6:9: found enum without name"
  And I expect the output:
    """
    <project>
        <package name="">
            <class name="" id="1">
                <field name="a"/>
            </class>
            <class name="A">
                <reference target="1"/>
            </class>
            <class name="" id="2">
                <field name="b"/>
            </class>
            <class name="B">
                <reference target="2"/>
            </class>
        </package>
    </project>

    """


Scenario: parse enum without a name
  Given I have the source file:
    """
    enum
    {
      a,
      b,
      c,
    };
    """

  When I parse the source

  Then I expect this part in the error output: "input.cpp:1:1: found enum without name"
  And I expect the output:
    """
    <project>
        <package name="">
            <class name="">
                <field name="a"/>
                <field name="b"/>
                <field name="c"/>
            </class>
        </package>
    </project>

    """


Scenario: parse 2 enums without a name
  Given I have the source file:
    """
    enum
    {
      a,
      b,
    };
    enum
    {
      c,
      d,
    };
    auto x = b;
    auto y = c;
    """

  When I parse the source

  Then I expect this part in the error output: "input.cpp:1:1: found enum without name"
  And I expect this part in the error output: "input.cpp:6:1: found enum without name"
  And I expect the output:
    """
    <project>
        <package name="">
            <class name="" id="1">
                <field name="a"/>
                <field name="b" id="2"/>
            </class>
            <class name="" id="3">
                <field name="c" id="4"/>
                <field name="d"/>
            </class>
            <variable name="x">
                <reference target="1"/>
                <reference target="2"/>
            </variable>
            <variable name="y">
                <reference target="3"/>
                <reference target="4"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse 2 empty enums without a name
  Given I have the source file:
    """
    enum
    {
    };
    enum
    {
    };
    """

  When I parse the source

  Then I expect this part in the error output: "input.cpp:1:1: found enum without name"
  And I expect this part in the error output: "input.cpp:4:1: found enum without name"
  And I expect the output:
    """
    <project>
        <package name=""/>
    </project>

    """
