# language: en

Feature: parse class with friend
  As a developer
  I want to be able to get the AST of a class with friends
  In order to process all files


Scenario: print info about unhandled friend declaration
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
      friend class A;
    };
    """

  When I parse the source

  Then I expect this part in the error output: "input.cpp:7:10: can not handle friend declaration"


@todo
Scenario: don't yet add a reference for a friend
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
      friend class A;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A"/>
            <class name="B"/>
        </package>
    </project>

    """


@todo
Scenario: parse a friend to another class that has no prototype declaration
  Given I have the source file:
    """
    class A
    {
    public:
      friend class B;
    };

    class B
    {
    public:
      B* x;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A"/>
            <class name="B" id="1">
                <field name="x">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


@todo
Scenario: parse friend that is only declared in a different translation unit
  Given I have the source file "A.cpp":
    """
    class A
    {
      friend class B;
    };
    """
  And I have the source file "B.cpp":
    """
    class B
    {
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A"/>
            <class name="B"/>
        </package>
    </project>

    """
