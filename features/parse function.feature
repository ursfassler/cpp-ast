# language: en

Feature: parse a function
  As a developer
  I want to get the AST with functions
  In order to see only the relevant information


Scenario: parse a simple function
  Given I have the source file:
    """
    void foo()
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="foo()"/>
        </package>
    </project>

    """


Scenario: parse a function with class references
  Given I have the source file:
    """
    class A{};
    class B{};

    void foo(A* a)
    {
      B* b = (B*)a;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B" id="2"/>
            <function name="foo(A*)">
                <reference target="1"/>
                <reference target="2"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: parse a function with two class references
  Given I have the source file:
    """
    class A{};
    class B{};

    void foo(A* a, B)
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B" id="2"/>
            <function name="foo(A*, B)">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: parse a function with function references
  Given I have the source file:
    """
    void bar();

    void foo()
    {
      bar();
    }

    void bar()
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="bar()" id="1"/>
            <function name="foo()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse a function with function references
  Given I have the source file:
    """
    void bar();

    void foo()
    {
      bar();
    }

    void bar()
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="bar()" id="1"/>
            <function name="foo()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse a function with function references in the arguments
  Given I have the source file:
    """
    void f1(int)
    {
    }

    int f2()
    {
      return {};
    }

    void f3()
    {
      f1(f2());
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="f1(int)" id="1"/>
            <function name="f2()" id="2"/>
            <function name="f3()">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: parse a recursive function with prototype declaration
  Given I have the source file:
    """
    void func();

    void func()
    {
        func();
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="func()" id="1">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse a function with an C array as parameter
  Given I have the source file:
    """
    class A{};

    void foo(A data[])
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <function name="foo(A*)">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: don't handle a function prototype in a function
  Given I have the source file:
    """
    void foo()
    {
      void bar();
    }

    void bar()
    {
    }
    """

  When I parse the source

  Then I expect this part in the error output: "input.cpp:3:8: can't handle function prototype in function"
  And I expect the output:
    """
    <project>
        <package name="">
            <function name="foo()"/>
            <function name="bar()"/>
        </package>
    </project>

    """


Scenario: don't handle a function prototype in a method
  Given I have the source file:
    """
    class A
    {
      void foo()
      {
        void bar();
      }
    };

    void bar()
    {
    }
    """

  When I parse the source

  Then I expect this part in the error output: "input.cpp:5:10: can't handle function prototype in method"
  And I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="foo()"/>
            </class>
            <function name="bar()"/>
        </package>
    </project>

    """


Scenario: correctly parse relation to function when prototype is in method
  Given I have the source file:
    """
    class A
    {
      void foo()
      {
        void bar();

        bar();
      }
    };

    void bar()
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="foo()">
                    <reference target="1"/>
                </method>
            </class>
            <function name="bar()" id="1"/>
        </package>
    </project>

    """


Scenario: parse a class definition in a function
  Given I have the source file:
    """
    void foo()
    {
      class A{};
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="foo()">
                <class name="A"/>
            </function>
        </package>
    </project>

    """


Scenario: parse a class definition with constructor and initializer in a function
  Given I have the source file:
    """
    void foo()
    {
      class A
      {
        A() :
          x(42)
        {
        }

        int x;
      };
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="foo()">
                <class name="A">
                    <method name="A()">
                        <reference target="1"/>
                    </method>
                    <field name="x" id="1"/>
                </class>
            </function>
        </package>
    </project>

    """


Scenario: parse overloaded function
  Given I have the source file:
    """
    int a{};
    int b{};

    void foo(int)
    {
        a;
    }

    void foo(bool)
    {
        b;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <variable name="a" id="1"/>
            <variable name="b" id="2"/>
            <function name="foo(int)">
                <reference target="1"/>
            </function>
            <function name="foo(bool)">
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: parse try catch-all
  Given I have the source file:
    """
    void foo()
    {
      try {
      } catch (...) {
      }
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="foo()"/>
        </package>
    </project>

    """
