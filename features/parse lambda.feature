# language: en

Feature: parse lambda function
  As a developer
  I want to get the AST with lambda functions
  In order to see only the relevant information


Scenario: parse a simple lambda function declaration
  Given I have the source file:
    """
    void func()
    {
      const auto lambda = [](){};
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="func()"/>
        </package>
    </project>

    """


Scenario: call predefined lambda function
  Given I have the source file:
    """
    const auto lambda = [](){};

    void func()
    {
      lambda();
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <variable name="lambda" id="1"/>
            <function name="func()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: can parse lambda with auto type argument
  Given I have the source file:
    """
    #include <vector>
    #include <string>
    #include <algorithm>

    std::string str(const std::vector<std::string>& value)
    {
      std::string result{};
      std::for_each(value.begin(), value.end(), [&result](const auto& itr){
        result += itr + "\n";
      });
      return result;

    }

    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="str(const std::vector&lt;std::string&gt;&amp;)"/>
        </package>
    </project>

    """


Scenario: parse references to types used in lambda
  Given I have the source file "A.cpp":
    """
    class B{};

    auto a = [](const B* value){
      return !value;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="B" id="1"/>
            <variable name="a">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse list of lambdas with arguments
  Given I have the source file "A.cpp":
    """
    #include <functional>
    #include <vector>

    std::vector<std::function<void(int)>> x
    {
      { [](int)
        {
        }
      },
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <variable name="x"/>
        </package>
    </project>

    """


Scenario: parse list of lambdas with references to classes
  Given I have the source file "A.cpp":
    """
    #include <functional>
    #include <vector>

    class A
    {
    };

    class B :
      public A
    {
    };

    std::vector<std::function<void(B&)>> x
    {
      { [](A&)
        {
        }
      },
      { [](B&)
        {
        }
      },
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B" id="2">
                <reference target="1"/>
            </class>
            <variable name="x">
                <reference target="2"/>
                <reference target="2"/>
                <reference target="1"/>
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse list of lambdas with local variables
  Given I have the source file "A.cpp":
    """
    #include <functional>
    #include <vector>

    class A
    {
    };

    std::vector<std::function<void()>> x
    {
      { []()
        {
          A a;
        }
      },
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <variable name="x">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse list of lambdas with class declaration
  Given I have the source file "A.cpp":
    """
    #include <functional>
    #include <vector>

    class A
    {
    };

    std::vector<std::function<void()>> x
    {
      { []()
        {
          class B :
            public A
          {
          };
        }
      },
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <variable name="x">
                <class name="B">
                    <reference target="1"/>
                </class>
            </variable>
        </package>
    </project>

    """

