# language: en

Feature: parse a simple class
  As a developer
  I want to get the AST of a simple class
  In order to see only the relevant information


Scenario: parse an empty C++ class
  Given I have the source file:
    """
    class A
    {
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A"/>
        </package>
    </project>

    """


Scenario: parse two empty C++ class
  Given I have the source file:
    """
    class A
    {
    };

    class B
    {
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A"/>
            <class name="B"/>
        </package>
    </project>

    """


Scenario: ignore classes from system header files
  Given I have the source file:
    """
    #include <iostream>
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name=""/>
    </project>

    """


Scenario: parse a class with a method
  Given I have the source file:
    """
    class A
    {
    public:
      void foo()
      {
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="foo()"/>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a field
  Given I have the source file:
    """
    class A
    {
      int bar{};
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <field name="bar"/>
            </class>
        </package>
    </project>

    """


Scenario: parse a class with a fields and methods
  Given I have the source file:
    """
    class A
    {
    public:
      void foo1()
      {
      }

      void foo2()
      {
      }

    private:
      int bar1{};
      int bar2{};

      void foo3()
      {
      }

    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="foo1()"/>
                <method name="foo2()"/>
                <field name="bar1"/>
                <field name="bar2"/>
                <method name="foo3()"/>
            </class>
        </package>
    </project>

    """


Scenario: parse a class prototpye declaration
  Given I have the source file:
    """
    class A;

    class B
    {
    };

    class A
    {
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A"/>
            <class name="B"/>
        </package>
    </project>

    """


Scenario: parse a static method
  Given I have the source file:
    """
    class A
    {
    public:
      static void foo()
      {
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="foo()"/>
            </class>
        </package>
    </project>

    """


Scenario: parse a static field
  Given I have the source file:
    """
    class A
    {
    public:
      static int x;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <field name="x"/>
            </class>
        </package>
    </project>

    """


Scenario: parse reference to static field via instance
  Given I have the source file "A.cpp":
    """
    struct A
    {
      static int x;
    };

    A z;

    void foo()
    {
      z.x = 42;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <field name="x" id="2"/>
            </class>
            <variable name="z" id="3">
                <reference target="1"/>
            </variable>
            <function name="foo()">
                <reference target="2"/>
                <reference target="3"/>
            </function>
        </package>
    </project>

    """


Scenario: parse a class constructor
  Given I have the source file:
    """
    class A
    {
    public:
      A()
      {
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="A()"/>
            </class>
        </package>
    </project>

    """


Scenario: parse a class destructor
  Given I have the source file:
    """
    class A
    {
    public:
      ~A()
      {
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="~A()"/>
            </class>
        </package>
    </project>

    """


Scenario: parse a class conversion function
  Given I have the source file:
    """
    class A{};

    class X
    {
    public:
      operator bool()
      {
        A a{};
        return {};
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="X">
                <method name="operator bool()">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse constructor of inherited class
  Given I have the source file:
    """
    class A
    {
    };

    class X :
        public A
    {
      public:
        X()
        {
        }
    };


    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="X">
                <reference target="1"/>
                <method name="X()"/>
            </class>
        </package>
    </project>

    """


Scenario: parse method calling each other
  Given I have the source file:
    """
    class A
    {
      void bar();
      void foo();
    };

    void A::bar()
    {
      foo();
    }

    void A::foo()
    {
      bar();
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="bar()" id="1">
                    <reference target="2"/>
                </method>
                <method name="foo()" id="2">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse type reference to class method pointer
  Given I have the source file:
    """
    class A
    {
    };

    void(A::*bar)();
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <variable name="bar">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse type reference to class method pointer parameter
  Given I have the source file:
    """
    class A{};
    class B{};

    void(A::*bar)(int, B*);
    """

  When I parse the source

# we have 2 dependencies from bar to B: one from the bar variable type and one from the parameter in the prototype function
  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B" id="2"/>
            <variable name="bar">
                <reference target="1"/>
                <reference target="2"/>
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse reference to class method pointer
  Given I have the source file:
    """
    class A
    {
    public:
      void foo();
    };

    void A::foo()
    {
    }

    void(A::*bar)() = &A::foo;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <method name="foo()" id="2"/>
            </class>
            <variable name="bar">
                <reference target="1"/>
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse reference to class field pointer
  Given I have the source file:
    """
    class A
    {
    public:
      int x;
    };

    A a{};

    void foo()
    {
      int* y = &a.x;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <field name="x" id="2"/>
            </class>
            <variable name="a" id="3">
                <reference target="1"/>
            </variable>
            <function name="foo()">
                <reference target="2"/>
                <reference target="3"/>
            </function>
        </package>
    </project>

    """


Scenario: parse deduced type reference to class method pointer
  Given I have the source file:
    """
    class A
    {
    public:
      void foo()
      {
      }
    };

    auto const bar = &A::foo;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <method name="foo()" id="2"/>
            </class>
            <variable name="bar">
                <reference target="1"/>
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse class with methods accessing a field
  Given I have the source file:
    """
    class A
    {
      public:
        void set()
        {
          value = true;
        }

        bool get() const
        {
          return value;
        }

      private:
        bool value{false};
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="set()">
                    <reference target="1"/>
                </method>
                <method name="get() const">
                    <reference target="1"/>
                </method>
                <field name="value" id="1"/>
            </class>
        </package>
    </project>

    """


Scenario: parse reference to prototyped class with method pointer
  Given I have the source file:
    """
    class A;

    class A
    {
    public:
      typedef int (A::*B) ();

      struct C
      {
        B b;
      };
    };

    void foo(A*)
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <class name="B" id="2">
                    <reference target="1"/>
                </class>
                <class name="C">
                    <field name="b">
                        <reference target="2"/>
                    </field>
                </class>
            </class>
            <function name="foo(A*)">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse delegating initializer
  Given I have the source file:
    """
    class A
    {
    public:
      A(int)
      {
      }

      A() :
        A(42)
      {
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="A(int)" id="1"/>
                <method name="A()">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse implementation of method referencing types differently than their declaration
  Given I have the source file:
    """
    namespace ns
    {

    class A
    {
      public:
        typedef int B;
        void foo(B) const;

    };

    void A::foo(A::B) const
    {
    }

    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="ns">
                <class name="A">
                    <class name="B" id="1"/>
                    <method name="foo(B) const">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
        </package>
    </project>

    """


Scenario: parse implementation of method referencing types by reference differently than their declaration
  Given I have the source file:
    """
    namespace ns
    {

    class A
    {
      public:
        typedef int B;
        void foo(B&) const;

    };

    void A::foo(A::B&) const
    {
    }

    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="ns">
                <class name="A">
                    <class name="B" id="1"/>
                    <method name="foo(B&amp;) const">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
        </package>
    </project>

    """


Scenario: parse implementation of method when declared in newly opened namespace
  Given I have the source file:
    """
    namespace ns
    {

    class A
    {
      typedef int B;

      void foo(B);
    };

    }

    namespace ns
    {

    void A::foo(A::B)
    {
    }

    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="ns">
                <class name="A">
                    <class name="B" id="1"/>
                    <method name="foo(B)">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
        </package>
    </project>

    """


Scenario: produce method signature independent of declaration
  Given I have the source file:
    """
    namespace ns
    {

    class A
    {
      public:
        class B{};
        void m1(B){}
        void m2(A::B){}
        void m3(ns::A::B){}

    };

    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="ns">
                <class name="A">
                    <class name="B" id="1"/>
                    <method name="m1(B)">
                        <reference target="1"/>
                    </method>
                    <method name="m2(A::B)">
                        <reference target="1"/>
                    </method>
                    <method name="m3(ns::A::B)">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
        </package>
    </project>

    """


Scenario: parse const and non const method
  Given I have the source file:
    """
    int x;
    int y;

    class A
    {
      public:
        void foo()
        {
          x;
        }

        void foo() const
        {
          y;
        }

    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <variable name="x" id="1"/>
            <variable name="y" id="2"/>
            <class name="A">
                <method name="foo()">
                    <reference target="1"/>
                </method>
                <method name="foo() const">
                    <reference target="2"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse references from pure virtual functions
  Given I have the source file:
    """
    class A
    {
      public:
        typedef int B;

        virtual void test(B) = 0;

    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <class name="B" id="1"/>
                <method name="test(B)">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: call destructor as method
  Given I have the source file:
    """
    class A
    {
    public:
      ~A() = default;
    };

    A* x;

    void foo()
    {
      x->~A();
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <method name="~A()" id="2"/>
            </class>
            <variable name="x" id="3">
                <reference target="1"/>
            </variable>
            <function name="foo()">
                <reference target="2"/>
                <reference target="3"/>
            </function>
        </package>
    </project>

    """


Scenario: parse class with 2 unnamed unions
  Given I have the source file:
    """
    class A
    {
      union
      {
        int a;
      };

      union
      {
        int b;
      };

      int x = a;
      int y = b;
    };
    """

  When I parse the source

  Then I expect this part in the error output: "input.cpp:3:3: found union without name"
  And I expect this part in the error output: "input.cpp:8:3: found union without name"
  And I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <class name="">
                    <field name="a" id="1"/>
                </class>
                <class name="">
                    <field name="b" id="2"/>
                </class>
                <field name="x">
                    <reference target="1"/>
                </field>
                <field name="y">
                    <reference target="2"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: parse 2 empty unions without a name
  Given I have the source file:
    """
    class A
    {
      union
      {
      };

      union
      {
      };
    };
    """

  When I parse the source

  Then I expect this part in the error output: "input.cpp:3:3: found union without name"
  And I expect this part in the error output: "input.cpp:7:3: found union without name"
  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A"/>
        </package>
    </project>

    """


Scenario: parse 2 unnamed structs
  Given I have the source file:
    """
    struct
    {
      int a;
    } A;

    struct
    {
      int b;
    } B;
    """

  When I parse the source

  Then I expect this part in the error output: "input.cpp:1:1: found struct without name"
  And I expect this part in the error output: "input.cpp:6:1: found struct without name"
  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="" id="1">
                <field name="a"/>
            </class>
            <variable name="A">
                <reference target="1"/>
            </variable>
            <class name="" id="2">
                <field name="b"/>
            </class>
            <variable name="B">
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse unnamed union with 2 unnamed structs in it
  Given I have the source file:
    """
    union
    {
      struct
      {
        int a;
      } A;

      struct
      {
        int b;
      } B;
    } C;
    """

  When I parse the source

  Then I expect this part in the error output: "input.cpp:1:1: found union without name"
  And I expect this part in the error output: "input.cpp:3:3: found struct without name"
  And I expect this part in the error output: "input.cpp:8:3: found struct without name"
  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="" id="1">
                <class name="" id="2">
                    <field name="a"/>
                </class>
                <field name="A">
                    <reference target="2"/>
                </field>
                <class name="" id="3">
                    <field name="b"/>
                </class>
                <field name="B">
                    <reference target="3"/>
                </field>
            </class>
            <variable name="C">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse unnamed structs that contain with elements with same name
  Given I have the source file:
    """
    struct
    {
      int a;
      char z;
    } A;

    struct
    {
      int b;
      char z;
    } B;
    """

  When I parse the source

  Then I expect this part in the error output: "input.cpp:1:1: found struct without name"
  And I expect this part in the error output: "input.cpp:7:1: found struct without name"
  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="" id="1">
                <field name="a"/>
                <field name="z"/>
            </class>
            <variable name="A">
                <reference target="1"/>
            </variable>
            <class name="" id="2">
                <field name="b"/>
                <field name="z"/>
            </class>
            <variable name="B">
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse unnamed structs that contain only elements with same name
  Given I have the source file:
    """
    struct
    {
      int y;
      char z;
    } A;

    struct
    {
      int y;
      char z;
    } B;
    """

  When I parse the source

  Then I expect this part in the error output: "input.cpp:1:1: found struct without name"
  And I expect this part in the error output: "input.cpp:7:1: found struct without name"
  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="" id="1">
                <field name="y"/>
                <field name="z"/>
            </class>
            <variable name="A">
                <reference target="1"/>
            </variable>
            <class name="" id="2">
                <field name="y"/>
                <field name="z"/>
            </class>
            <variable name="B">
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: declare class of same name in methods of same name
  Given I have the header file "AB.h":
    """
    #pragma once

    class A
    {
      void foo();
    };

    class B
    {
      void foo();
    };
    """
  And I have the source file "AB.cpp":
    """
    #include "AB.h"

    void A::foo()
    {
      class X{};
    }

    void B::foo()
    {
      class X{};
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="foo()">
                    <class name="X"/>
                </method>
            </class>
            <class name="B">
                <method name="foo()">
                    <class name="X"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: declare class of same name in same method twice
  Given I have the source file:
    """
    class A
    {
      void* foo(bool pred)
      {
        if (pred) {
          class B
          {
          };
          return new B();
        } else {
          class B
          {
          };
          return new B();
        }
      }
    };

    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="foo(bool)">
                    <reference target="1"/>
                    <reference target="2"/>
                    <class name="B" id="1"/>
                    <class name="B" id="2"/>
                </method>
            </class>
        </package>
    </project>

    """
