# language: en

Feature: parse variables
  As a developer
  I want to get the AST with variables
  In order to see only the relevant information


Scenario: parse a simple variable
  Given I have the source file:
    """
    int a;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <variable name="a"/>
        </package>
    </project>

    """


Scenario: parse a variable with class references
  Given I have the source file:
    """
    class A{};

    A a{};
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <variable name="a">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse a variable with variable references in constructor
  Given I have the source file:
    """
    int a{};
    int b{a};
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <variable name="a" id="1"/>
            <variable name="b">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse a variable with variable references in assignment
  Given I have the source file:
    """
    int a{};
    int b = a;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <variable name="a" id="1"/>
            <variable name="b">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse a variable with function references
  Given I have the source file:
    """
    int bar()
    {
      return {};
    }

    int a = bar();
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="bar()" id="1"/>
            <variable name="a">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse a function with variable references
  Given I have the source file:
    """
    int a;

    int bar()
    {
      return a;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <variable name="a" id="1"/>
            <function name="bar()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse a variable with a decltype type
  Given I have the source file:
    """
    class A{};

    A a;
    decltype(a) b;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <variable name="a" id="2">
                <reference target="1"/>
            </variable>
            <variable name="b">
                <reference target="1"/>
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse variable declared with extern
  Given I have the header file "A.h":
    """
    #pragma once

    class A{};

    extern A* a;
    """
  And I have the source file "A.cpp":
    """
    #include "A.h"

    A* a{};
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <variable name="a">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse references to extern lambda variable
  Given I have the header file "A.h":
    """
    #pragma once

    #include <functional>

    typedef std::function<bool(int)> C;
    extern C a;
    """
  And I have the source file "A.cpp":
    """
    #include "A.h"

    C a = [](int value){
      return !value;
    };
    """
  And I have the source file "B.cpp":
    """
    #include "A.h"

    void bar()
    {
      a(42);
    }
    """
  And I have the source file "C.cpp":
    """
    #include "A.h"

    void foo()
    {
      a({});
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="C" id="1"/>
            <variable name="a" id="2">
                <reference target="1"/>
            </variable>
            <function name="bar()">
                <reference target="2"/>
            </function>
            <function name="foo()">
                <reference target="2"/>
            </function>
        </package>
    </project>

    """
