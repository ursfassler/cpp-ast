/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AstBuilder.h"
#include "component/ast/Nodes.h"
#include "component/ast/logger.h"
#include <cassert>


namespace graph
{


AstBuilder::AstBuilder(const Logger& logger_) :
  logger{logger_}
{
}

void AstBuilder::beginProject(const Name& name)
{
  assert(stack.empty());

  root = new Root(name);
}

void AstBuilder::endProject()
{
  assert(stack.empty());

  for (const auto& edge : edges) {
    const auto source = edge.first;
    for (const auto& dst : edge.second) {
      const auto destination = idToElement[dst];
      if (destination) {
        source->addReference(destination);
      } else {
        logger.error("reference target not found: " + dst);
      }
    }
  }

  assert(stack.empty());
}

void AstBuilder::beginNode(AstBuilder::Type type, const Name& name, const std::optional<Path>& path, const std::optional<Id>& id)
{
  if (stack.empty()) {
    assert(type == AstBuilder::Type::Package);
    assert(name == "");
    assert(!id);
    assert(!path);

    stack.push_back(&root->top);

    return;
  }

  assert(!stack.empty());

  auto* node = produce(type, name, path.value_or(""));
  if (id) {
    idToElement[*id] = dynamic_cast<Declaration*>(node);
  }

  if (type == Builder::Type::Package) {
    dynamic_cast<Package*>(stack.back())->addPackage(dynamic_cast<Package*>(node));
  } else {
    stack.back()->addChild(dynamic_cast<Declaration*>(node));
  }
  stack.push_back(node);
}

void AstBuilder::endNode()
{
  assert(!stack.empty());
  stack.pop_back();
}

void AstBuilder::addEdgeTo(const AstBuilder::Id& id)
{
  edges[dynamic_cast<Declaration*>(stack.back())].push_back(id);
}

Root* AstBuilder::get() const
{
  assert(root);
  assert(stack.empty());
  return root;
}

Node* AstBuilder::produce(Type type, const Name& name, const Id& id) const
{
  switch (type) {
    case Type::Class: return new Class(name, id);
    case Type::Field: return new Field(name, id);
    case Type::Method: return new Method(name, id);
    case Type::Package: return new Package(name);
    case Type::Function: return new Function(name, id);
    case Type::Variable: return new Variable(name, id);
  }
  assert(false);
  return {};
}


}
