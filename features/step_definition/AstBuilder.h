/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Builder.h"
#include <map>
#include <vector>


class Root;
class Node;
class Declaration;

class Logger;

namespace graph
{

class AstBuilder :
    public Builder
{
public:
  AstBuilder(const Logger&);

  void beginProject(const Name&) override;
  void endProject() override;

  void beginNode(Type, const Name&, const std::optional<Path>&, const std::optional<Id>&) override;
  void endNode() override;

  void addEdgeTo(const Id&) override;

  Root* get() const;

private:
  const Logger& logger;
  Root* root{};

  std::vector<Node*> stack{};

  std::map<Id, Declaration*> idToElement{};
  std::map<Declaration*, std::vector<Id>> edges{};
  Node* produce(Type, const Name&, const Id&) const;

};


}
