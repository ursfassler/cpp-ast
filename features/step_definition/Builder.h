/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <optional>

namespace graph
{


class Builder
{
public:
  typedef std::string Id;
  typedef std::string Path;
  typedef std::string Name;
  enum class Type
  {
    Package,
    Class,
    Field,
    Method,
    Variable,
    Function,
  };

  virtual ~Builder() = default;

  virtual void beginProject(const Name&) = 0;
  virtual void endProject() = 0;

  virtual void beginNode(Type, const Name&, const std::optional<Path>&, const std::optional<Id>&) = 0;
  virtual void endNode() = 0;

  virtual void addEdgeTo(const Id&) = 0;

};


}
