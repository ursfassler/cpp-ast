/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "GraphParser.h"
#include "Builder.h"
#include "component/ast/logger.h"


namespace graph
{


GraphParser::GraphParser(
    Builder& builder_,
    const Logger& logger_
    ) :
  builder{builder_},
  logger{logger_},
  Parser
  {
    {"class", std::bind(&GraphParser::parseClass, this, std::placeholders::_1)},
    {"package", std::bind(&GraphParser::parsePackage, this, std::placeholders::_1)},
    {"function", std::bind(&GraphParser::parseFunction, this, std::placeholders::_1)},
    {"variable", std::bind(&GraphParser::parseVariable, this, std::placeholders::_1)},
  }
{
}

void GraphParser::parse(const TiXmlDocument &document)
{
  const auto child = document.FirstChildElement();
  if (!child) {
    return;
  }

  const auto cname = child->Attribute("name");
  const std::string name = cname ? cname : "";

  builder.beginProject(name);

  const auto topPackageNode = child->FirstChildElement("package");
  if (!topPackageNode) {
    logger.error("missing top level package");
    builder.endProject();
    return;
  }
  const auto lastTopPackageNode = child->LastChild("package");
  if (topPackageNode != lastTopPackageNode) {
    logger.error("multiple top level packages");
  }

  parsePackage(topPackageNode);

  builder.endProject();
}


std::optional<std::string> GraphParser::getPath(const TiXmlElement* element) const
{
  const auto path = element->Attribute("path");
  if (path) {
    return {path};
  } else {
    return {};
  }
}

std::optional<std::string> GraphParser::getId(const TiXmlElement* element) const
{
  const auto id = element->Attribute("id");
  if (id) {
    return {id};
  } else {
    return {};
  }
}

void GraphParser::addEdgeTo(const TiXmlElement* element)
{
  const auto target = element->Attribute("target");
  builder.addEdgeTo(target);
}

void GraphParser::handleUnknown(const TiXmlElement* element)
{
  const std::string type = element->Value();
  logger.error("unknown element found: " + type + " (" + std::to_string(element->Row()) + ":" + std::to_string(element->Column()) + ")");
}

void GraphParser::parseField(const TiXmlElement* element)
{
  const auto name = element->Attribute("name");
  builder.beginNode(Builder::Type::Field, name, getPath(element), getId(element));
  parseMemberContent(element);
  builder.endNode();
}

void GraphParser::parseMethod(const TiXmlElement* element)
{
  const auto name = element->Attribute("name");
  builder.beginNode(Builder::Type::Method, name, getPath(element), getId(element));
  parseMemberContent(element);
  builder.endNode();
}

void GraphParser::parseMemberContent(const TiXmlElement* element)
{
  const std::string childName = element->Attribute("name");
  for (const TiXmlElement *child = element->FirstChildElement(); child != nullptr; child = child->NextSiblingElement()) {
    const std::string type = child->Value();
    if (type == "reference") {
      addEdgeTo(child);
    } else {
      handleUnknown(child);
    }
  }
}


void GraphParser::parseClass(const TiXmlElement* element)
{
  const auto name = element->Attribute("name");
  builder.beginNode(Builder::Type::Class, name, getPath(element), getId(element));

  for (const TiXmlElement *child = element->FirstChildElement(); child != nullptr; child = child->NextSiblingElement()) {
    const std::string type = child->Value();
    if (type == "field") {
      parseField(child);
    } else if (type == "method") {
      parseMethod(child);
    } else if (type == "class") {
      parseClass(child);
    } else if (type == "reference") {
      addEdgeTo(child);
    } else {
      handleUnknown(child);
    }
  }

  builder.endNode();
}

void GraphParser::parsePackage(const TiXmlElement *element)
{
  const auto name = element->Attribute("name");
  builder.beginNode(Builder::Type::Package, name, getPath(element), getId(element));

  parseChildren(element);

  builder.endNode();
}

void GraphParser::parseNodeWithNoChildren(const TiXmlElement *element)
{
  for (const TiXmlElement *child = element->FirstChildElement(); child != nullptr; child = child->NextSiblingElement()) {
    const std::string type = child->Value();
    if (type == "reference") {
      addEdgeTo(child);
    } else {
      handleUnknown(child);
    }
  }
}

void GraphParser::parseFunction(const TiXmlElement* element)
{
  const auto name = element->Attribute("name");
  builder.beginNode(Builder::Type::Function, name, getPath(element), getId(element));
  parseNodeWithNoChildren(element);
  builder.endNode();
}

void GraphParser::parseVariable(const TiXmlElement* element)
{
  const auto name = element->Attribute("name");
  builder.beginNode(Builder::Type::Variable, name, getPath(element), getId(element));
  parseNodeWithNoChildren(element);
  builder.endNode();
}

void GraphParser::parseChildren(const TiXmlElement* element)
{
  for (const TiXmlElement *child = element->FirstChildElement(); child != nullptr; child = child->NextSiblingElement()) {
    const std::string type = child->Value();

    const auto idx = Parser.find(type);
    if (idx == Parser.end()) {
      handleUnknown(child);
    } else {
      idx->second(child);
    }
  }
}


}
