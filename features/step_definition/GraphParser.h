/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <tinyxml.h>
#include <map>
#include <functional>
#include <optional>

class Logger;

namespace graph
{

class Builder;


class GraphParser
{
  public:
    GraphParser(
        Builder&,
        const Logger&
        );

    void parse(const TiXmlDocument&);

  private:
    Builder& builder;
    const Logger& logger;

    const std::map<std::string, std::function<void(const TiXmlElement*)>> Parser;

    void addEdgeTo(const TiXmlElement*);
    std::optional<std::string> getPath(const TiXmlElement*) const;
    std::optional<std::string> getId(const TiXmlElement*) const;

    void handleUnknown(const TiXmlElement*);
    void parseChildren(const TiXmlElement*);
    void parseField(const TiXmlElement*);
    void parseMethod(const TiXmlElement*);
    void parseMemberContent(const TiXmlElement*);
    void parseClass(const TiXmlElement*);
    void parsePackage(const TiXmlElement*);
    void parseFunction(const TiXmlElement*);
    void parseVariable(const TiXmlElement*);
    void parseNodeWithNoChildren(const TiXmlElement*);
};


}
