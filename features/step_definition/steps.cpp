/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AstBuilder.h"
#include "GraphParser.h"
#include "component/ast/Node.h"
#include "component/ast/logger.h"
#include "component/linker/link.h"
#include "component/parser/InputArguments.h"
#include "component/parser/parse.h"
#include "component/writer/NodePrinter.h"
#include "component/writer/NumberedId.h"
#include "component/writer/Writer.h"
#include <algorithm>
#include <fstream>
#include <iterator>
#include <nlohmann/json.hpp>
#include <optional>
#include <stdio.h>
#include <stdlib.h>
#include <tinyxml.h>
#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>

#include <clang-c/Index.h>


namespace feature_test
{
namespace
{

class Context
{
  public:
    std::string directory{};
    std::stringstream output{};
    StdLogger stdLogger{};
    MemoryLogger memorLogger{};
    LogDistributor logger{stdLogger, memorLogger};

    void writeFile(const std::string& content, const std::string& filename)
    {
      std::ofstream file{path(filename)};
      file << content;

      fileNames.push_back(path(filename));
    }

    void deleteFiles()
    {
      for (const auto& file : fileNames) {
        deleteFile(file);
      }
      fileNames.clear();
    }

    void registerSourcefile(const std::string& filename)
    {
      sourceFileNames.push_back(path(filename));
    }

    const std::vector<std::string>& sourceFiles() const
    {
      return sourceFileNames;
    }

    std::string path(const std::string& filename) const
    {
      return directory + "/" + filename;
    }

  private:
    std::vector<std::string> sourceFileNames{};
    std::vector<std::string> fileNames{};

    void deleteFile(const std::string& filename)
    {
      const auto ret = ::unlink(filename.c_str());
      if (ret != 0) {
        throw std::runtime_error("could not delete file: " + filename);
      }
    }

};

std::string createTemporaryDirectory()
{
  char filename[] = "cpp-ast-XXXXXX";
  const auto ret = mkdtemp(filename);
  if (!ret) {
    throw std::runtime_error("could not create temporary file");
  }

  const auto pwd = get_current_dir_name();
  const auto full = std::string{pwd} + "/" + std::string{ret};

  free(pwd);

  return full;
}

void deleteDirectory(const std::string& directory)
{
  const auto ret = ::rmdir(directory.c_str());
  if (ret != 0) {
    throw std::runtime_error("could not delete directory: " + directory);
  }
}

void parseFiles(std::ostream& stream, std::string dbPath, Logger& logger)
{
  const std::vector<std::string> arg = {
    "cpp-ast",
    dbPath,
  };

  InputArguments args{logger};
  args.parse(arg);

  ASSERT_TRUE(args.allOk());

  const auto ret = parse(args.sources(), args.compilationDatabase(), stream, logger);

  ASSERT_TRUE(ret);
}

void linkFiles(std::ostream& stream, const std::vector<std::string>& files, Logger& logger)
{
  Root out{{}};

  for (const auto& filename : files) {
    graph::AstBuilder builder{logger};
    graph::GraphParser gp{builder, logger};

    TiXmlDocument xml_doc;
    const auto success = xml_doc.LoadFile(filename);
    ASSERT_TRUE(success);
    gp.parse(xml_doc);

    const auto project = builder.get();

    linkInto(&out, project, logger);
  }

  NumberedId ids{out};
  NodePrinter printer{&out, ids.idGetter(), NodePrinter::PathFromNode, logger};

  xml::Writer writer{stream};
  printer.print(writer);
}

bool isCpp(const std::string& filename)
{
  const auto end = filename.substr(filename.size()-4);
  return end == ".cpp";
}

std::string compileCommand(const std::string& filename)
{
  if (isCpp(filename)) {
    return "clang++ -std=gnu++17 " + filename;
  } else {
    return "clang -std=c11 " + filename;
  }
}

std::string createCompilationDatabase(const std::vector<std::string>& sources, const std::string& directory)
{
  nlohmann::json json{};

  for (const std::string& filename : sources) {
    nlohmann::json entry{};
    entry["directory"] = directory;
    entry["command"] = compileCommand(filename);
    entry["file"] = filename;
    json.push_back(entry);
  }

  return json.dump();
}

BEFORE()
{
  cucumber::ScenarioScope<Context> context;
  context->directory = createTemporaryDirectory();
}

AFTER()
{
  cucumber::ScenarioScope<Context> context;

  context->deleteFiles();
  deleteDirectory(context->directory);
}

GIVEN("^I have the C source file:$")
{
  const auto filename = "input.c";
  REGEX_PARAM(std::string, input);

  cucumber::ScenarioScope<Context> context;
  context->registerSourcefile(filename);
  context->writeFile(input, filename);
}

GIVEN("^I have the source file:$")
{
  const auto filename = "input.cpp";
  REGEX_PARAM(std::string, input);

  cucumber::ScenarioScope<Context> context;
  context->registerSourcefile(filename);
  context->writeFile(input, filename);
}

GIVEN("^I have the source file \"([^\"]*)\":$")
{
  REGEX_PARAM(std::string, filename);
  REGEX_PARAM(std::string, input);

  cucumber::ScenarioScope<Context> context;
  context->registerSourcefile(filename);
  context->writeFile(input, filename);
}

GIVEN("^I have the header file \"([^\"]*)\":$")
{
  REGEX_PARAM(std::string, filename);
  REGEX_PARAM(std::string, input);

  cucumber::ScenarioScope<Context> context;
  context->writeFile(input, filename);
}

GIVEN("^I have the AST file \"([^\"]*)\":$")
{
  REGEX_PARAM(std::string, filename);
  REGEX_PARAM(std::string, input);

  cucumber::ScenarioScope<Context> context;
  context->writeFile(input, filename);
}

WHEN("^I parse the source$")
{
  cucumber::ScenarioScope<Context> context;

  const auto db = createCompilationDatabase(context->sourceFiles(), context->directory);
  context->writeFile(db, "compile_commands.json");

  const auto dbPath = context->path("compile_commands.json");
  parseFiles(context->output, dbPath, context->logger);
}

WHEN("^I link the files \"([^\"]*)\" and \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, filename1);
  REGEX_PARAM(std::string, filename2);

  cucumber::ScenarioScope<Context> context;
  linkFiles(context->output, {context->path(filename1), context->path(filename2)}, context->logger);
}

THEN("^I expect the output:$")
{
  REGEX_PARAM(std::string, expected);

  cucumber::ScenarioScope<Context> context;
  ASSERT_EQ(expected, context->output.str());
}

THEN("^I expect no error output$")
{
  cucumber::ScenarioScope<Context> context;
  const decltype (context->memorLogger.messages) expected{};
  ASSERT_EQ(expected, context->memorLogger.messages);
}

THEN("^I expect this part in the error output: \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, expected);

  cucumber::ScenarioScope<Context> context;
  std::string actual{};
  for (const auto& message : context->memorLogger.messages) {
    actual += message + "\n";
  }
  const auto pos = actual.find(expected);
  const auto found = pos != std::string::npos;

  ASSERT_TRUE(found) << "can not find \"" + expected + "\" in: " + actual;
}


}
}
