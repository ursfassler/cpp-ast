# language: en

Feature: parse template classes
  As a developer
  I want to get the AST with template classes
  In order to see only the relevant information


Scenario: parse simple class template specialization
  Given I have the source file:
    """
    template<typename T>
    class A
    {
    };

    void test()
    {
      A<int> a{};
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1"/>
            <function name="test()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse simple non type template specialization
  Given I have the source file:
    """
    template<int N>
    class A
    {
      int x{N};
    };

    void test()
    {
      A<42> a{};
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;int&gt;" id="1">
                <field name="x"/>
            </class>
            <function name="test()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse template parameter pack
  Given I have the source file:
    """
    template<typename... Ts>
    class A
    {
    public:
      A(Ts... ts)
      {
      }
    };

    class B{};

    void test()
    {
      B b{};
      A a{b, true, 42};
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="A&lt;Ts...&gt;(Ts...)" id="2"/>
            </class>
            <class name="B" id="3"/>
            <function name="test()">
                <reference target="3"/>
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: add template arguments as references
  Given I have the source file:
    """
    template<typename T>
    class A{};

    class B{};

    void test()
    {
      A<B> a{};
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1"/>
            <class name="B" id="2"/>
            <function name="test()">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: template class with method
  Given I have the source file:
    """
    template<typename T>
    class A
    {
    public:
      void foo(){}
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;">
                <method name="foo()"/>
            </class>
        </package>
    </project>

    """


Scenario: call method of template class
  Given I have the source file:
    """
    template<typename T>
    class A
    {
    public:
      void foo(T)
      {
      }
    };

    void test()
    {
      A<int> a{};
      a.foo(42);
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="foo(T)" id="2"/>
            </class>
            <function name="test()">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: access field of template class
  Given I have the source file:
    """
    template<typename T>
    class A
    {
    public:
      T x;
    };

    void test()
    {
      A<int> a{};
      a.x = 42;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <field name="x" id="2"/>
            </class>
            <function name="test()">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


@todo
Scenario: constructor and destructor of template class have wrong names
  I expect the name to be without the template parameter name
  Given I have the source file:
    """
    template<typename T>
    class A
    {
    public:
      A(){}
      ~A(){}
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;">
                <method name="A&lt;T&gt;()"/>
                <method name="~A&lt;T&gt;()"/>
            </class>
        </package>
    </project>

    """


Scenario: add constructor reference
  Given I have the source file:
    """
    template<typename T>
    class A
    {
    public:
      A()
      {
      }
    };

    void test()
    {
      A<int> a{};
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="A&lt;T&gt;()" id="2"/>
            </class>
            <function name="test()">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: add constructor arguments as references
  Given I have the source file:
    """
    template<typename T>
    class A
    {
    public:
      A(int)
      {
      }
    };

    int b{42};

    void test()
    {
      A<int> a1{b};
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="A&lt;T&gt;(int)" id="2"/>
            </class>
            <variable name="b" id="3"/>
            <function name="test()">
                <reference target="1"/>
                <reference target="2"/>
                <reference target="3"/>
            </function>
        </package>
    </project>

    """


Scenario: parse two different specialization of a class template
  Given I have the source file:
    """
    template<typename T>
    class A
    {
    public:
      A()
      {
      }
    };

    void test()
    {
      A<int> a1{};
      A<float> a2{};
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="A&lt;T&gt;()" id="2"/>
            </class>
            <function name="test()">
                <reference target="1"/>
                <reference target="2"/>
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: parse class template without explicit specialization
  Given I have the source file:
    """
    template<typename T>
    class A
    {
    public:
      A(T)
      {
      }
    };

    void test()
    {
      A a1{42};
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="A&lt;T&gt;(T)" id="2"/>
            </class>
            <function name="test()">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: specialize chain of class template
  Given I have the source file:
    """
    template<typename T>
    class A
    {
    };

    template<typename T>
    class B
    {
      A<T> a;
    };

    void test()
    {
      B<bool> b{};
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1"/>
            <class name="B&lt;typename&gt;" id="2">
                <field name="a">
                    <reference target="1"/>
                </field>
            </class>
            <function name="test()">
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: add template arguments as references in template
  Given I have the source file:
    """
    template<typename T, typename R>
    class A
    {
    };

    class B{};

    template<typename T>
    class C
    {
      A<T, B> a;
    };

    void test()
    {
      C<bool> b{};
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename, typename&gt;" id="1"/>
            <class name="B" id="2"/>
            <class name="C&lt;typename&gt;" id="3">
                <field name="a">
                    <reference target="1"/>
                    <reference target="2"/>
                </field>
            </class>
            <function name="test()">
                <reference target="3"/>
            </function>
        </package>
    </project>

    """


Scenario: parse type alias template class specialization
  Given I have the source file:
    """
    class A{};

    template<typename T>
    using B = A;

    B<int> x;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B&lt;typename&gt;" id="2">
                <reference target="1"/>
            </class>
            <variable name="x">
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse type alias template class specialization
  Given I have the source file:
    """
    template<typename T, typename S>
    class A{};

    class B{};

    template<typename T>
    using C = A<T, B>;

    class D{};

    C<D> x;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename, typename&gt;" id="1"/>
            <class name="B" id="2"/>
            <class name="C&lt;typename&gt;" id="3">
                <reference target="1"/>
                <reference target="2"/>
            </class>
            <class name="D" id="4"/>
            <variable name="x">
                <reference target="3"/>
                <reference target="4"/>
            </variable>
        </package>
    </project>

    """


Scenario: class template partial specialization
  Given I have the source file:
    """
    template<typename T, typename S>
    struct A
    {
    };

    template <typename T>
    struct A<T, int>
    {
      using type = T;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename, typename&gt;" id="1"/>
            <class name="A&lt;type-parameter-0-0, int&gt;">
                <reference target="1"/>
            </class>
        </package>
    </project>

    """


Scenario: parse type alias declaration without template
  Given I have the source file:
    """
    class A
    {
    };

    using B = A;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B">
                <reference target="1"/>
            </class>
        </package>
    </project>

    """


Scenario: class template specialization
  Given I have the source file:
    """
    template <bool parameter> struct A
    {
    };

    template <> struct A<true>
    {
      int a;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;bool&gt;" id="1"/>
            <class name="A&lt;1&gt;">
                <reference target="1"/>
                <field name="a"/>
            </class>
        </package>
    </project>

    """


Scenario: parse call template class method
  Given I have the header file "A.h":
    """
    #include <stddef.h>

    template<class T>
    class A {
    public:
      void foo();
    };

    template<class T>
    void A<T>::foo()
    {
    }
    """
  And I have the source file "X.cpp":
    """
    #include "A.h"

    void func()
    {
        A<int> x;
        x.foo();
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="foo()" id="2"/>
            </class>
            <function name="func()">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: parse template member specialization
  Given I have the source file:
    """
    template<class T>
    class A
    {
    public:
      virtual void foo() = 0;
    };

    class B
    {
    public:
      void bar()
      {
        x->foo();
      }

      A<int>* x;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="foo()" id="2"/>
            </class>
            <class name="B">
                <method name="bar()">
                    <reference target="2"/>
                    <reference target="3"/>
                </method>
                <field name="x" id="3">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: reference to template member specialization method
  Given I have the source file:
    """
    #include <stddef.h>

    template<class T>
    class A {
    public:
      T operator[](size_t)
      {
        return {};
      }
    };

    void func()
    {
        A<int> x;
        x[4];
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="operator[](size_t)" id="2"/>
            </class>
            <function name="func()">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: parse template type parameter
  Given I have the source file:
    """
    class A
    {
    };

    template<A* T>
    class B
    {
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B&lt;A*&gt;">
                <reference target="1"/>
            </class>
        </package>
    </project>

    """


Scenario: parse reference to non-templated class whose parent is a class template specialization
  Given I have the source file:
    """
    template<typename T>
    class A
    {
    public:
      class B
      {
      };
    };

    void func()
    {
      A<int>::B x;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;">
                <class name="B" id="1"/>
            </class>
            <function name="func()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse injected class name type
  Given I have the source file:
    """
    #include <stddef.h>

    template<class T>
    class A
    {
    public:
      A(const A&)
      {
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="A&lt;T&gt;(const A&lt;T&gt;&amp;)">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse dependent name type
  Given I have the source file:
    """
    template<typename T>
    class A
    {
    public:
      class B
      {
      };
    };


    template<typename S>
    class C
    {
    public:
      void foo()
      {
        typename A<S>::B D;
      }
    };

    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <class name="B"/>
            </class>
            <class name="C&lt;typename&gt;">
                <method name="foo()">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse templated static method of class
  Given I have the source file:
    """
    class A
    {
    public:
      template<typename T>
      static void foo()
      {
      }
    };

    void bar()
    {
      A::foo<int>();
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="foo&lt;typename&gt;()" id="1"/>
            </class>
            <function name="bar()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse templated method of a class
  Given I have the source file:
    """
    class A
    {
    public:
      template<typename T>
      void bar(T)
      {
      }

      void foo()
      {
        bar(1.23);
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="bar&lt;typename&gt;(T)" id="1"/>
                <method name="foo()">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse reference to same template with destructor from two translation units
  Given I have the header file "A.h":
    """
    #pragma once

    template<typename T>
    class A
    {
      public:
        ~A()
        {
        }
    };
    """
  And I have the source file "X.cpp":
    """
    #include "A.h"

    A<int> x{};
    """
  And I have the source file "Y.cpp":
    """
    #include "A.h"

    A<int> y{};
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="~A&lt;T&gt;()"/>
            </class>
            <variable name="x">
                <reference target="1"/>
            </variable>
            <variable name="y">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: pointer to method of templated class
  Given I have the source file:
    """
    class B{};

    template<typename T>
    class A
    {
    public:
      void bar(B)
      {
      }
    };

    auto x = &A<int>::bar;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="B" id="1"/>
            <class name="A&lt;typename&gt;" id="2">
                <method name="bar(B)" id="3">
                    <reference target="1"/>
                </method>
            </class>
            <variable name="x">
                <reference target="2"/>
                <reference target="1"/>
                <reference target="3"/>
            </variable>
        </package>
    </project>

    """


Scenario: don't introduce dependencies between packages when specialize templates
  Given I have the source file:
    """
    namespace a
    {

    template<typename T, typename S>
    class A
    {
      void method(T t, S s)
      {
        t.foo();
        s.bar();
      }
    };

    }

    namespace b
    {

    class B
    {
      void foo()
      {
      }
    };

    }

    namespace c
    {

    class C
    {
      void bar()
      {
      }
    };

    }

    namespace x
    {

    a::A<b::B, c::C> v;

    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="A&lt;typename, typename&gt;" id="1">
                    <method name="method(T, S)"/>
                </class>
            </package>
            <package name="b">
                <class name="B" id="2">
                    <method name="foo()"/>
                </class>
            </package>
            <package name="c">
                <class name="C" id="3">
                    <method name="bar()"/>
                </class>
            </package>
            <package name="x">
                <variable name="v">
                    <reference target="1"/>
                    <reference target="2"/>
                    <reference target="3"/>
                </variable>
            </package>
        </package>
    </project>

    """


Scenario: parse constructor with function template
  Given I have the source file:
    """
    template <typename T>
    class A
    {
    public:

      template <class B>
      A(const A<B>& c)
      {
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="A&lt;T&gt;&lt;typename&gt;(const A&lt;B&gt;&amp;)">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: bind templated method
  Given I have the source file:
    """
    #include <functional>

    class A
    {
    };

    template<typename T>
    class B
    {
    public:
      void foo(T)
      {
      }
    };


    void test()
    {
      B<A*> b{};

      std::bind(&B<A*>::foo, &b, std::placeholders::_1);
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B&lt;typename&gt;" id="2">
                <method name="foo(T)" id="3"/>
            </class>
            <function name="test()">
                <reference target="2"/>
                <reference target="1"/>
                <reference target="2"/>
                <reference target="1"/>
                <reference target="1"/>
                <reference target="3"/>
            </function>
        </package>
    </project>

    """


Scenario: parse templated operator
  Given I have the source file:
    """
    template <typename T, typename R>
    class A
    {
    public:
      template <class X, class Y>
      A<T,R>& operator=(const A<X,Y>&)
      {
        return {};
      }

    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename, typename&gt;" id="1">
                <method name="operator=&lt;typename, typename&gt;(const A&lt;X, Y&gt;&amp;)">
                    <reference target="1"/>
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse usage of templated operator
  Given I have the source file:
    """
    class A
    {
    public:
      template<typename T>
      A& operator<< (const T&)
      {
        return *this;
      }
    };

    void foo()
    {
      A x{};

      x << 42;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1">
                <method name="operator&lt;&lt;&lt;typename&gt;(const T&amp;)" id="2">
                    <reference target="1"/>
                </method>
            </class>
            <function name="foo()">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: parse class with call to templated static method
  Given I have the source file:
    """
    class A
    {
        template<typename T>
        static void foo(T x)
        {
          foo(x);
        }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A">
                <method name="foo&lt;typename&gt;(T)" id="1">
                    <reference target="1"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse templated operator of templated class
  Given I have the source file:
    """
    template<typename T>
    class A
    {
    public:
      template<class B>
      A<T> operator+(B* c)
      {
        return {};
      }

    };

    void foo()
    {
      A<int> a{};
      A<int> b = a + "x";
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="operator+&lt;typename&gt;(B*)" id="2">
                    <reference target="1"/>
                </method>
            </class>
            <function name="foo()">
                <reference target="1"/>
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: parse template prototype declaration
  Given I have the source file:
    """
    template <class T>
    class A;

    template <class T>
    class A
    {
    };

    void foo()
    {
      A<int> x;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1"/>
            <function name="foo()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


@todo
Scenario: can not yet parse type reference defined in templated class
  Given I have the source file:
    """
    template <typename T>
    class A
    {
    public:
      typedef T B;

    };

    A<int>::B x;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;">
                <class name="B"/>
            </class>
            <variable name="x"/>
        </package>
    </project>

    """


@todo
Scenario: can not yet parse type reference defined in templated class and instantiated in a typedef
  Given I have the source file:
    """
    template<typename T>
    class A
    {
    public:
      typedef T B;
    };

    typedef A<int> C;

    C::B x;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <class name="B"/>
            </class>
            <class name="C">
                <reference target="1"/>
            </class>
            <variable name="x"/>
        </package>
    </project>

    """


Scenario: parse type reference defined in templated class
  Given I have the source file:
    """
    class A
    {
    };

    template <typename T>
    class B
    {
    public:
      typedef T C;

    };

    B<A>::C x;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B&lt;typename&gt;">
                <class name="C"/>
            </class>
            <variable name="x">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse reference to enum type defined in templated class
  Given I have the source file:
    """
    template <class T>
    class A
    {
    public:
      enum E
      {
        ev,
      };
    };

    typedef A<int> B;

    A<bool>::E x;
    B::E y;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <class name="E" id="2">
                    <field name="ev"/>
                </class>
            </class>
            <class name="B">
                <reference target="1"/>
            </class>
            <variable name="x">
                <reference target="2"/>
            </variable>
            <variable name="y">
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


@todo
Scenario: parse reference to enum value defined in templated class
  Given I have the source file:
    """
    template <class T>
    class A
    {
    public:
      enum E
      {
        ev,
      };
    };

    typedef A<int> B;

    auto x = B::ev;
    """

  When I parse the source

  Then I expect this part in the error output: "input.cpp:13:13: can not handle reference to enum value defined within a template"
  And I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <class name="E" id="2">
                    <field name="ev"/>
                </class>
            </class>
            <class name="B">
                <reference target="1"/>
            </class>
            <variable name="x">
                <reference target="2"/>
            </variable>
        </package>
    </project>

    """


Scenario: bind templated method
  Given I have the source file:
    """
    #include <functional>

    class A
    {
    };

    template<typename T>
    class B
    {
    public:
      void foo(T)
      {
      }
    };


    void test()
    {
      B<A*> b{};

      std::bind(&B<A*>::foo, &b, std::placeholders::_1);
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B&lt;typename&gt;" id="2">
                <method name="foo(T)" id="3"/>
            </class>
            <function name="test()">
                <reference target="2"/>
                <reference target="1"/>
                <reference target="2"/>
                <reference target="1"/>
                <reference target="1"/>
                <reference target="3"/>
            </function>
        </package>
    </project>

    """


Scenario: can delete pointer to class without destructor in templated class
  Given I have the source file:
    """
    class A
    {
    };

    template <typename T>
    class B
    {
    public:
      void foo()
      {
        delete x;
      }

      A* x;
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <class name="B&lt;typename&gt;">
                <method name="foo()">
                    <reference target="2"/>
                </method>
                <field name="x" id="2">
                    <reference target="1"/>
                </field>
            </class>
        </package>
    </project>

    """


Scenario: can delete pointer templated class with destructor
  Given I have the source file:
    """
    template <typename T>
    class A
    {
    public:
      ~A()
      {
      }
    };

    A<int>* x;

    void foo()
    {
      delete x;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="~A&lt;T&gt;()" id="2"/>
            </class>
            <variable name="x" id="3">
                <reference target="1"/>
            </variable>
            <function name="foo()">
                <reference target="2"/>
                <reference target="3"/>
            </function>
        </package>
    </project>

    """


Scenario: parse call to getline with stringstream
  Given I have the source file:
    """
    #include <string>
    #include <sstream>

    void foo()
    {
      std::stringstream ss{"Hello"};
      std::string item;
      std::getline(ss, item, '+');
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="foo()"/>
        </package>
    </project>

    """


Scenario: parse reference to static field of template via instance
  Given I have the source file "A.cpp":
    """
    template <typename T1>
    struct A
    {
      static T1 x;
    };

    template <typename T2>
    T2 A<T2>::x;

    A<int> y;

    void foo()
    {
      y.x = 42;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <field name="x" id="2"/>
            </class>
            <variable name="y" id="3">
                <reference target="1"/>
            </variable>
            <function name="foo()">
                <reference target="2"/>
                <reference target="3"/>
            </function>
        </package>
    </project>

    """


Scenario: parse initialization of static field of template via instance
  Given I have the source file "A.cpp":
    """
    template <typename T1>
    struct A
    {
      static T1 x;
    };

    int z = 42;

    template <typename T2>
    T2 A<T2>::x = z;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;">
                <field name="x">
                    <reference target="1"/>
                </field>
            </class>
            <variable name="z" id="1"/>
        </package>
    </project>

    """


Scenario: parse reference to static field from inherited templated class
  Given I have the source file "A.cpp":
    """
    template<typename T>
    class A
    {
    public:
      static T* x;
    };

    template <typename S> S* A<S>::x;

    class B :
        public A<B>
    {
    public:
      B* foo()
      {
        return x;
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <field name="x" id="2"/>
            </class>
            <class name="B" id="3">
                <reference target="1"/>
                <reference target="3"/>
                <method name="foo()">
                    <reference target="3"/>
                    <reference target="2"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse reference to static field from templated class
  Given I have the source file "A.cpp":
    """
    #include <string>

    const auto x = std::string::npos;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <variable name="x"/>
        </package>
    </project>

    """


Scenario: parse template class with method that has partial specialization
  Given I have the source file "A.cpp":
    """
    int generic{};
    int specFloat{};
    int specBool{};

    template <class T>
    class A
    {
    public:
      T test()
      {
        return generic;
      }

    };

    template <>
    inline float A<float>::test()
    {
      return specFloat;
    }

    template <>
    inline bool A<bool>::test()
    {
      return specBool;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <variable name="generic" id="1"/>
            <variable name="specFloat" id="2"/>
            <variable name="specBool" id="3"/>
            <class name="A&lt;typename&gt;">
                <method name="test()">
                    <reference target="1"/>
                    <reference target="2"/>
                    <reference target="3"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse reference to method that has partial specialization
  Given I have the source file "A.cpp":
    """
    template <class T>
    class A
    {
    public:
      void test()
      {
      }

    };

    template <>
    inline void A<float>::test()
    {
    }

    A<float> x{};

    void foo()
    {
      x.test();
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="test()" id="2"/>
            </class>
            <variable name="x" id="3">
                <reference target="1"/>
            </variable>
            <function name="foo()">
                <reference target="2"/>
                <reference target="3"/>
            </function>
        </package>
    </project>

    """


Scenario: parse global namespace specifier
  Given I have the source file:
    """
    class A
    {
    };

    ::A x{};
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <variable name="x">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse namespace alias specifier
  Given I have the source file:
    """
    namespace a
    {
      class A
      {
      };
    }

    namespace b = a;

    b::A x{};
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="a">
                <class name="A" id="1"/>
            </package>
            <variable name="x">
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """


@todo
Scenario: parse reference to typedef of template inside template
  Given I have the source file "A.cpp":
    """
    template <class TA>
    class A
    {
    public:
      template <class TB>
      class B
      {
      };

      typedef B<TA> C;
    };

    A<float>::C* x;
    """

  When I parse the source

  Then I expect this part in the error output: "A.cpp:13:14: can not resolve type: A<float>::C *"
  And I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;">
                <class name="B&lt;typename&gt;" id="1"/>
                <class name="C">
                    <reference target="1"/>
                </class>
            </class>
            <variable name="x"/>
        </package>
    </project>

    """


Scenario: parse reference to conversion method of template
  Given I have the source file:
    """
    template <typename T>
    class A
    {
    public:
      operator T()
      {
        return x;
      }

      T x;
    };

    A<int> y{};

    int z = y;
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1">
                <method name="operator type-parameter-0-0()" id="2">
                    <reference target="3"/>
                </method>
                <field name="x" id="3"/>
            </class>
            <variable name="y" id="4">
                <reference target="1"/>
            </variable>
            <variable name="z">
                <reference target="2"/>
                <reference target="4"/>
            </variable>
        </package>
    </project>

    """


Scenario: parse conversion that return same template with different specialization parameter
  Given I have the source file:
    """
    template<typename T>
    class A
    {
    };

    A<bool> b;
    A<int> i;

    class B
    {
    public:
      operator A<bool>()
      {
        return b;
      }

      operator A<int>()
      {
        return i;
      }
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1"/>
            <variable name="b" id="2">
                <reference target="1"/>
            </variable>
            <variable name="i" id="3">
                <reference target="1"/>
            </variable>
            <class name="B">
                <method name="operator A&lt;bool&gt;()">
                    <reference target="1"/>
                    <reference target="2"/>
                </method>
                <method name="operator A&lt;int&gt;()">
                    <reference target="1"/>
                    <reference target="3"/>
                </method>
            </class>
        </package>
    </project>

    """


Scenario: parse 2 class template specializations
  Given I have the source file:
    """
    template <typename T>
    struct A
    {
    };

    template <>
    struct A<void (*)(void)>
    {
    };

    template <typename R>
    struct A<R (*)(void)>
    {
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename&gt;" id="1"/>
            <class name="A&lt;void (*)()&gt;">
                <reference target="1"/>
            </class>
            <class name="A&lt;type-parameter-0-0 (*)()&gt;">
                <reference target="1"/>
            </class>
        </package>
    </project>

    """


Scenario: parse template template parameter
  Given I have the source file:
    """
    #include <string>

    template<typename T, int N>
    class A
    {
    };

    template<typename K, template<typename, int> typename C>
    class B
    {
        C<K, 42> key;
        C<std::string, 57> value;
    };

    B<bool, A> b{};
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A&lt;typename, int&gt;" id="1"/>
            <class name="B&lt;typename, template&lt;typename, int&gt; typename&gt;" id="2">
                <field name="key"/>
                <field name="value"/>
            </class>
            <variable name="b">
                <reference target="2"/>
                <reference target="1"/>
            </variable>
        </package>
    </project>

    """
