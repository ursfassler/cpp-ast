# language: en

Feature: parse template functions
  As a developer
  I want to get the AST with template functions
  In order to see only the relevant information


Scenario: parse simple function template specialization
  Given I have the source file:
    """
    template<typename T>
    void func()
    {
    }

    void test()
    {
      func<int>();
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="func&lt;typename&gt;()" id="1"/>
            <function name="test()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: call function from function template
  Given I have the source file:
    """
    void func()
    {
    };

    template<typename T>
    void templ()
    {
      func();
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="func()" id="1"/>
            <function name="templ&lt;typename&gt;()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: add template arguments as references
  Given I have the source file:
    """
    template<typename T>
    void func()
    {
    }

    class A{};

    void test()
    {
      func<A>();
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="func&lt;typename&gt;()" id="1"/>
            <class name="A" id="2"/>
            <function name="test()">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: add function arguments as references
  Given I have the source file:
    """
    template<typename T>
    void func(int)
    {
    }

    int b{42};

    void test()
    {
      func<int>(b);
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="func&lt;typename&gt;(int)" id="1"/>
            <variable name="b" id="2"/>
            <function name="test()">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: parse two different specialization to a template function
  Given I have the source file:
    """
    template<typename T>
    void func()
    {
    }

    void test()
    {
      func<int>();
      func<float>();
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="func&lt;typename&gt;()" id="1"/>
            <function name="test()">
                <reference target="1"/>
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse function template without explicit specialization
  Given I have the source file:
    """
    template<typename T>
    void func(T)
    {
    }

    void test()
    {
      int a{};
      func(a);
    };
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="func&lt;typename&gt;(T)" id="1"/>
            <function name="test()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse dependency from function to template class without explicit specialization
  Given I have the source file:
    """
    template<typename T>
    class ClassA
    {
      public:
        ClassA(const T&)
        {
        }
    };

    void test()
    {
      ClassA c{42};
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="ClassA&lt;typename&gt;" id="1">
                <method name="ClassA&lt;T&gt;(const T&amp;)" id="2"/>
            </class>
            <function name="test()">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


@todo
Scenario: find reference to constructor to templated class from templated function
  Given I have the source file:
    """
    template<typename T>
    class ClassA
    {
      public:
        ClassA(const T&)
        {
        }
    };

    template<typename T>
    void test(const T& arg)
    {
      ClassA c{arg}; // expect dependency to constructor ClassA()
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="ClassA&lt;typename&gt;" id="1">
                <method name="ClassA&lt;T&gt;(const T&amp;)"/>
            </class>
            <function name="test&lt;typename&gt;(const T&amp;)">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse std::cout << std::endl
  Given I have the source file:
    """
    #include <iostream>

    void func()
    {
      std::cout << std::endl;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="func()"/>
        </package>
    </project>

    """


Scenario: specialize chain of template functions
  Given I have the source file:
    """
    template<typename T>
    void funcA()
    {
    };

    template<typename T>
    void funcB()
    {
      funcA<T>();
    }

    void funcC()
    {
      funcB<int>();
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="funcA&lt;typename&gt;()" id="1"/>
            <function name="funcB&lt;typename&gt;()" id="2">
                <reference target="1"/>
            </function>
            <function name="funcC()">
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: specialize chain of template functions with more templates
  Given I have the source file:
    """
    template<typename T>
    void funcA()
    {
    };

    template<typename T>
    class ClassB
    {
    };

    template<typename T>
    void funcC()
    {
      funcA<ClassB<T>>();
    }

    void funcD()
    {
      funcC<int>();
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="funcA&lt;typename&gt;()" id="1"/>
            <class name="ClassB&lt;typename&gt;" id="2"/>
            <function name="funcC&lt;typename&gt;()" id="3">
                <reference target="1"/>
                <reference target="2"/>
            </function>
            <function name="funcD()">
                <reference target="3"/>
            </function>
        </package>
    </project>

    """


Scenario: concrete specialization of template function in template function
  Given I have the source file:
    """
    template<typename T>
    void funcA()
    {
    };

    template<typename T>
    void funcB()
    {
      funcA<int>();
    }

    void funcC()
    {
      funcB<int>();
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="funcA&lt;typename&gt;()" id="1"/>
            <function name="funcB&lt;typename&gt;()" id="2">
                <reference target="1"/>
            </function>
            <function name="funcC()">
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: parse template function defined with using
  Given I have the source file:
    """
    #include <functional>

    template<typename T>
    using Func = std::function<void(const T&)>;

    template<typename T>
    void test(const Func<T>&)
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="Func&lt;typename&gt;" id="1"/>
            <function name="test&lt;typename&gt;(const Func&lt;T&gt;&amp;)">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse template type parameter
  Given I have the source file:
    """
    class A
    {
    };

    template<A* T>
    void foo()
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <class name="A" id="1"/>
            <function name="foo&lt;A*&gt;()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: parse a template function with a const auto variable
  Given I have the source file:
    """
    template<typename T>
    void func(T x)
    {
        const auto y = x;
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="func&lt;typename&gt;(T)"/>
        </package>
    </project>

    """


Scenario: parse variadic template pack expansions
  Given I have the source file:
    """
    template<typename T, typename... S>
    void foo(S&&...)
    {
    }

    int a = 10;
    bool b = true;
    class C{};

    void bar()
    {
      foo<C>(a, b);
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="foo&lt;typename, typename&gt;(S&amp;&amp;...)" id="1"/>
            <variable name="a" id="2"/>
            <variable name="b" id="3"/>
            <class name="C" id="4"/>
            <function name="bar()">
                <reference target="1"/>
                <reference target="4"/>
                <reference target="2"/>
                <reference target="3"/>
            </function>
        </package>
    </project>

    """


Scenario: parse template function specialization within namespace
  Given I have the source file:
    """
    #include <functional>

    namespace ns
    {

    template<typename T>
    void templateFunction()
    {
    }

    void caller()
    {
      templateFunction<int>();
    }

    }
    """

  When I parse the source

  Then I expect no error output
  Then I expect the output:
    """
    <project>
        <package name="">
            <package name="ns">
                <function name="templateFunction&lt;typename&gt;()" id="1"/>
                <function name="caller()">
                    <reference target="1"/>
                </function>
            </package>
        </package>
    </project>

    """


Scenario: parse function template with same name but different signature
  Given I have the source file:
    """
    template<typename T>
    T templ(T a)
    {
      return a;
    }

    template<typename T>
    T templ(T a, T b)
    {
      return a + b;
    }

    void func1()
    {
      templ(42);
    }

    void func2()
    {
      templ(42, 57);
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="templ&lt;typename&gt;(T)" id="1"/>
            <function name="templ&lt;typename&gt;(T, T)" id="2"/>
            <function name="func1()">
                <reference target="1"/>
            </function>
            <function name="func2()">
                <reference target="2"/>
            </function>
        </package>
    </project>

    """


Scenario: handle template function with dependent type name declaration
  Given I have the source file:
    """
    #include <iterator>

    template <typename T>
    void bar()
    {
      typedef typename std::iterator_traits<T>::difference_type S;
    }

    void foo()
    {
      bar<std::string::iterator>();
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="bar&lt;typename&gt;()" id="1">
                <class name="S"/>
            </function>
            <function name="foo()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """


Scenario: handle template with same name and arguments but different template parameter
  Given I have the source file:
    """
    template<typename T, typename S>
    void foo()
    {
    }

    template<typename T>
    void foo()
    {
    }

    template<int T>
    void foo()
    {
    }

    template<bool T>
    void foo()
    {
    }

    class A{};

    template<A* T>
    void foo()
    {
    }
    """

  When I parse the source

  Then I expect the output:
    """
    <project>
        <package name="">
            <function name="foo&lt;typename, typename&gt;()"/>
            <function name="foo&lt;typename&gt;()"/>
            <function name="foo&lt;int&gt;()"/>
            <function name="foo&lt;bool&gt;()"/>
            <class name="A" id="1"/>
            <function name="foo&lt;A*&gt;()">
                <reference target="1"/>
            </function>
        </package>
    </project>

    """
