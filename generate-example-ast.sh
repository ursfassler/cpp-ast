#!/bin/sh

set -e

scriptdir=`dirname "$0"`
scriptdir=`realpath ${scriptdir}`
cd ${scriptdir}/example

mkdir -p build/
cd build
cmake ../ -DCMAKE_EXPORT_COMPILE_COMMANDS=YES

cd ${scriptdir}/example
${scriptdir}/build/app/cpp-ast ${scriptdir}/example/build/compile_commands.json
mv out.ast ${scriptdir}/example.ast

echo "generated example AST"
 
